/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./services/jsonCompletion", "./services/jsonHover", "./services/jsonValidation", "./services/jsonDocumentSymbols", "./parser/jsonParser", "./services/configuration", "./services/jsonSchemaService", "./services/jsonFolding", "./services/jsonSelectionRanges", "./utils/sort", "./utils/format", "./services/jsonLinks", "./jsonLanguageTypes"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.getLanguageService = void 0;
    const jsonCompletion_1 = require("./services/jsonCompletion");
    const jsonHover_1 = require("./services/jsonHover");
    const jsonValidation_1 = require("./services/jsonValidation");
    const jsonDocumentSymbols_1 = require("./services/jsonDocumentSymbols");
    const jsonParser_1 = require("./parser/jsonParser");
    const configuration_1 = require("./services/configuration");
    const jsonSchemaService_1 = require("./services/jsonSchemaService");
    const jsonFolding_1 = require("./services/jsonFolding");
    const jsonSelectionRanges_1 = require("./services/jsonSelectionRanges");
    const sort_1 = require("./utils/sort");
    const format_1 = require("./utils/format");
    const jsonLinks_1 = require("./services/jsonLinks");
    __exportStar(require("./jsonLanguageTypes"), exports);
    function getLanguageService(params) {
        const promise = params.promiseConstructor || Promise;
        const jsonSchemaService = new jsonSchemaService_1.JSONSchemaService(params.schemaRequestService, params.workspaceContext, promise);
        jsonSchemaService.setSchemaContributions(configuration_1.schemaContributions);
        const jsonCompletion = new jsonCompletion_1.JSONCompletion(jsonSchemaService, params.contributions, promise, params.clientCapabilities);
        const jsonHover = new jsonHover_1.JSONHover(jsonSchemaService, params.contributions, promise);
        const jsonDocumentSymbols = new jsonDocumentSymbols_1.JSONDocumentSymbols(jsonSchemaService);
        const jsonValidation = new jsonValidation_1.JSONValidation(jsonSchemaService, promise);
        return {
            configure: (settings) => {
                jsonSchemaService.clearExternalSchemas();
                settings.schemas?.forEach(jsonSchemaService.registerExternalSchema.bind(jsonSchemaService));
                jsonValidation.configure(settings);
            },
            resetSchema: (uri) => jsonSchemaService.onResourceChange(uri),
            doValidation: jsonValidation.doValidation.bind(jsonValidation),
            getLanguageStatus: jsonValidation.getLanguageStatus.bind(jsonValidation),
            parseJSONDocument: (document) => (0, jsonParser_1.parse)(document, { collectComments: true }),
            newJSONDocument: (root, diagnostics) => (0, jsonParser_1.newJSONDocument)(root, diagnostics),
            getMatchingSchemas: jsonSchemaService.getMatchingSchemas.bind(jsonSchemaService),
            doResolve: jsonCompletion.doResolve.bind(jsonCompletion),
            doComplete: jsonCompletion.doComplete.bind(jsonCompletion),
            findDocumentSymbols: jsonDocumentSymbols.findDocumentSymbols.bind(jsonDocumentSymbols),
            findDocumentSymbols2: jsonDocumentSymbols.findDocumentSymbols2.bind(jsonDocumentSymbols),
            findDocumentColors: jsonDocumentSymbols.findDocumentColors.bind(jsonDocumentSymbols),
            getColorPresentations: jsonDocumentSymbols.getColorPresentations.bind(jsonDocumentSymbols),
            doHover: jsonHover.doHover.bind(jsonHover),
            getFoldingRanges: jsonFolding_1.getFoldingRanges,
            getSelectionRanges: jsonSelectionRanges_1.getSelectionRanges,
            findDefinition: () => Promise.resolve([]),
            findLinks: jsonLinks_1.findLinks,
            format: (document, range, options) => (0, format_1.format)(document, options, range),
            sort: (document, options) => (0, sort_1.sort)(document, options)
        };
    }
    exports.getLanguageService = getLanguageService;
});
