(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "jsonc-parser", "../jsonLanguageTypes"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.format = void 0;
    const jsonc_parser_1 = require("jsonc-parser");
    const jsonLanguageTypes_1 = require("../jsonLanguageTypes");
    function format(documentToFormat, formattingOptions, formattingRange) {
        let range = undefined;
        if (formattingRange) {
            const offset = documentToFormat.offsetAt(formattingRange.start);
            const length = documentToFormat.offsetAt(formattingRange.end) - offset;
            range = { offset, length };
        }
        const options = {
            tabSize: formattingOptions ? formattingOptions.tabSize : 4,
            insertSpaces: formattingOptions?.insertSpaces === true,
            insertFinalNewline: formattingOptions?.insertFinalNewline === true,
            eol: '\n',
            keepLines: formattingOptions?.keepLines === true
        };
        return (0, jsonc_parser_1.format)(documentToFormat.getText(), range, options).map(edit => {
            return jsonLanguageTypes_1.TextEdit.replace(jsonLanguageTypes_1.Range.create(documentToFormat.positionAt(edit.offset), documentToFormat.positionAt(edit.offset + edit.length)), edit.content);
        });
    }
    exports.format = format;
});
