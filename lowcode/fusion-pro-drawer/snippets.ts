import { uuid } from '../utils'

export const snippets = [
  {
    title: '高级抽屉',
    screenshot:'/resources/O1CN01PIbHBa1dxMY8m44cf_!!6000000003802-55-tps-56-56.svg',
    schema: {
      componentName: 'FusionProDrawer',
      title: '高级抽屉',
      props: {
        title: '高级抽屉',
        placement: 'right',
        destroyOnClose: true,
        width: 760,
        operations: [
          {
            action: 'cancel',
            type: 'normal',
            content: '取消',
            id: `operation_${uuid()}`
          },
          {
            action: 'submit',
            type: 'primary',
            content: '确认',
            id: `operation_${uuid()}`
          }
        ]
      },
      children: []
    }
  }
]
