import { all } from './common/common'

const componentName = 'FusionCharts'
const title = '图表组件'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'component',
        title: '组件名称',
        setter: 'StringSetter',
      },
      ...all()
    ],
  },
};

const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        component: 'LineChart',
        data: [
          { year: '1991', value: 3 },
          { year: '1992', value: 4 },
          { year: '1993', value: 3.5 },
          { year: '1994', value: 5 },
          { year: '1995', value: 4.9 },
          { year: '1996', value: 6 },
          { year: '1997', value: 7 },
          { year: '1998', value: 9 },
          { year: '1999', value: 13 },
        ],
        xField: 'year',
        yField: 'value',
        color: ['#0079f2'],
        meta: {
          year: { alias:'年份', range: [0, 1], },
          value: { alias: '数量' }
        },
        label: { visible: true, },
      },
    },
  },
];

import AreaChart from './components/AreaChart';
import LineChart from './components/LineChart';
import StackedAreaChart from './components/StackedAreaChart';
import StepLineChart from './components/StepLineChart';
import ColumnChart from './components/ColumnChart';
import StackedColumnChart from './components/StackedColumnChart';
import GroupedColumnChart from './components/GroupedColumnChart';
import RangeColumnChart from './components/RangeColumnChart';
import HistogramChart from './components/HistogramChart';
import WaterfallChart from './components/WaterfallChart';
import BarChart from './components/BarChart';
import StackedBarChart from './components/StackedBarChart';
import GroupedBarChart from './components/GroupedBarChart';
import RangeBarChart from './components/RangeBarChart';
import PieChart from './components/PieChart';
import DonutChart from './components/DonutChart';
import RoseChart from './components/RoseChart';
import StackedRoseChart from './components/StackedRoseChart';
import GroupedRoseChart from './components/GroupedRoseChart';
import RadarChart from './components/RadarChart';
import ScatterChart from './components/ScatterChart';
import HeatmapChart from './components/HeatmapChart';
import DensityHeatmapChart from './components/DensityHeatmapChart';
import TreemapChart from './components/TreemapChart';
import CalendarChart from './components/CalendarChart';
import BulletChart from './components/BulletChart';
import GaugeChart from './components/GaugeChart';
import LiquidChart from './components/LiquidChart';
import WordCloudChart from './components/WordCloudChart';
import FunnelChart from './components/FunnelChart';


export default [
  // { ...Meta, snippets },
  ...AreaChart,
  ...LineChart,
  ...StackedAreaChart,
  ...StepLineChart,
  ...ColumnChart,
  ...StackedColumnChart,
  ...GroupedColumnChart,
  ...RangeColumnChart,
  ...HistogramChart,
  ...WaterfallChart,
  ...BarChart,
  ...StackedBarChart,
  ...GroupedBarChart,
  ...RangeBarChart,
  ...PieChart,
  ...DonutChart,
  ...RoseChart,
  ...StackedRoseChart,
  ...GroupedRoseChart,
  ...RadarChart,
  ...ScatterChart,
  ...HeatmapChart,
  ...DensityHeatmapChart,
  ...TreemapChart,
  ...CalendarChart,
  ...BulletChart,
  ...GaugeChart,
  ...LiquidChart,
  ...WordCloudChart,
  ...FunnelChart,
];
