const commons = {
  "width": {
    "name": "width",
    "title": {
      "label": "width",
      "tip": "设置图表宽度"
    },
    "setter": "NumberSetter"
  },
  "height": {
    "name": "height",
    "title": {
      "label": "height",
      "tip": "设置图表高度"
    },
    "setter": "NumberSetter"
  },
  "autoFit": {
    "name": "autoFit",
    "title": {
      "label": "autoFit",
      "tip": "图表是否自适应容器宽高。当 autoFit 设置为true时，width 和 height 的设置将失效"
    },
    "defaultValue": true,
    "setter": "BoolSetter"
  },
  "pixelRatio": {
    "name": "pixelRatio",
    "title": {
      "label": "pixelRatio",
      "tip": "设置图表渲染的像素比"
    },
    "setter": "NumberSetter",
    "defaultValue": 2
  },
  "renderer": {
    "name": "renderer",
    "title": {
      "label": "renderer",
      "tip": "设置图表渲染方式为 canvas 或 svg"
    },
    "defaultValue": "canvas",
    "setter": {
      "componentName": "RadioGroupSetter",
      "props": {
        "options": [
          "canvas",
          "svg"
        ]
      }
    }
  },
  "data": {
    "name": "data",
    "title": {
      "label": "data",
      "tip": "设置图表数据源"
    },
    "setter": "JsonSetter"
  },
  "value": {
    "name": "value",
    "title": {
      "label": "value",
      "tip": "配置仪表盘当前数值"
    },
    "setter": "NumberSetter"
  },
  "meta": {
    "name": "meta",
    "title": {
      "label": "meta",
      "tip": "全局化配置图表数据元信息，以字段为单位进行配置。在 meta 上的配置将同时影响所有组件的文本信息。"
    },
    "setter": "JsonSetter"
  },
  "xField": {
    "name": "xField",
    "title": {
      "label": "xField",
      "tip": "折线形状在 x 方向（横向延伸）对应的数据字段名，一般对应一个连续字段"
    },
    "defaultValue": "x",
    "setter": "StringSetter"
  },
  "yField": {
    "name": "yField",
    "title": {
      "label": "yField",
      "tip": "折线形状在 y 方向对应的数据字段名，一般对应一个离散字段。"
    },
    "defaultValue": "y",
    "setter": "StringSetter"
  },
  "seriesField": {
    "name": "seriesField",
    "title": {
      "label": "seriesField",
      "tip": "多折线必选。 数据集中的分组字段名，一般对应一个分类字段。通过该字段的值，折线图将会被分为多个组，通过颜色进行区分，视觉上呈现为多条折线。"
    },
    "setter": "StringSetter"
  },
  "angleField": {
    "name": "angleField",
    "title": {
      "label": "angleField",
      "tip": "扇形切片大小（弧度）所对应的数据字段名"
    },
    "setter": "StringSetter"
  },
  "radiusField": {
    "name": "radiusField",
    "title": {
      "label": "radiusField",
      "tip": "扇形切片半径长度所对应的数据字段名。"
    },
    "setter": "StringSetter"
  },
  "categoryField": {
    "name": "categoryField",
    "title": {
      "label": "categoryField",
      "tip": "扇形切片分类所对应的数据字段名（每个扇形的弧度相等）"
    },
    "setter": "StringSetter"
  },
  "colorField": {
    "name": "colorField",
    "title": {
      "label": "colorField",
      "tip": "柱形颜色映射对应的数据字段名。从基础柱状图的使用场景来说，我们不推荐对柱形进行额外的颜色映射"
    },
    "setter": "StringSetter"
  },
  "stackField": {
    "name": "stackField",
    "title": {
      "label": "stackField",
      "tip": "数据集中的分组字段名，通过该字段的值，柱子将会被分割为多个部分，通过颜色进行区分"
    },
    "setter": "StringSetter"
  },
  "sizeField": {
    "name": "sizeField",
    "title": {
      "label": "sizeField",
      "tip": "指定色块形状大小映射的字段，要求必须为一个连续字段"
    },
    "setter": "StringSetter"
  },
  "groupField": {
    "name": "groupField",
    "title": {
      "label": "groupField",
      "tip": "数据集中的分组字段名，通过该字段的值，柱子将会被分为多个组，通过颜色进行区分"
    },
    "setter": "StringSetter"
  },
  "dateField": {
    "name": "dateField",
    "title": {
      "label": "dateField",
      "tip": "日历图中对应日期数据的字段。"
    },
    "setter": "StringSetter"
  },
  "valueField": {
    "name": "valueField",
    "title": {
      "label": "valueField",
      "tip": "日历图中对应每个格子中值的字段。"
    },
    "setter": "StringSetter"
  },
  "binField": {
    "name": "binField",
    "title": {
      "label": "binField",
      "tip": "设置直方图绘制 (进行分箱) 的字段"
    },
    "setter": "StringSetter"
  },
  "maxLevel": {
    "name": "maxLevel",
    "title": {
      "label": "maxLevel",
      "tip": "矩阵树图的最大嵌套层级"
    },
    "setter": "NumberSetter"
  },
  "binWidth": {
    "name": "binWidth",
    "title": {
      "label": "binWidth",
      "tip": "设置直方图的分箱宽度，binWidth影响直方图分成多少箱"
    },
    "setter": "NumberSetter"
  },
  "binNumber": {
    "name": "binNumber",
    "title": {
      "label": "binNumber",
      "tip": " 设置直方图的分箱数量，binNumber影响直方图分箱后每个柱子的宽度。"
    },
    "setter": "NumberSetter"
  },
  "step": {
    "name": "step",
    "title": {
      "label": "step",
      "tip": "阶梯折线的阶梯转折形态，可选配置为 hv | vh | vhv | hvh"
    },
    "setter": {
      "componentName": "RadioGroupSetter",
      "props": {
        "options": [
          "hv",
          "vh",
          "vhv",
          "hvh"
        ]
      }
    }
  },
  "dateRange": {
    "name": "dateRange",
    "title": {
      "label": "dateRange",
      "tip": "显示日期的范围。"
    },
    "setter": "DateRangeSetter"
  },
  "min": {
    "name": "min",
    "title": {
      "label": "min",
      "tip": "仪表盘刻度最小值"
    },
    "setter": "NumberSetter"
  },
  "max": {
    "name": "max",
    "title": {
      "label": "max",
      "tip": "仪表盘刻度最大值。"
    },
    "setter": "NumberSetter"
  },
  "range": {
    "name": "range",
    "title": {
      "label": "range",
      "tip": "仪表盘刻度最大值。"
    },
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "NumberSetter"
        }
      }
    }
  },
  "months": {
    "name": "months",
    "title": {
      "label": "range",
      "tip": "对应月份名称的数组"
    },
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "StringSetter"
        }
      }
    }
  },
  "weeks": {
    "name": "weeks",
    "title": {
      "label": "weeks",
      "tip": "对应星期名称的数组，从周日开始"
    },
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "StringSetter"
        }
      }
    }
  },
  "rangeMax": {
    "name": "rangeMax",
    "title": {
      "label": "rangeMax",
      "tip": "进度条的色条范围区间最大值"
    },
    "setter": "NumberSetter"
  },
  "measureSize": {
    "name": "measureSize",
    "title": {
      "label": "measureSize",
      "tip": "实际进度条大小设置 即实际进度条的高度"
    },
    "setter": "NumberSetter"
  },
  "color": {
    "name": "color",
    "title": {
      "label": "color",
      "tip": "指定颜色，即可以指定一系列色值，也可以通过回调函数的方法根据对应数值进行设置。"
    },
    "defaultValue": [
      "#1383ab",
      "#c52125"
    ],
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "ColorSetter"
        }
      }
    }
  },
  "measureColors": {
    "name": "measureColors",
    "title": {
      "label": "measureColors",
      "tip": "设置进度条颜色，进度条的色条区间颜色依次取数组中的颜色色值。"
    },
    "defaultValue": [
      "#1383ab",
      "#c52125"
    ],
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "ColorSetter"
        }
      }
    }
  },
  "area": {
    "name": "area",
    "title": {
      "label": "area",
      "tip": "配置雷达图上的颜色填充"
    },
    "setter": "JsonSetter"
  },
  "areaStyle": {
    "name": "areaStyle",
    "title": {
      "label": "areaStyle",
      "tip": "设置area图形的样式。areaStyle中的fill会覆盖color的设置。sreaStyle可以直接指定，也可以通过callback的方式，根据数据为每个形状指定单独的样式。"
    },
    "setter": "JsonSetter"
  },
  "waterfallStyle": {
    "name": "waterfallStyle",
    "title": {
      "label": "waterfallStyle",
      "tip": "设置柱子样式。waterfallStyle中的fill会覆盖 color 的配置。waterfallStyle可以直接指定，也可以通过callback的方式，根据数据为每一根柱子指定单独的样式。"
    },
    "setter": "JsonSetter"
  },
  "radius": {
    "name": "radius",
    "title": {
      "label": "radius",
      "tip": "饼图的半径，原点为画布中心。配置值域为 [0,1]，0 代表饼图大小为 0，即不显示，1 代表饼图撑满绘图区域"
    },
    "setter": "NumberSetter"
  },
  "rangeSize": {
    "name": "rangeSize",
    "title": {
      "label": "rangeSize",
      "tip": "区间背景条大小设置。"
    },
    "setter": "NumberSetter"
  },
  "rangeColors": {
    "name": "rangeColors",
    "title": {
      "label": "rangeColors",
      "tip": "设置进度条背景颜色，进度条的色条区间颜色依次取数组中的颜色色值。"
    },
    "defaultValue": [
      "#1383ab",
      "#c52125"
    ],
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "ColorSetter"
        }
      }
    }
  },
  "maskImage": {
    "name": "maskImage",
    "title": {
      "label": "maskImage",
      "tip": "遮罩图片(url 或者 base64 地址) 默认配置： 无"
    },
    "setter": "StringSetter"
  },
  "random": {
    "name": "random",
    "title": {
      "label": "random",
      "tip": "自定义所使用的随机函数，其值可以是一个 [0, 1) 区间中的值"
    },
    "setter": "NumberSetter"
  },
  "markerSize": {
    "name": "markerSize",
    "title": {
      "label": "markerSize",
      "tip": "目标值 marker 大小设置（即目标值 marker 的高度），相对数值（相对于 measureSize）"
    },
    "setter": "NumberSetter"
  },
  "markerColors": {
    "name": "markerColors",
    "title": {
      "label": "markerColors",
      "tip": "设置进度条目标值颜色"
    },
    "defaultValue": [
      "#1383ab",
      "#c52125"
    ],
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "ColorSetter"
        }
      }
    }
  },
  "intensity": {
    "name": "intensity",
    "title": {
      "label": "intensity",
      "tip": "热力权重，决定一个原始数据点的对周边数据点的影响力"
    },
    "setter": "NumberSetter"
  },
  "innerRadius": {
    "name": "innerRadius",
    "title": {
      "label": "innerRadius",
      "tip": "环图的内环半径，原点为画布中心。半径和内环半径决定了环图的厚度 (thickness)。"
    },
    "setter": "NumberSetter"
  },
  "columnSize": {
    "name": "columnSize",
    "title": {
      "label": "columnSize",
      "tip": "设置柱形宽度"
    },
    "setter": "NumberSetter"
  },
  "columnStyle": {
    "name": "columnStyle",
    "title": {
      "label": "columnStyle",
      "tip": "设置柱子样式。columnStyle中的fill会覆盖 color 的配置。columnStyle可以直接指定，也可以通过callback的方式，根据数据为每一根柱子指定单独的样式"
    },
    "setter": "JsonSetter"
  },
  "sectorStyle": {
    "name": "sectorStyle",
    "title": {
      "label": "sectorStyle",
      "tip": "设置扇形样式。sectorStyle中的fill会覆盖 color 的配置。sectorStyle可以直接指定，也可以通过callback的方式，根据数据为每个扇形切片指定单独的样式。"
    },
    "setter": "JsonSetter"
  },
  "markerStyle": {
    "name": "markerStyle",
    "title": {
      "label": "markerStyle",
      "tip": "目标值 marker 的样式设置"
    },
    "setter": "JsonSetter"
  },
  "shapeSize": {
    "name": "shapeSize",
    "title": {
      "label": "shapeSize",
      "tip": "指定色块形状大小的值域，顺序为[min,max]。"
    },
    "setter": {
      "componentName": "ArraySetter",
      "props": {
        "itemSetter": {
          "componentName": "NumberSetter"
        }
      }
    }
  },
  "shapeType": {
    "name": "shapeType",
    "title": "形状的类型",
    "setter": {
      "componentName": "RadioGroupSetter",
      "props": {
        "options": [
          {
            "value": "rect",
            "title": "rect"
          },
          {
            "value": "circle",
            "title": "circle"
          }
        ]
      }
    }
  },
  "forceSquare": {
    "name": "forceSquare",
    "title": "强制正方形",
    "setter": "BoolSetter"
  },
  "shapeStyle": {
    "name": "shapeStyle",
    "title": {
      "label": "shapeStyle",
      "tip": "设置色块形状的样式。shapeStyle中的fill会覆盖 color 的配置。shapeStyle可以直接指定，也可以通过callback的方式，根据数据为每一根色块指定单独的样式"
    },
    "setter": "JsonSetter"
  },
  "rangeStyle": {
    "name": "rangeStyle",
    "title": {
      "label": "rangeStyle",
      "tip": "配置仪表盘色条样式，详细配置请参考绘图属性文档。"
    },
    "setter": "JsonSetter"
  },
  "rangeBackgroundStyle": {
    "name": "rangeBackgroundStyle",
    "title": {
      "label": "rangeBackgroundStyle",
      "tip": "配置仪表盘色条背景(空白区域)样式，详细配置请参考绘图属性文档"
    },
    "setter": "JsonSetter"
  },
  "rectStyle": {
    "name": "rectStyle",
    "title": {
      "label": "rectStyle",
      "tip": "设置treemap中的矩形样式。rectStyle中的fill会覆盖 color 的配置。pointtyle可以直接指定，也可以通过callback的方式，根据数据指定单独的样式"
    },
    "setter": "JsonSetter"
  },
  "intervalPadding": {
    "name": "intervalPadding",
    "title": {
      "label": "intervalPadding",
      "tip": "定义不同组柱子间的距离大小，值为像素。"
    },
    "setter": "NumberSetter"
  },
  "dodgePadding": {
    "name": "dodgePadding",
    "title": {
      "label": "dodgePadding",
      "tip": "定义同一分组柱子间的距离大小，值为像素，仅对分组条形图适用。"
    },
    "setter": "NumberSetter"
  },
  "minBarWidth": {
    "name": "minBarWidth",
    "title": {
      "label": "minBarWidth",
      "tip": "定义柱子的最小宽度，值为像素。"
    },
    "setter": "NumberSetter"
  },
  "maxBarWidth": {
    "name": "maxBarWidth",
    "title": {
      "label": "maxBarWidth",
      "tip": "定义柱子的最大宽度，值为像素。"
    },
    "setter": "NumberSetter"
  },
  "barWidthRatio": {
    "name": "barWidthRatio",
    "title": {
      "label": "barWidthRatio",
      "tip": "条形图宽度占比 [0-1]"
    },
    "setter": "NumberSetter"
  },
  "marginRatio": {
    "name": "marginRatio",
    "title": {
      "label": "marginRatio",
      "tip": "分组中柱子之间的间距 [0-1]，仅对分组条形图适用。"
    },
    "setter": "NumberSetter"
  },
  "barSize": {
    "name": "barSize",
    "title": {
      "label": "barSize",
      "tip": "设置条形高度。对于一般场景来说，条形高度会根据数据自行计算，不需特别指定"
    },
    "setter": "NumberSetter"
  },
  "barStyle": {
    "name": "barStyle",
    "title": {
      "label": "barStyle",
      "tip": "设置条形样式。barStyle中的fill会覆盖 color 的配置。barStyle可以直接指定，也可以通过callback的方式，根据数据指定单独的样式。"
    },
    "setter": "JsonSetter"
  },
  "lineSize": {
    "name": "lineSize",
    "title": {
      "label": "lineSize",
      "tip": "设置折线宽度"
    },
    "setter": "NumberSetter"
  },
  "lineStyle": {
    "name": "lineStyle",
    "title": {
      "label": "lineStyle",
      "tip": "设置折线样式。linsStyle中的lineWidth会覆盖 lineSize 的配置，stroke会覆盖color的设置。lineStyle可以直接指定，也可以通过callback的方式，根据数据为每一条折线指定单独的样式。"
    },
    "setter": "JsonSetter"
  },
  "pieStyle": {
    "name": "pieStyle",
    "title": {
      "label": "pieStyle",
      "tip": "设置扇形样式。pieStyle中的fill会覆盖 color 的配置。pieStyle可以直接指定，也可以通过callback的方式，根据数据为每个扇形切片指定单独的样式。"
    },
    "setter": "JsonSetter"
  },
  "smooth": {
    "name": "smooth",
    "title": {
      "label": "smooth",
      "tip": "是否将折线绘制为曲线"
    },
    "setter": "BoolSetter"
  },
  "backgroundColor": {
    "name": "backgroundColor",
    "title": {
      "label": "backgroundColor",
      "tip": "设置背景颜色"
    },
    "setter": "ColorSetter"
  },
  "background": {
    "name": "background",
    "title": {
      "label": "background",
      "tip": "背景设置"
    },
    "setter": "JsonSetter"
  },
  "selected": {
    "name": "selected",
    "title": "当前选中项",
    "setter": "NumberSetter"
  },
  "pointSize": {
    "name": "pointSize",
    "title": {
      "label": "pointSize",
      "tip": "设置点的大小"
    },
    "setter": "NumberSetter"
  },
  "point": {
    "name": "point",
    "title": {
      "label": "point",
      "tip": "配置点的样式"
    },
    "setter": "JsonSetter"
  },
  "pointStyle": {
    "name": "pointStyle",
    "title": {
      "label": "pointStyle",
      "tip": "配置点的样式"
    },
    "setter": "JsonSetter"
  },
  "liquidStyle": {
    "name": "liquidStyle",
    "title": {
      "label": "liquidStyle",
      "tip": "配置水波图的样式"
    },
    "setter": "JsonSetter"
  },
  "wordStyle": {
    "name": "wordStyle",
    "title": {
      "label": "wordStyle",
      "tip": "文字样式配置"
    },
    "setter": "JsonSetter"
  },
  "title": {
    "name": "title",
    "title": {
      "label": "title",
      "tip": "配置图表的标题，默认显示在图表左上角"
    },
    "setter": {
      "componentName": "ObjectSetter",
      "props": {
        "config": {
          "items": [
            {
              "important": true,
              "name": "text",
              "title": "标题",
              "setter": ["StringSetter", "SlotSetter"]
            },
            {
              "important": true,
              "name": "visible",
              "title": "是否展示",
              "defaultValue": true,
              "setter": "BoolSetter"
            }
          ]
        }
      },
      "initialValue": {
        "text": "标题",
        "visible": true
      }
    }
  },
  "description": {
    "name": "description",
    "title": {
      "label": "description",
      "tip": "配置图表的描述，默认显示在图表左上角，标题下方。"
    },
    "setter": {
      "componentName": "ObjectSetter",
      "props": {
        "config": {
          "items": [
            {
              "important": true,
              "name": "text",
              "title": "标题",
              "setter": ["StringSetter", "SlotSetter"]
            },
            {
              "important": true,
              "name": "visible",
              "title": "是否展示",
              "defaultValue": true,
              "setter": "BoolSetter"
            }
          ]
        }
      },
      "initialValue": {
        "text": "描述",
        "alignTo": "left",
        "visible": true
      }
    }
  },
  "axis": {
    "name": "axis",
    "title": {
      "label": "axis",
      "tip": "配置仪表盘刻度轴"
    },
    "setter": "JsonSetter"
  },
  "pivot": {
    "name": "pivot",
    "title": {
      "label": "pivot",
      "tip": "配置仪表盘指针"
    },
    "setter": "JsonSetter"
  },
  "xAxis": {
    "name": "xAxis",
    "title": {
      "label": "xAxis",
      "tip": "x方向上的坐标轴，用于展示xField对应的映射信息"
    },
    "setter": "JsonSetter"
  },
  "yAxis": {
    "name": "yAxis",
    "title": {
      "label": "yAxis",
      "tip": "y方向上的坐标轴，用于展示yField对应的映射信息"
    },
    "setter": "JsonSetter"
  },
  "dynamicHeight": {
    "name": "dynamicHeight",
    "title": {
      "label": "dynamicHeight",
      "tip": "是否是动态高度，即漏斗侧边是否平滑"
    },
    "setter": "BoolSetter"
  },
  "transpose": {
    "name": "transpose",
    "title": "水平显示",
    "setter": "BoolSetter"
  },
  "isTransposed": {
    "name": "isTransposed",
    "title": "水平显示",
    "setter": "BoolSetter"
  },
  "compareField": {
    "name": "compareField",
    "title": "水平显示",
    "setter": "StringSetter"
  },
  "angleAxis": {
    "name": "angleAxis",
    "title": {
      "label": "angleAxis",
      "tip": "雷达图角度轴配置"
    },
    "setter": "JsonSetter"
  },
  "radiusAxis": {
    "name": "radiusAxis",
    "title": {
      "label": "radiusAxis",
      "tip": "雷达图径向轴配置"
    },
    "setter": "JsonSetter"
  },
  "legend": {
    "name": "legend",
    "title": {
      "label": "legend",
      "tip": "图例，多折线时显示，用于展示颜色分类信息"
    },
    "setter": "JsonSetter"
  },
  "tooltip": {
    "name": "tooltip",
    "title": {
      "label": "tooltip",
      "tip": "信息提示框"
    },
    "setter": "JsonSetter"
  },
  "label": {
    "name": "label",
    "title": {
      "label": "label",
      "tip": "标签文本"
    },
    "setter": "JsonSetter"
  },
  "label.content": {
    "name": "label.content",
    "title": "标签格式化",
    "setter": {
      "componentName": 'FunctionSetter',
      "props": {
        "template": "\tcontent(v){ return `${v.type}\\n${v.value}` }\n"
      }
    }
  },
  "statistic": {
    "name": "statistic",
    "title": {
      "label": "statistic",
      "tip": "统计内容组件。当内半径(innerRadius) 大于 0 时才生效，默认展示汇总值，可以通过 formatter 格式化展示内容，也可以通过 customHtml 自定义更多的内容。"
    },
    "setter": "JsonSetter"
  },
  "statistic.content": {
    "name": "statistic.content",
    "title": {
      "label": "statistic.content",
      "tip": "内容展示配置"
    },
    "setter": "JsonSetter"
  },
  "statistic.content.customHtml": {
    "name": "statistic.content.customHtml",
    "title": {
      "label": "statistic.content.customHtml",
      "tip": "格式化内容"
    },
    "setter": {
      "componentName": 'FunctionSetter',
      "props": {
        "template": "\tcustomHtml(container, view, item) {\n\t\treturn `${(item.percent * 100).toFixed(2)}%` \n\t}\n"
      }
    }
  },
  "guideLine": {
    "name": "guideLine",
    "title": {
      "label": "guideLine",
      "tip": "配置图表辅助线，支持同时配置多条。"
    },
    "setter": "JsonSetter"
  },
  "conversionTag": {
    "name": "conversionTag",
    "title": {
      "label": "conversionTag",
      "tip": "转化率组件"
    },
    "setter": "JsonSetter"
  },
  "connectedArea": {
    "name": "connectedArea",
    "title": {
      "label": "connectedArea",
      "tip": "联通区域组件通过绘制同一字段的联通区域提供视觉上的辅助识别,方便进行数据对比。"
    },
    "setter": "JsonSetter"
  },
  "leaderLine": {
    "name": "leaderLine",
    "title": {
      "label": "leaderLine",
      "tip": "是否显示柱子间的连接线以及连接线的样式"
    },
    "setter": "JsonSetter"
  },
  "showTotal": {
    "name": "showTotal",
    "title": {
      "label": "showTotal",
      "tip": "是否显示总计值以及总计值的标签设置"
    },
    "setter": "JsonSetter"
  },
  "diffLabel": {
    "name": "diffLabel",
    "title": {
      "label": "diffLabel",
      "tip": "差值label，显示在柱子头部"
    },
    "setter": "JsonSetter"
  },
  "line": {
    "name": "line",
    "title": {
      "label": "diffLabel",
      "tip": "配置辅助折线，分组及颜色映射方式与面积图形保持一致"
    },
    "setter": "JsonSetter"
  },
  "quadrant": {
    "name": "quadrant",
    "title": {
      "label": "quadrant",
      "tip": "四象限组件。将图表区域进行象限划分，用以展示线性数据的分类趋势。"
    },
    "setter": "JsonSetter"
  },
  "trendLine": {
    "name": "trendLine",
    "title": {
      "label": "trendLine",
      "tip": "趋势线组件，为图表田间回归曲线。"
    },
    "setter": "JsonSetter"
  }
}

export default (keywords) => keywords.map(key=>{
  if(commons[key]){
    return commons[key]
  }
  console.log('can not find key ' + key)
}).filter(f=>f)

export const all = () => Object.keys(commons).map(key=>commons[key])