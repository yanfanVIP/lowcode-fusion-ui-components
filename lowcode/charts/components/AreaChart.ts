import common from '../common/common';

const componentName = 'FusionCharts.AreaChart'
const title = '面积图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","seriesField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","areaStyle","smooth","line","point"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","xAxis","yAxis","legend","tooltip","label", "label.content","guideLine"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          { year: '1991', value: 3 },
          { year: '1992', value: 4 },
          { year: '1993', value: 3.5 },
          { year: '1994', value: 5 },
          { year: '1995', value: 4.9 },
          { year: '1996', value: 6 },
          { year: '1997', value: 7 },
          { year: '1998', value: 9 },
          { year: '1999', value: 13 },
        ],
        xField: 'year',
        yField: 'value',
        color: ['#1383ab', '#c52125'],
        meta: {
          year: { alias:'年份', range: [0, 1], },
          value: { alias: '数量' }
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];