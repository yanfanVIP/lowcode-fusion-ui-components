import common from '../common/common'

const componentName = 'FusionCharts.StackedBarChart'
const title = '堆叠条形图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","stackField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","barSize","barStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","yAxis","xAxis","legend","tooltip","label", "label.content"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          {
            地区: '华东',
            细分: '公司',
            销售额: 1454715.807999998,
          },
          {
            地区: '华东',
            细分: '消费者',
            销售额: 2287358.261999998,
          },
          {
            地区: '中南',
            细分: '公司',
            销售额: 1335665.3239999984,
          },
          {
            地区: '中南',
            细分: '消费者',
            销售额: 2057936.7620000008,
          },
          {
            地区: '东北',
            细分: '公司',
            销售额: 834842.827,
          },
          {
            地区: '东北',
            细分: '消费者',
            销售额: 1323985.6069999991,
          },
          {
            地区: '华北',
            细分: '公司',
            销售额: 804769.4689999995,
          },
          {
            地区: '华北',
            细分: '消费者',
            销售额: 1220430.5610000012,
          },
          {
            地区: '西南',
            细分: '公司',
            销售额: 469341.684,
          },
          {
            地区: '西南',
            细分: '消费者',
            销售额: 677302.8919999995,
          },
          {
            地区: '西北',
            细分: '公司',
            销售额: 253458.1840000001,
          },
          {
            地区: '西北',
            细分: '消费者',
            销售额: 458058.1039999998,
          },
        ],
        color: ['#1383ab', '#c52125'],
        xField: '销售额',
        yField: '地区',
        stackField: '细分',
        meta: {
          type: {
            alias: '类别',
          },
          sales: {
            alias: '销售额(万)',
          },
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];