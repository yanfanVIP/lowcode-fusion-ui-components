import common from '../common/common'

const componentName = 'FusionCharts.BulletChart'
const title = '子弹图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","rangeMax","measureSize"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["measureColors","rangeSize","rangeColors","markerSize","markerColors","markerStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","axis","legend","tooltip"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          {
            title: '满意度',
            measures: [83],
            target: [90],
          },
          {
            title: '好评度',
            measures: [73],
            target: [90],
          },
        ],
        measureField:'measures',
        rangeField:'ranges',
        targetField:'target',
        xField:"title",
        rangeMax:100,
        measureColors : ['#1383ab', '#c52125'],
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];