import common from '../common/common';

const componentName = 'FusionCharts.StackedColumnChart'
const title = '堆叠柱状图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","stackField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","columnSize","columnStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","xAxis","yAxis","legend","tooltip","label", "label.content","guideLine", "connectedArea"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          {
            year: '1991',
            value: 3,
            type: 'Lon',
          },
          {
            year: '1992',
            value: 4,
            type: 'Lon',
          },
          {
            year: '1993',
            value: 3.5,
            type: 'Lon',
          },
          {
            year: '1994',
            value: 5,
            type: 'Lon',
          },
          {
            year: '1995',
            value: 4.9,
            type: 'Lon',
          },
          {
            year: '1996',
            value: 6,
            type: 'Lon',
          },
          {
            year: '1997',
            value: 7,
            type: 'Lon',
          },
          {
            year: '1998',
            value: 9,
            type: 'Lon',
          },
          {
            year: '1999',
            value: 13,
            type: 'Lon',
          },
          {
            year: '1991',
            value: 3,
            type: 'Bor',
          },
          {
            year: '1992',
            value: 4,
            type: 'Bor',
          },
          {
            year: '1993',
            value: 3.5,
            type: 'Bor',
          },
          {
            year: '1994',
            value: 5,
            type: 'Bor',
          },
          {
            year: '1995',
            value: 4.9,
            type: 'Bor',
          },
          {
            year: '1996',
            value: 6,
            type: 'Bor',
          },
          {
            year: '1997',
            value: 7,
            type: 'Bor',
          },
          {
            year: '1998',
            value: 9,
            type: 'Bor',
          },
          {
            year: '1999',
            value: 13,
            type: 'Bor',
          },
        ],
        xField: 'year',
        yField: 'value',
        stackField:'type',
        color: ['#ae331b', '#1a6179'],
        meta: {
          type: {
            alias: '类别',
          },
          sales: {
            alias: '销售额(万)',
          },
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];