import common from '../common/common'

const componentName = 'FusionCharts.TreemapChart'
const title = '矩形树图'

const datas = {
  name: 'root',
  children: [
    { name: '分类 1', value: 560 },
    { name: '分类 2', value: 500 },
    { name: '分类 3', value: 150 },
    { name: '分类 4', value: 140 },
    { name: '分类 5', value: 115 },
    { name: '分类 6', value: 95 },
    { name: '分类 7', value: 90 },
    { name: '分类 8', value: 75 },
    { name: '分类 9', value: 98 },
    { name: '分类 10', value: 60 },
    { name: '分类 11', value: 45 },
    { name: '分类 12', value: 40 },
    { name: '分类 13', value: 40 },
    { name: '分类 14', value: 35 },
    { name: '分类 15', value: 40 },
    { name: '分类 16', value: 40 },
    { name: '分类 17', value: 40 },
    { name: '分类 18', value: 30 },
    { name: '分类 19', value: 28 },
    { name: '分类 20', value: 16 },
  ],
}

function processData(data) {
  let sumValue = 0;
  data.children.map((d) => {
    sumValue += d.value;
  });
  data.value = sumValue;
  return data
}

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","maxLevel","colorField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","rectStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","tooltip","label", "label.content"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: processData(datas),
        colorField: 'name',
      },
    },
  },
];



export default [
  { ...Meta, snippets }
];