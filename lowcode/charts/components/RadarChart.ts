import common from '../common/common'


const componentName = 'FusionCharts.RadarChart'
const title = '雷达图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","angleField","radiusField","seriesField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","smooth","area","point","line"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","angleAxis","radiusAxis","legend","tooltip","label", "label.content"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          {
            item: 'Design',
            user: 'a',
            score: 70,
          },
          {
            item: 'Design',
            user: 'b',
            score: 30,
          },
          {
            item: 'Development',
            user: 'a',
            score: 60,
          },
          {
            item: 'Development',
            user: 'b',
            score: 70,
          },
          {
            item: 'Marketing',
            user: 'a',
            score: 60,
          },
          {
            item: 'Marketing',
            user: 'b',
            score: 50,
          },
          {
            item: 'Users',
            user: 'a',
            score: 40,
          },
          {
            item: 'Users',
            user: 'b',
            score: 50,
          },
          {
            item: 'Test',
            user: 'a',
            score: 60,
          },
          {
            item: 'Test',
            user: 'b',
            score: 70,
          },
          {
            item: 'Language',
            user: 'a',
            score: 70,
          },
          {
            item: 'Language',
            user: 'b',
            score: 50,
          },
          {
            item: 'Technology',
            user: 'a',
            score: 50,
          },
          {
            item: 'Technology',
            user: 'b',
            score: 40,
          },
          {
            item: 'Support',
            user: 'a',
            score: 30,
          },
          {
            item: 'Support',
            user: 'b',
            score: 40,
          },
          {
            item: 'Sales',
            user: 'a',
            score: 60,
          },
          {
            item: 'Sales',
            user: 'b',
            score: 40,
          },
          {
            item: 'UX',
            user: 'a',
            score: 50,
          },
          {
            item: 'UX',
            user: 'b',
            score: 60,
          },
        ],
        angleField: 'item',
        radiusField: 'score',
        seriesField: 'user',
        radiusAxis:{
          grid: {
            line: {
              type: 'line',
            },
          },
        },
        line:{
          visible: true,
        },
        point:{
          visible: true,
          shape: 'circle',
        },
        legend: {
          visible: true,
          position: 'bottom-center',
        }
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];