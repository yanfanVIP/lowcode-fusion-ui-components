import common from '../common/common'

const componentName = 'FusionCharts.HistogramChart'
const title = '直方图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","binField","binWidth","binNumber"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","columnStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","xAxis","yAxis","legend","tooltip","label", "label.content","guideLine"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          { value: 1.2 },
          { value: 3.4 },
          { value: 3.7 },
          { value: 4.3 },
          { value: 5.2 },
          { value: 5.8 },
          { value: 6.1 },
          { value: 6.5 },
          { value: 6.8 },
          { value: 7.1 },
          { value: 7.3 },
          { value: 7.7 },
          { value: 8.3 },
          { value: 8.6 },
          { value: 8.8 },
          { value: 9.1 },
          { value: 9.2 },
          { value: 9.4 },
          { value: 9.5 },
          { value: 9.7 },
          { value: 10.5 },
          { value: 10.7 },
          { value: 10.8 },
          { value: 11.0 },
          { value: 11.0 },
          { value: 11.1 },
          { value: 11.2 },
          { value: 11.3 },
          { value: 11.4 },
          { value: 11.4 },
          { value: 11.7 },
          { value: 12.0 },
          { value: 12.9 },
          { value: 12.9 },
          { value: 13.3 },
          { value: 13.7 },
          { value: 13.8 },
          { value: 13.9 },
          { value: 14.0 },
          { value: 14.2 },
          { value: 14.5 },
          { value: 15 },
          { value: 15.2 },
          { value: 15.6 },
          { value: 16.0 },
          { value: 16.3 },
          { value: 17.3 },
          { value: 17.5 },
          { value: 17.9 },
          { value: 18.0 },
          { value: 18.0 },
          { value: 20.6 },
          { value: 21 },
          { value: 23.4 },
        ],
        color: ['#1383ab', '#c52125'],
        binField: 'value',
        binNumber: 10,
        meta: {
          type: {
            alias: '类别',
          },
          sales: {
            alias: '销售额(万)',
          },
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];