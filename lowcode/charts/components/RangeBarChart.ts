import common from '../common/common'

const componentName = 'FusionCharts.RangeBarChart'
const title = '区间条形图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","groupField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","barSize","barStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","yAxis","xAxis","legend","tooltip","label", "label.content"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          { type: '分类一', values: [76, 100] },
          { type: '分类二', values: [56, 108] },
          { type: '分类三', values: [38, 129] },
          { type: '分类四', values: [58, 155] },
          { type: '分类五', values: [45, 120] },
          { type: '分类六', values: [23, 99] },
          { type: '分类七', values: [18, 56] },
          { type: '分类八', values: [18, 34] },
        ],
        color: ['#1383ab', '#c52125'],
        xField: 'values',
        yField: 'type',
        meta: {
          type: {
            alias: '类别',
          },
          sales: {
            alias: '销售额(万)',
          },
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];