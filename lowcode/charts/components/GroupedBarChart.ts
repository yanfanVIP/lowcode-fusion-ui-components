import common from '../common/common'

const componentName = 'FusionCharts.GroupedBarChart'
const title = '分组条形图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","colorField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","intervalPadding","dodgePadding","minBarWidth","maxBarWidth","barWidthRatio","marginRatio","barSize","barStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","yAxis","xAxis","legend","tooltip","label", "label.content"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          {
            label: 'Mon.',
            type: 'series1',
            value: 2800,
          },
          {
            label: 'Mon.',
            type: 'series2',
            value: 2260,
          },
          {
            label: 'Tues.',
            type: 'series1',
            value: 1800,
          },
          {
            label: 'Tues.',
            type: 'series2',
            value: 1300,
          },
          {
            label: 'Wed.',
            type: 'series1',
            value: 950,
          },
          {
            label: 'Wed.',
            type: 'series2',
            value: 900,
          },
          {
            label: 'Thur.',
            type: 'series1',
            value: 500,
          },
          {
            label: 'Thur.',
            type: 'series2',
            value: 390,
          },
          {
            label: 'Fri.',
            type: 'series1',
            value: 170,
          },
          {
            label: 'Fri.',
            type: 'series2',
            value: 100,
          },
        ],
        isGroup: true,
        color: ['#1383ab', '#c52125'],
        xField: 'value',
        yField: 'label',
        seriesField: 'type',
        meta: {
          type: {
            alias: '类别',
          },
          sales: {
            alias: '销售额(万)',
          },
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];