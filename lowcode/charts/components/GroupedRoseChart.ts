import common from '../common/common'


const componentName = 'FusionCharts.GroupedRoseChart'
const title = '分组玫瑰图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","radiusField","categoryField","groupField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["radius","color","sectorStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","legend","tooltip","label", "label.content"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          {
            type: '分类一',
            value: 27,
            user: '用户一',
          },
          {
            type: '分类二',
            value: 25,
            user: '用户一',
          },
          {
            type: '分类三',
            value: 18,
            user: '用户一',
          },
          {
            type: '分类四',
            value: 15,
            user: '用户一',
          },
          {
            type: '分类五',
            value: 10,
            user: '用户一',
          },
          {
            type: '其它',
            value: 5,
            user: '用户一',
          },
          {
            type: '分类一',
            value: 7,
            user: '用户二',
          },
          {
            type: '分类二',
            value: 5,
            user: '用户二',
          },
          {
            type: '分类三',
            value: 38,
            user: '用户二',
          },
          {
            type: '分类四',
            value: 5,
            user: '用户二',
          },
          {
            type: '分类五',
            value: 20,
            user: '用户二',
          },
          {
            type: '其它',
            value: 15,
            user: '用户二',
          },
        ],
        isGroup: true,
        radius: 0.8,
        radiusField: 'value',
        categoryField: 'type',
        seriesField: 'user',
        color: ['#1383ab', '#c52125'],
        label: { 
          offset: -15,
        },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];