import common from '../common/common'

const componentName = 'FusionCharts.LineChart'
const title = '折线图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","seriesField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","lineSize","lineStyle","smooth","point"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","xAxis","yAxis","legend","tooltip","label", "label.content","guideLine"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          { country: 'Asia', year: '1750', value: 502,},
          { country: 'Asia', year: '1800', value: 635,},
          { country: 'Europe', year: '1750', value: 163,},
          { country: 'Europe', year: '1800', value: 203,},
        ],
        xField: 'year',
        yField: 'value',
        color: ['#1383ab', '#c52125'],
        seriesField: "country",
        meta: {
          year: { alias:'年份', range: [0, 1], },
          value: { alias: '数量' }
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];