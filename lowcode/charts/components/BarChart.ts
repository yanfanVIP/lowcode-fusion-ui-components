import common from '../common/common'

const componentName = 'FusionCharts.BarChart'
const title = '条形图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","colorField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","barSize","barStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","yAxis","xAxis","legend","tooltip","label", "label.content","conversionTag"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          { 地区: '华东', 销售额: 4684506.442 },
          { 地区: '中南', 销售额: 4137415.0929999948 },
          { 地区: '东北', 销售额: 2681567.469000001 },
          { 地区: '华北', 销售额: 2447301.017000004 },
          { 地区: '西南', 销售额: 1303124.508000002 },
          { 地区: '西北', 销售额: 815039.5959999998 },
        ],
        color: ['#1383ab', '#c52125'],
        xField: '销售额',
        yField: '地区',
        meta: {
          type: {
            alias: '类别',
          },
          sales: {
            alias: '销售额(万)',
          },
        },
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];