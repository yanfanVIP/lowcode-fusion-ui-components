import common from '../common/common'

const componentName = 'FusionCharts.GroupedColumnChart'
const title = '分组柱状图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","groupField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","columnSize","columnStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","xAxis","yAxis","legend","tooltip","label", "label.content","guideLine"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          {
            name: 'London',
            月份: 'Jan.',
            月均降雨量: 18.9,
          },
          {
            name: 'London',
            月份: 'Feb.',
            月均降雨量: 28.8,
          },
          {
            name: 'London',
            月份: 'Mar.',
            月均降雨量: 39.3,
          },
          {
            name: 'London',
            月份: 'Apr.',
            月均降雨量: 81.4,
          },
          {
            name: 'London',
            月份: 'May',
            月均降雨量: 47,
          },
          {
            name: 'London',
            月份: 'Jun.',
            月均降雨量: 20.3,
          },
          {
            name: 'London',
            月份: 'Jul.',
            月均降雨量: 24,
          },
          {
            name: 'London',
            月份: 'Aug.',
            月均降雨量: 35.6,
          },
          {
            name: 'Berlin',
            月份: 'Jan.',
            月均降雨量: 12.4,
          },
          {
            name: 'Berlin',
            月份: 'Feb.',
            月均降雨量: 23.2,
          },
          {
            name: 'Berlin',
            月份: 'Mar.',
            月均降雨量: 34.5,
          },
          {
            name: 'Berlin',
            月份: 'Apr.',
            月均降雨量: 99.7,
          },
          {
            name: 'Berlin',
            月份: 'May',
            月均降雨量: 52.6,
          },
          {
            name: 'Berlin',
            月份: 'Jun.',
            月均降雨量: 35.5,
          },
          {
            name: 'Berlin',
            月份: 'Jul.',
            月均降雨量: 37.4,
          },
          {
            name: 'Berlin',
            月份: 'Aug.',
            月均降雨量: 42.4,
          },
        ],
        isGroup: true,
        xField: '月份',
        yField: '月均降雨量',
        seriesField:'name',
        color: ['#1383ab', '#c52125'],
        label: { visible: true, },
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];