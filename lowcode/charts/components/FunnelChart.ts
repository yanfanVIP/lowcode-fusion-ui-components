import common from '../common/common'


const componentName = 'FusionCharts.FunnelChart'
const title = '漏斗图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["data","meta","xField","yField","dynamicHeight","transpose", "isTransposed","compareField"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","legend","tooltip","label", "label.content"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        data: [
          { action: '浏览网站', pv: 50000, quarter: '2020Q1' },
          { action: '放入购物车', pv: 35000, quarter: '2020Q1' },
          { action: '生成订单', pv: 25000, quarter: '2020Q1' },
          { action: '支付订单', pv: 15000, quarter: '2020Q1' },
          { action: '完成交易', pv: 11500, quarter: '2020Q1' },
          { action: '浏览网站', pv: 80000, quarter: '2020Q2' },
          { action: '放入购物车', pv: 63000, quarter: '2020Q2' },
          { action: '生成订单', pv: 47000, quarter: '2020Q2' },
          { action: '支付订单', pv: 24000, quarter: '2020Q2' },
          { action: '完成交易', pv: 17500, quarter: '2020Q2' },
        ],
        xField:'action',
        yField:'pv',
        compareField:'quarter',
        transpose: true,
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];