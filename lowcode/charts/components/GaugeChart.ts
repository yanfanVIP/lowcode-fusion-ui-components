import common from '../common/common'

const componentName = 'FusionCharts.GaugeChart'
const title = '仪表盘'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'BizCharts',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionCharts',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'common-group',
        type: 'group',
        display: 'accordion',
        title: '基础配置',
        items: common(["width","height","autoFit","pixelRatio","renderer"])
      },{
        name: 'data-group',
        type: 'group',
        display: 'accordion',
        title: '数据配置',
        items: common(["value","min","max","range"])
      },{
        name: 'styles-group',
        type: 'group',
        display: 'accordion',
        title: '样式配置',
        items: common(["color","rangeSize","rangeStyle","rangeBackgroundStyle"])
      },{
        name: 'components-group',
        type: 'group',
        display: 'accordion',
        title: '组件配置',
        items: common(["title","description","axis","pivot","statistic", "statistic.content", "statistic.content.customHtml"])
      },
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        _componentName: componentName,
        width:400,
        height:400,
        value:64,
        min:0,
        max: 100,
        range: [0, 25, 50, 75, 100],
        color: ['#39B8FF', '#52619B', '#43E089', '#C0EDF3'],
        statistic:{
          title: {
            content: '标题'
          },
          content: {
            content: '内容'
          }
        }
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];