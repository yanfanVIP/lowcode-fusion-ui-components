import { snippets } from './snippets'
import { uuid } from '../utils'

const FusionProModalMeta = {
  componentName: 'FusionProModal',
  title: 'FusionProModal',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  group: '高级组件',
  category: 'AntDesignPro',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: 'FusionProModal',
    main: 'lib/index.js',
    destructuring: true,
    subName: ''
  },
  props: [
    {
      name: 'ref',
      title: {
        label: 'ref',
        tip: "ref | 通过 this.$('xxx') 获取到组件实例"
      },
      defaultValue: () => {
        return `pro_modal_${uuid()}`
      },
      setter: ['StringSetter', 'VariableSetter']
    },
    {
      name: 'title',
      title: '标题',
      propType: 'string',
      setter: ['StringSetter', 'VariableSetter']
    },
    {
      name: 'width',
      title: { label: '宽度', tip: 'width | 宽度' },
      propType: { type: 'oneOfType', value: ['string', 'number'] },
      setter: ['NumberSetter', 'StringSetter', 'VariableSetter'],
      defaultValue: 520
    },
    {
      name: 'mask',
      title: { label: '显示遮罩', tip: 'mask | 显示遮罩' },
      propType: 'bool',
      setter: 'BoolSetter',
      defaultValue: true
    },
    {
      name: 'visible',
      title: { label: '是否可见', tip: '对话框是否可见' },
      propType: 'bool',
      setter: 'BoolSetter'
    },
    {
      name: 'centered',
      title: { label: '垂直居中', tip: '垂直居中展示 Modal' },
      propType: 'bool',
      defaultValue: false,
      setter: 'BoolSetter'
    },
    {
      name: 'closable',
      title: { label: '显示关闭按钮', tip: '是否显示右上角的关闭按钮' },
      propType: 'bool',
      defaultValue: true,
      setter: 'BoolSetter'
    },
    {
      name: 'confirmLoading',
      title: { label: '确定按钮loading', tip: '确定按钮loading' },
      propType: 'bool',
      defaultValue: false,
      setter: 'BoolSetter'
    },
    {
      name: 'destroyOnClose',
      title: { label: '销毁子元素', tip: '关闭时销毁 Modal 里的子元素' },
      propType: 'bool',
      defaultValue: false,
      setter: 'BoolSetter'
    },
    {
      name: 'forceRender',
      title: { label: '强制渲染Modal', tip: '强制渲染Modal' },
      propType: 'bool',
      defaultValue: false,
      setter: 'BoolSetter'
    },
    {
      name: 'keyboard',
      title: { label: 'esc关闭', tip: '是否支持键盘 esc 关闭' },
      propType: 'bool',
      defaultValue: true,
      setter: 'BoolSetter'
    },
    {
      name: 'maskClosable',
      title: { label: '点击蒙层关闭', tip: '点击蒙层是否允许关闭' },
      propType: 'bool',
      defaultValue: true,
      setter: 'BoolSetter'
    },
    {
      name: 'zIndex',
      title: { label: 'z-index', tip: '设置 Modal 的 `z-index`' },
      propType: 'number',
      setter: 'NumberSetter'
    },
    {
      name: 'wrapClassName',
      title: { label: '外层容器类名', tip: '对话框外层容器的类名' },
      propType: 'string',
      setter: 'StringSetter'
    },
    {
      name: 'getContainer',
      title: {
        label: '指定挂载节点',
        tip: '指定 Modal 挂载的 HTML 节点, false 为挂载在当前 dom'
      },
      propType: { type: 'oneOfType', value: ['node', 'func'] }
    },
    {
      name: 'operations',
      display: 'block',
      title: '操作项',
      getValue: (target, value) => {
        return value || [];
      },
      setter: {
        componentName: 'MixedSetter',
        props: {
          setters: [
            {
              componentName: 'SlotSetter',
              defaultValue: {
                type: 'JSSlot',
                value: [],
              },
            },
            {
              componentName: 'ArraySetter',
              props: {
                itemSetter: {
                  componentName: 'ObjectSetter',
                  props: {
                    config: {
                      items: [
                        {
                          name: 'id',
                          condition: () => false,
                          setter: (target) => {
                            if (!target.getValue()) {
                              target.setValue(`${target.id}`);
                            }
                            return 'StringSetter';
                          },
                        },
                        {
                          name: 'content',
                          display: 'inline',
                          title: '文本',
                          setter: 'StringSetter',
                          important: true,
                        },
                        {
                          "name": "size",
                          "title": {
                            "label": {
                              "type": "i18n",
                              "zh-CN": "按钮尺寸",
                              "en-US": "Button Size"
                            },
                            "tip": {
                              "type": "i18n",
                              "zh-CN": "属性: size | 说明: 按钮尺寸",
                              "en-US": "prop: size | description: button size"
                            }
                          },
                          "setter": {
                            "componentName": "SelectSetter",
                            "props": {
                              "options": [
                                {
                                  "title": "small",
                                  "value": "small"
                                },
                                {
                                  "title": "medium",
                                  "value": "medium"
                                },
                                {
                                  "title": "large",
                                  "value": "large"
                                }
                              ]
                            }
                          },
                          "description": "按钮的尺寸",
                          "defaultValue": "medium"
                        },{
                          "name": "shape",
                          "title": '按钮形状',
                          "setter": {
                            "componentName": "SelectSetter",
                            "props": {
                              "options": [
                                {
                                  "title": "default",
                                  "value": "default"
                                },
                                {
                                  "title": "circle",
                                  "value": "circle"
                                },
                                {
                                  "title": "round",
                                  "value": "round"
                                }
                              ]
                            }
                          },
                          "description": "按钮形状",
                          "defaultValue": "default"
                        },
                        {
                          "name": "type",
                          "title": {
                            "label": {
                              "type": "i18n",
                              "zh-CN": "按钮类型",
                              "en-US": "Button Type"
                            },
                            "tip": {
                              "type": "i18n",
                              "zh-CN": "属性: type | 说明: 按钮类型",
                              "en-US": "prop: type | description: button type"
                            }
                          },
                          "setter": {
                            "componentName": "SelectSetter",
                            "props": {
                              "options": [
                                {
                                  "title": "primary",
                                  "value": "primary"
                                },
                                {
                                  "title": "dashed",
                                  "value": "dashed"
                                },
                                {
                                  "title": "link",
                                  "value": "link"
                                },
                                {
                                  "title": "text",
                                  "value": "text"
                                },
                                {
                                  "title": "default",
                                  "value": "default"
                                }
                              ]
                            }
                          },
                          "description": "按钮的类型",
                          "defaultValue": "primary"
                        },
                        {
                          "name": "ghost",
                          "title": {
                            "type": "i18n",
                            "zh-CN": "幽灵按钮",
                            "en-US": "ghost"
                          },
                          "setter": 'BoolSetter',
                          "description": "是否为幽灵按钮",
                          "defaultValue": false
                        },
                        {
                          "name": "danger",
                          "title": "是否为警告按钮",
                          initialValue: false,
                          setter: 'BoolSetter',
                          "description": "是否为警告按钮",
                          "defaultValue": false
                        },
                        {
                          "name": "bgColor",
                          "title": "按钮颜色",
                          "setter": {
                            "componentName": "ColorSetter"
                          }
                        },{
                          "name": "fontColor",
                          "title": "文字颜色",
                          "setter": {
                            "componentName": "ColorSetter"
                          }
                        },
                        {
                          "name": "icon",
                          "title": "图标",
                          "setter": "StringSetter"
                        },
                        {
                          name: 'action',
                          display: 'inline',
                          title: '操作',
                          important: true,
                          setValue: (target, value: any) => {
                            const actionNameMap: any = {
                              submit: '提交',
                              reset: '重置',
                              custom: '自定义',
                            };
                            const actionName = actionNameMap[value] || '自定义';
                            target.parent.setPropValue('content', actionName);
                          },
                          setter: {
                            componentName: 'SelectSetter',
                            props: {
                              options: [
                                {
                                  title: '提交',
                                  value: 'submit',
                                },
                                {
                                  title: '重置',
                                  value: 'reset',
                                },
                                {
                                  title: '自定义',
                                  value: 'custom',
                                },
                              ],
                            },
                          },
                        },
                        {
                          name: 'onClick',
                          display: 'inline',
                          title: '点击事件',
                          condition: (target) => {
                            const action = target.parent.getPropValue('action');
                            return !action || action === 'custom';
                          },
                          setter: 'FunctionSetter',
                          extraProps: {
                            supportVariable: true,
                          },
                        },
                        {
                          name: 'htmlType',
                          condition: () => false,
                        }
                      ],
                    },
                  },
                  initialValue: () => {
                    return {
                      content: '提交',
                      action: 'submit',
                      type: 'normal',
                    };
                  },
                },
              },
            },
          ],
        },
      },
    },
    {
      name: 'onCancel',
      title: {
        label: '取消按钮回调',
        tip: '点击遮罩层或右上角叉或取消按钮的回调'
      },
      propType: 'func'
    },
    {
      name: 'onOk',
      title: { label: '点击确定回调', tip: '点击确定回调' },
      propType: 'func'
    }
  ],
  configure: {
    component: {
      isContainer: true,
      isModal: true,
      rootSelector: '.ant-modal-content',
      nestingRule: {
        parentWhitelist: ['Page', 'Component']
      }
    },
    supports: {
      style: true,
      events: [
        {
          name: 'afterClose',
          templete:
            "onCancel(${extParams}){\n// 完全关闭后的回调\nconsole.log('afterClose');}"
        },
        {
          name: 'onCancel',
          template:
            "onCancel(${extParams}){\n// 点击遮罩层或右上角叉或取消按钮的回调\nconsole.log('onCancel');}"
        },
        {
          name: 'onOk',
          template:
            "onOk(${extParams}){\n// 点击确定回调\nconsole.log('onOk');}"
        }
      ]
    }
  }
}

export default {
  ...FusionProModalMeta,
  snippets
}
