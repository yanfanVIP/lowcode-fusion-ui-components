export const snippets = [
  {
    title: '高级对话框',
    screenshot:'/resources/O1CN01n5JLZG1aBmYZlckSx_!!6000000003292-55-tps-56-56.svg',
    schema: {
      title: '高级对话框',
      componentName: 'FusionProModal',
      props: {
        title: '高级对话框',
        width: 760,
        operations: [
          {
            action: 'cancel',
            type: 'normal',
            content: '取消'
          },
          {
            action: 'submit',
            type: 'primary',
            content: '确认'
          }
        ]
      },
      children: []
    }
  }
]
