import { IComponentDescription } from '../types/index';
import { ProTableProps } from '../pro-table/pro-table-meta';

const TableProps = ProTableProps.map((item) => {
  if (item.name === 'advanced') {
    return {
      type: 'group',
      title: '高级',
      name: 'other',
      extraProps: {
        display: 'accordion',
        defaultCollapsed: true,
      },
      items: [
        {
          name: 'sortColumn',
          title: '排序栏',
          setter: "BoolSetter"
        },
        {
          type: 'field',
          name: '!选择模式',
          title: '选择模式',
          display: 'inline',
          setter: {
            componentName: 'RadioGroupSetter',
            props: {
              options: [
                {
                  title: '无',
                  value: 'none',
                  tip: 'none',
                },
                {
                  title: '多选',
                  value: 'multiple',
                  tip: 'multiple',
                },
                {
                  title: '单选',
                  value: 'single',
                  tip: 'single',
                },
              ],
            },
          },
          defaultValue: 'none',
          getValue: (target) => {
            const rowSelection = target.parent.getPropValue('rowSelection');
            if (!rowSelection) {
              return 'none';
            }
            return rowSelection.mode === 'single' ? 'single' : 'multiple';
          },
          setValue: (field, value) => {
            const { node } = field;
            if (['single', 'multiple'].includes(value)) {
              node.setPropValue('rowSelection', {
                ...node.getPropValue('rowSelection'),
                mode: value,
              });
            } else {
              node.setPropValue('rowSelection', undefined);
            }
          },
        },
        {
          name: 'tableLayout',
          title: '表格布局方式',
          setter: {
            componentName: 'RadioGroupSetter',
            props: {
              options: [
                {
                  title: 'fixed',
                  value: 'fixed'
                },
                {
                  title: 'auto',
                  value: 'auto'
                },
              ],
            },
          },
        },
        {
          name: 'useVirtual',
          title: '开启虚拟滚动',
          setter: 'BoolSetter'
        },
        {
          name: 'fixedHeader',
          title: '表头固定',
          setter: 'BoolSetter'
        },
        {
          name: 'maxBodyHeight',
          title: '内容高度',
          setter: ['NumberSetter', 'StringSetter']
        },{
          name: 'stickyHeader',
          title: '表头sticky',
          setter: 'BoolSetter'
        },
        {
          type: 'field',
          name: 'indexColumn',
          title: '开启序号列',
          extraProps: {
            display: 'inline',
            defaultValue: false,
          },
          setter: {
            componentName: 'BoolSetter',
          },
        },
        {
          type: 'field',
          name: 'settingButtons',
          title: '开启设置按钮',
          extraProps: {
            display: 'inline',
            defaultValue: false,
          },
          setter: {
            componentName: 'BoolSetter',
          },
        },
        {
          type: 'field',
          name: 'primaryKey',
          title: {
            label: '数据主键',
            tip: '数据主键用于区分数据中不同的行，对行选择和行编辑功能非常重要，不同的行主键值不可重复，一般采用数据库中自增 ID 字段',
          },
          extraProps: {
            display: 'inline',
            defaultValue: 'id',
            condition: () => false,
          },
        },
        {
          name: 'cellDefault',
          title: {
            label: '单元格缺省填充',
            tip: '当单元格值为空时，显示内容',
          },
          setter: 'StringSetter',
        },
      ],
    };
  }
  return { ...item };
});

export const Meta: IComponentDescription = {
  componentName: 'CospandTable',
  title: '嵌套表格',
  docUrl: '',
  icon: '/resources/O1CN01dtjMvv1heoyqst9u5_!!6000000004303-55-tps-56-56.svg',
  devMode: 'procode',
  group: '高级组件',
  category: '表格类',
  tags: ['业务组件'],
  npm: {
    package: '@alifd/fusion-ui',
    version: '1.0.24-21',
    exportName: 'CospandTable',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        type: 'group',
        title: '展开行渲染',
        name: '展开行渲染',
        extraProps: {
          display: 'accordion'
        },
        items: [
          {
            title: '展开内容',
            name: 'children',
            setter: {
              componentName: 'SlotSetter',
              initialValue: {
                type: 'JSSlot',
                value: [{
                  componentName: 'EditTable',
                  props: {
                    columns: [
                      {
                        title: '字段',
                        dataIndex: 'field',
                        formatType: 'text'
                      },{
                        title: '值',
                        dataIndex: 'value',
                        formatType: 'text'
                      },{
                        title: '说明',
                        dataIndex: 'description',
                        formatType: 'text'
                      }
                    ]
                  }
                }]
              }
            }
          },
          {
            title: '渲染字段',
            name: 'children_field',
            setter: 'StringSetter',
            defaultValue: 'value',
          }
        ]
      },
      ...TableProps
    ],
    component: {
      isContainer: false,
      nestingRule: {},
    },
    supports: {
      style:true,
      events: ['onChange', 'onInit'],
    },
  },
  snippets: [
    {
      title: '嵌套表格',
      screenshot: '/resources/O1CN01dtjMvv1heoyqst9u5_!!6000000004303-55-tps-56-56.svg',
      schema: {
        componentName: 'CospandTable',
        props: {
          children_field:'value',
          dataSource: [
            {
              id: 'id-2f5DdE2b-0',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司1',
              children: [
                { id:1, field:'field1', value: 'value1', description:'这个是第一条数据'},
                { id:2, field:'field2', value: 'value2', description:'这个是第二条数据'},
                { id:3, field:'field3', value: 'value3', description:'这个是第三条数据'}
              ]
            },
            {
              id: 'id-2f5DdE2b-1',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司2',
              children: [
                { id:1, field:'field1', value: 'value1', description:'第一个'},
                { id:2, field:'field2', value: 'value2', description:'第二个'},
                { id:3, field:'field3', value: 'value3', description:'第三个'}
              ]
            },
            {
              id: 'id-2f5DdE2b-2',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司3',
              children: [
                { id:1, field:'field1', value: 'value1', description:'这个是第一条数据'},
                { id:2, field:'field2', value: 'value2', description:'这个是第二条数据'},
                { id:3, field:'field3', value: 'value3', description:'这个是第三条数据'}
              ]
            },
            {
              id: 'id-2f5DdE2b-3',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司4',
              children: [
                { id:1, field:'field1', value: 'value1', description:'这个是第一条数据'},
                { id:2, field:'field2', value: 'value2', description:'这个是第二条数据'},
                { id:3, field:'field3', value: 'value3', description:'这个是第三条数据'}
              ]
            },
            {
              id: 'id-2f5DdE2b-4',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司5',
              children: [
                { id:1, field:'field1', value: 'value1', description:'这个是第一条数据'},
                { id:2, field:'field2', value: 'value2', description:'这个是第二条数据'},
                { id:3, field:'field3', value: 'value3', description:'这个是第三条数据'}
              ]
            },
          ],
          actionColumnButtons: {
            text: true,
            visibleButtonCount: 3,
          },
          actionBarButtons: {
            dataSource: [
              {
                type: 'primary',
                children: '操作一',
              },
              {
                type: 'normal',
                children: '操作二',
              },
            ],
            visibleButtonCount: 3,
          },
          paginationProps: {
            pageSize: 20,
            current: 1,
          },
          settingButtons: true,
          columns: [
            {
              title: '公司',
              dataIndex: 'company',
              width: 160,
              formatType: 'link',
              searchable: true,
            },
            {
              title: '单据金额',
              dataIndex: 'documentAmount',
              formatType: 'money',
            },
            {
              title: '币种',
              dataIndex: 'currency',
              formatType: 'currency',
              filters: [
                {
                  label: 'CNY',
                  value: 'CNY',
                },
                {
                  label: 'USD',
                  value: 'USD',
                },
                {
                  label: 'JPY',
                  value: 'JPY',
                },
                {
                  label: 'HKD',
                  value: 'HKD',
                },
              ],
              filterMode: 'multiple',
              explanation: '提示信息',
              width: 110,
            },
            {
              title: '完成进度',
              dataIndex: 'percent',
              formatType: 'progress',
            },
            {
              title: '到账日期',
              dataIndex: 'date',
              formatType: 'date',
            },
          ],
        },
      },
    },
  ],
};

export default Meta;
