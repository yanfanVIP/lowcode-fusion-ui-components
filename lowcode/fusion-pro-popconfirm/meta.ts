import snippets from './snippets'

export default {
  snippets,
  componentName: 'FusionProPopconfirm',
  title: '高级气泡框',
  group: '高级组件',
  category: 'AntDesignPro',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: 'FusionProPopconfirm',
    main: 'lib/index.js',
    destructuring: true,
    subName: ''
  },
  props: [
    {
      name: 'title',
      title: '标题',
      setter: 'StringSetter'
    },{
      name: 'description',
      title: '详细描述',
      setter: 'StringSetter'
    },{
      name: 'icon',
      title: '图标',
      setter: 'StringSetter'
    },{
      name: 'okType',
      title: '确认按钮类型',
      setter: 'StringSetter'
    },{
      name: 'okText',
      title: '确认按钮文字',
      setter: 'StringSetter'
    }, {
      name: 'okButtonProps',
      title: '确认按钮配置',
      setter: {
        componentName: 'ObjectSetter',
        props: {
          config: {
            items: [
              {
                name: 'id',
                condition: () => false,
                setter: (target) => {
                  if (!target.getValue()) {
                    target.setValue(`${target.id}`);
                  }
                  return 'StringSetter';
                },
              },
              {
                name: 'content',
                display: 'inline',
                title: '文本',
                setter: 'StringSetter',
                important: true,
              },
              {
                "name": "size",
                "title": {
                  "label": {
                    "type": "i18n",
                    "zh-CN": "按钮尺寸",
                    "en-US": "Button Size"
                  },
                  "tip": {
                    "type": "i18n",
                    "zh-CN": "属性: size | 说明: 按钮尺寸",
                    "en-US": "prop: size | description: button size"
                  }
                },
                "setter": {
                  "componentName": "SelectSetter",
                  "props": {
                    "options": [
                      {
                        "title": "small",
                        "value": "small"
                      },
                      {
                        "title": "medium",
                        "value": "medium"
                      },
                      {
                        "title": "large",
                        "value": "large"
                      }
                    ]
                  }
                },
                "description": "按钮的尺寸",
                "defaultValue": "medium"
              },{
                "name": "shape",
                "title": '按钮形状',
                "setter": {
                  "componentName": "SelectSetter",
                  "props": {
                    "options": [
                      {
                        "title": "default",
                        "value": "default"
                      },
                      {
                        "title": "circle",
                        "value": "circle"
                      },
                      {
                        "title": "round",
                        "value": "round"
                      }
                    ]
                  }
                },
                "description": "按钮形状",
                "defaultValue": "default"
              },
              {
                "name": "type",
                "title": {
                  "label": {
                    "type": "i18n",
                    "zh-CN": "按钮类型",
                    "en-US": "Button Type"
                  },
                  "tip": {
                    "type": "i18n",
                    "zh-CN": "属性: type | 说明: 按钮类型",
                    "en-US": "prop: type | description: button type"
                  }
                },
                "setter": {
                  "componentName": "SelectSetter",
                  "props": {
                    "options": [
                      {
                        "title": "primary",
                        "value": "primary"
                      },
                      {
                        "title": "dashed",
                        "value": "dashed"
                      },
                      {
                        "title": "link",
                        "value": "link"
                      },
                      {
                        "title": "text",
                        "value": "text"
                      },
                      {
                        "title": "default",
                        "value": "default"
                      }
                    ]
                  }
                },
                "description": "按钮的类型",
                "defaultValue": "primary"
              },
              {
                "name": "ghost",
                "title": {
                  "type": "i18n",
                  "zh-CN": "幽灵按钮",
                  "en-US": "ghost"
                },
                "setter": 'BoolSetter',
                "description": "是否为幽灵按钮",
                "defaultValue": false
              },
              {
                "name": "danger",
                "title": "是否为警告按钮",
                initialValue: false,
                setter: 'BoolSetter',
                "description": "是否为警告按钮",
                "defaultValue": false
              },
              {
                "name": "bgColor",
                "title": "按钮颜色",
                "setter": {
                  "componentName": "ColorSetter"
                }
              },{
                "name": "fontColor",
                "title": "文字颜色",
                "setter": {
                  "componentName": "ColorSetter"
                }
              },
              {
                "name": "icon",
                "title": "图标",
                "setter": "StringSetter"
              },
              {
                name: 'action',
                display: 'inline',
                title: '操作',
                important: true,
                setValue: (target, value: any) => {
                  const actionNameMap: any = {
                    submit: '提交',
                    reset: '重置',
                    custom: '自定义',
                  };
                  const actionName = actionNameMap[value] || '自定义';
                  target.parent.setPropValue('content', actionName);
                },
                setter: {
                  componentName: 'SelectSetter',
                  props: {
                    options: [
                      {
                        title: '提交',
                        value: 'submit',
                      },
                      {
                        title: '重置',
                        value: 'reset',
                      },
                      {
                        title: '自定义',
                        value: 'custom',
                      },
                    ],
                  },
                },
              },
              {
                name: 'onClick',
                display: 'inline',
                title: '点击事件',
                condition: (target) => {
                  const action = target.parent.getPropValue('action');
                  return !action || action === 'custom';
                },
                setter: 'FunctionSetter',
                extraProps: {
                  supportVariable: true,
                },
              },
              {
                name: 'htmlType',
                condition: () => false,
              }
            ],
          },
        },
        initialValue: () => {
          return {
            content: '提交',
            action: 'submit',
            type: 'normal',
          };
        },
      },
    },{
      name: 'showCancel',
      title: '显示取消按钮',
      setter: 'BoolSetter',
      defaultValue: true,
    },{
      name: 'cancelText',
      title: '取消按钮文字',
      setter: 'StringSetter'
    },{
      name: 'cancelButtonProps',
      title: '取消按钮配置',
      setter: {
        componentName: 'ObjectSetter',
        props: {
          config: {
            items: [
              {
                name: 'id',
                condition: () => false,
                setter: (target) => {
                  if (!target.getValue()) {
                    target.setValue(`${target.id}`);
                  }
                  return 'StringSetter';
                },
              },
              {
                name: 'content',
                display: 'inline',
                title: '文本',
                setter: 'StringSetter',
                important: true,
              },
              {
                "name": "size",
                "title": {
                  "label": {
                    "type": "i18n",
                    "zh-CN": "按钮尺寸",
                    "en-US": "Button Size"
                  },
                  "tip": {
                    "type": "i18n",
                    "zh-CN": "属性: size | 说明: 按钮尺寸",
                    "en-US": "prop: size | description: button size"
                  }
                },
                "setter": {
                  "componentName": "SelectSetter",
                  "props": {
                    "options": [
                      {
                        "title": "small",
                        "value": "small"
                      },
                      {
                        "title": "medium",
                        "value": "medium"
                      },
                      {
                        "title": "large",
                        "value": "large"
                      }
                    ]
                  }
                },
                "description": "按钮的尺寸",
                "defaultValue": "medium"
              },{
                "name": "shape",
                "title": '按钮形状',
                "setter": {
                  "componentName": "SelectSetter",
                  "props": {
                    "options": [
                      {
                        "title": "default",
                        "value": "default"
                      },
                      {
                        "title": "circle",
                        "value": "circle"
                      },
                      {
                        "title": "round",
                        "value": "round"
                      }
                    ]
                  }
                },
                "description": "按钮形状",
                "defaultValue": "default"
              },
              {
                "name": "type",
                "title": {
                  "label": {
                    "type": "i18n",
                    "zh-CN": "按钮类型",
                    "en-US": "Button Type"
                  },
                  "tip": {
                    "type": "i18n",
                    "zh-CN": "属性: type | 说明: 按钮类型",
                    "en-US": "prop: type | description: button type"
                  }
                },
                "setter": {
                  "componentName": "SelectSetter",
                  "props": {
                    "options": [
                      {
                        "title": "primary",
                        "value": "primary"
                      },
                      {
                        "title": "dashed",
                        "value": "dashed"
                      },
                      {
                        "title": "link",
                        "value": "link"
                      },
                      {
                        "title": "text",
                        "value": "text"
                      },
                      {
                        "title": "default",
                        "value": "default"
                      }
                    ]
                  }
                },
                "description": "按钮的类型",
                "defaultValue": "primary"
              },
              {
                "name": "ghost",
                "title": {
                  "type": "i18n",
                  "zh-CN": "幽灵按钮",
                  "en-US": "ghost"
                },
                "setter": 'BoolSetter',
                "description": "是否为幽灵按钮",
                "defaultValue": false
              },
              {
                "name": "danger",
                "title": "是否为警告按钮",
                initialValue: false,
                setter: 'BoolSetter',
                "description": "是否为警告按钮",
                "defaultValue": false
              },
              {
                "name": "bgColor",
                "title": "按钮颜色",
                "setter": {
                  "componentName": "ColorSetter"
                }
              },{
                "name": "fontColor",
                "title": "文字颜色",
                "setter": {
                  "componentName": "ColorSetter"
                }
              },
              {
                "name": "icon",
                "title": "图标",
                "setter": "StringSetter"
              },
              {
                name: 'action',
                display: 'inline',
                title: '操作',
                important: true,
                setValue: (target, value: any) => {
                  const actionNameMap: any = {
                    submit: '提交',
                    reset: '重置',
                    custom: '自定义',
                  };
                  const actionName = actionNameMap[value] || '自定义';
                  target.parent.setPropValue('content', actionName);
                },
                setter: {
                  componentName: 'SelectSetter',
                  props: {
                    options: [
                      {
                        title: '提交',
                        value: 'submit',
                      },
                      {
                        title: '重置',
                        value: 'reset',
                      },
                      {
                        title: '自定义',
                        value: 'custom',
                      },
                    ],
                  },
                },
              },
              {
                name: 'onClick',
                display: 'inline',
                title: '点击事件',
                condition: (target) => {
                  const action = target.parent.getPropValue('action');
                  return !action || action === 'custom';
                },
                setter: 'FunctionSetter',
                extraProps: {
                  supportVariable: true,
                },
              },
              {
                name: 'htmlType',
                condition: () => false,
              }
            ],
          },
        },
        initialValue: () => {
          return {
            content: '提交',
            action: 'submit',
            type: 'normal',
          };
        },
      },
    },{
      name: 'disabled',
      title: '禁用',
      setter: 'BoolSetter',
      defaultValue: false,
    },
  ],
  configure: {
    component: { 
      isContainer: true
    },
    supports: {
      events: ['onCancel', 'onConfirm', 'onPopupClick']
    }
  }
}
