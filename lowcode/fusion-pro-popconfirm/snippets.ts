export default [
  {
    title: '高级气泡框',
    screenshot: '/resources/FusionProPopconfirm.svg',
    schema: {
      componentName: 'FusionProPopconfirm',
      props: {
        title: '确定删除?',
        okType: 'primary',
        okText: '确定',
        cancelText: '取消',
        style:{
          width:0
        }
      },
      children: {
        componentName: 'Button',
        props: {
          children: '删除'
        }
      }
    }
  }
]
