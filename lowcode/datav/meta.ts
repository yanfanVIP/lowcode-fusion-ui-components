import FullScreenContainer from './components/FullScreenContainer';
import Loading from './components/Loading';
import BorderBox from './components/BorderBox';
import Decoration from './components/Decoration';
import ActiveRingChart from './components/ActiveRingChart';
import CapsuleChart from './components/CapsuleChart';
import WaterLevelPond from './components/WaterLevelPond';
import PercentPond from './components/PercentPond';
import FlylineChartEnhanced from './components/FlylineChartEnhanced';
import ConicalColumnChart from './components/ConicalColumnChart';
import DigitalFlop from './components/DigitalFlop';
import ScrollBoard from './components/ScrollBoard';
import ScrollRankingBoard from './components/ScrollRankingBoard';


export default [
  ...FullScreenContainer,
  ...Loading,
  ...BorderBox,
  ...Decoration,
  ...ActiveRingChart,
  ...CapsuleChart,
  ...WaterLevelPond,
  ...PercentPond,
  ...FlylineChartEnhanced,
  ...ConicalColumnChart,
  ...DigitalFlop,
  ...ScrollBoard,
  ...ScrollRankingBoard,
];
