const componentName = 'FullScreenContainer'
const title = '全屏容器'

const Meta = {
  componentName: `FusionDatav.${componentName}`,
  title: title,
  category: 'Datav',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionDatav',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
    props:[
      {
        name: 'children',
        title: '内容',
        condition: ()=> false,
        setter: 'SlotSetter',
        initialValue: {
          type: 'JSSlot',
          value: []
        }
      }
    ]
  }
}

const snippets = [
  {
    title: title,
    screenshot: `/resources/FusionDatav.${componentName}.svg`,
    schema: {
      componentName: `FusionDatav.${componentName}`,
      props: {
        component: componentName,
        style: {
          height: '100%'
        }
      }
    },
  },
];

export default [
  { ...Meta, snippets }
];