const componentName = 'WaterLevelPond'
const title = '水位图'

const Meta = {
  componentName: `FusionDatav.${componentName}`,
  title: title,
  category: 'Datav',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionDatav',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
    props:[
      {
        name: 'config',
        title: '内容',
        setter: 'JsonSetter'
      }
    ]
  }
}


const snippets = [
  {
    title: title,
    screenshot: `/resources/FusionDatav.${componentName}.svg`,
    schema: {
      componentName: `FusionDatav.${componentName}`,
      props: {
        component: componentName,
        config: {
          data: [66]
        },
        style: {
          width: '150px',
          height: '200px',
        },
      }
    },
  },
];

export default [
  { ...Meta, snippets }
];