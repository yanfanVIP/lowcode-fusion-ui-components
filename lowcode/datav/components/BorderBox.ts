const componentName = 'BorderBox'
const title = '边框容器'

const Meta = {
  componentName: `FusionDatav.${componentName}`,
  title: title,
  category: 'Datav',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionDatav',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      events: ['onClickTitle', 'onClick'],
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
    props:[
      {
        "name": "component",
        "title": "边框类型",
        "setter": {
          componentName: 'SelectSetter',
          props: {
            options: [
              {
                title: 'BorderBox1',
                value: 'BorderBox1',
              },
              {
                title: 'BorderBox2',
                value: 'BorderBox2',
              },
              {
                title: 'BorderBox3',
                value: 'BorderBox3',
              },
              {
                title: 'BorderBox4',
                value: 'BorderBox4',
              },
              {
                title: 'BorderBox5',
                value: 'BorderBox5',
              },
              {
                title: 'BorderBox6',
                value: 'BorderBox6',
              },
              {
                title: 'BorderBox7',
                value: 'BorderBox7',
              },
              {
                title: 'BorderBox8',
                value: 'BorderBox8',
              },
              {
                title: 'BorderBox9',
                value: 'BorderBox9',
              },
              {
                title: 'BorderBox10',
                value: 'BorderBox10',
              },
              {
                title: 'BorderBox11',
                value: 'BorderBox11',
              },
              {
                title: 'BorderBox12',
                value: 'BorderBox12',
              },
              {
                title: 'BorderBox13',
                value: 'BorderBox13',
              }
            ],
          },
        }
      },
      {
        name: 'title',
        title: '标题',
        condition: (target)=> target.getProps().getPropValue('component') == 'BorderBox11',
        setter: "StringSetter"
      },
      {
        name: 'titleWidth',
        title: '标题宽度',
        condition: (target)=> target.getProps().getPropValue('component') == 'BorderBox11',
        setter: "NumberSetter"
      },
      {
        name: 'titleColor',
        title: '标题颜色',
        condition: (target)=> target.getProps().getPropValue('component') == 'BorderBox11',
        setter: "ColorSetter"
      },
      {
        name: 'dur',
        title: '动画时长',
        condition: (target)=> target.getProps().getPropValue('component') == 'BorderBox8',
        setter: "NumberSetter"
      },
      {
        name: 'children',
        title: '内容',
        setter: 'SlotSetter',
        condition: ()=> false,
        initialValue: {
          componentName: "FusionTypography",
          props: {
            type: 'H1',
            value: '内容'
          }
        }
      },
      {
        name: 'backgroundColor',
        title: '背景色',
        setter: "ColorSetter"
      },
      {
        name: 'color',
        title: '颜色',
        setter: {
          "componentName": "ArraySetter",
          "props": {
            "itemSetter": {
              "componentName": "ColorSetter"
            }
          }
        }
      },
      {
        name: 'reverse',
        title: '翻转',
        condition: (target)=> ['BorderBox4', 'BorderBox5', 'BorderBox8'].includes(target.getProps().getPropValue('component')),
        setter: 'BoolSetter'
      },
      {
        name: 'contentStyle',
        title: '内容样式',
        display: 'block',
        setter: "StyleSetter"
      }
    ]
  }
}


const snippets = [
  {
    title: title,
    screenshot: `/resources/FusionDatav.${componentName}.svg`,
    schema: {
      componentName: `FusionDatav.${componentName}`,
      props: {
        component: "BorderBox1",
        style: {
          height: '400px'
        }
      },
      children: [{
        componentName: "FusionTypography",
        props: {
          type: 'H1',
          value: '内容'
        }
      }]
    },
  },
];

export default [
  { ...Meta, snippets }
];