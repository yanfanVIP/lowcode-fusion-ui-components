const componentName = 'ScrollBoard'
const title = '轮播表'

const Meta = {
  componentName: `FusionDatav.${componentName}`,
  title: title,
  category: 'Datav',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionDatav',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
    props:[
      {
        name: 'config',
        title: '内容',
        setter: 'JsonSetter'
      }
    ]
  }
}


const snippets = [
  {
    title: title,
    screenshot: `/resources/FusionDatav.${componentName}.svg`,
    schema: {
      componentName: `FusionDatav.${componentName}`,
      props: {
        component: componentName,
        config: {
          data: [
            ['行1列1', '行1列2', '行1列3'],
            ['行2列1', '行2列2', '行2列3'],
            ['行3列1', '行3列2', '行3列3'],
            ['行4列1', '行4列2', '行4列3'],
            ['行5列1', '行5列2', '行5列3'],
            ['行6列1', '行6列2', '行6列3'],
            ['行7列1', '行7列2', '行7列3'],
            ['行8列1', '行8列2', '行8列3'],
            ['行9列1', '行9列2', '行9列3'],
            ['行10列1', '行10列2', '行10列3']
          ]
        },
        style: {
          height: '400px'
        },
      }
    },
  },
];

export default [
  { ...Meta, snippets }
];