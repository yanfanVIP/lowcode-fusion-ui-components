const componentName = 'ConicalColumnChart'
const title = '锥形柱图'

const Meta = {
  componentName: `FusionDatav.${componentName}`,
  title: title,
  category: 'Datav',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionDatav',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
    props:[
      {
        name: 'config',
        title: '内容',
        setter: 'JsonSetter'
      }
    ]
  }
}


const snippets = [
  {
    title: title,
    screenshot: `/resources/FusionDatav.${componentName}.svg`,
    schema: {
      componentName: `FusionDatav.${componentName}`,
      props: {
        component: componentName,
        config: {
          data: [
            {
              name: '周口',
              value: 55
            },
            {
              name: '南阳',
              value: 120
            },
            {
              name: '西峡',
              value: 71
            },
            {
              name: '驻马店',
              value: 66
            },
            {
              name: '新乡',
              value: 80
            },
            {
              name: '信阳',
              value: 35
            },
            {
              name: '漯河',
              value: 15
            }
          ],
          img: [
            '/resources/conicalColumnChart/1st.png',
            '/resources/conicalColumnChart/2st.png',
            '/resources/conicalColumnChart/3st.png',
            '/resources/conicalColumnChart/4st.png',
            '/resources/conicalColumnChart/5st.png',
            '/resources/conicalColumnChart/6st.png',
            '/resources/conicalColumnChart/7st.png'
          ],
          showValue: true
        },
        style: {
          height: '400px'
        },
      }
    },
  },
];

export default [
  { ...Meta, snippets }
];