const componentName = 'Decoration'
const title = '装饰'

const Meta = {
  componentName: `FusionDatav.${componentName}`,
  title: title,
  category: 'Datav',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionDatav',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      events: ['onClickTitle', 'onClick'],
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
    props:[
      {
        "name": "component",
        "title": "装饰类型",
        "setter": {
          componentName: 'SelectSetter',
          props: {
            options: [
              {
                title: 'Decoration1',
                value: 'Decoration1',
              },
              {
                title: 'Decoration2',
                value: 'Decoration2',
              },
              {
                title: 'Decoration3',
                value: 'Decoration3',
              },
              {
                title: 'Decoration4',
                value: 'Decoration4',
              },
              {
                title: 'Decoration5',
                value: 'Decoration5',
              },
              {
                title: 'Decoration6',
                value: 'Decoration6',
              },
              {
                title: 'Decoration7',
                value: 'Decoration7',
              },
              {
                title: 'Decoration8',
                value: 'Decoration8',
              },
              {
                title: 'Decoration9',
                value: 'Decoration9',
              },
              {
                title: 'Decoration10',
                value: 'Decoration10',
              },
              {
                title: 'Decoration11',
                value: 'Decoration11',
              },
              {
                title: 'Decoration12',
                value: 'Decoration12',
              }
            ],
          },
        }
      },
      {
        name: 'title',
        title: '内容',
        condition: (target)=> ['Decoration7','Decoration9', 'Decoration11'].includes(target.getProps().getPropValue('component')),
        setter: "StringSetter"
      },
      {
        name: 'titleColor',
        title: '标题颜色',
        condition: (target)=> ['Decoration7','Decoration9', 'Decoration11'].includes(target.getProps().getPropValue('component')),
        setter: "ColorSetter"
      },
      {
        name: 'dur',
        title: '动画时长',
        condition: (target)=> target.getProps().getPropValue('component') == 'Decoration9',
        setter: "NumberSetter"
      },
      {
        name: 'reverse',
        title: '翻转',
        condition: (target)=> ['Decoration2', 'Decoration4', 'Decoration8'].includes(target.getProps().getPropValue('component')),
        setter: 'BoolSetter'
      },
      {
        name: 'color',
        title: '颜色',
        setter: {
          "componentName": "ArraySetter",
          "props": {
            "itemSetter": {
              "componentName": "ColorSetter"
            }
          }
        }
      }
    ]
  }
}


const snippets = [
  {
    title: title,
    screenshot: `/resources/FusionDatav.${componentName}.svg`,
    schema: {
      componentName: `FusionDatav.${componentName}`,
      props: {
        component: "Decoration1",
        style: {
          height: '400px'
        }
      }
    },
  },
];

export default [
  { ...Meta, snippets }
];