const componentName = 'CapsuleChart'
const title = '胶囊柱图'

const Meta = {
  componentName: `FusionDatav.${componentName}`,
  title: title,
  category: 'Datav',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionDatav',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
    props:[
      {
        name: 'config',
        title: '内容',
        setter: 'JsonSetter'
      }
    ]
  }
}


const snippets = [
  {
    title: title,
    screenshot: `/resources/FusionDatav.${componentName}.svg`,
    schema: {
      componentName: `FusionDatav.${componentName}`,
      props: {
        component: componentName,
        config: {
          data: [
            {
              name: '南阳',
              value: 167
            },
            {
              name: '周口',
              value: 123
            },
            {
              name: '漯河',
              value: 98
            },
            {
              name: '郑州',
              value: 75
            },
            {
              name: '西峡',
              value: 66
            },
          ],
          colors: ['#e062ae', '#fb7293', '#e690d1', '#32c5e9', '#96bfff'],
          unit: '单位',
          showValue: true
        },
        style: {
          height: '400px'
        },
      }
    },
  },
];

export default [
  { ...Meta, snippets }
];