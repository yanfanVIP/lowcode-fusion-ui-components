import { ProTableProps } from '../pro-table/pro-table-meta';
import { mockProTableRow, mockId, getDataSourceItemSetter } from '../pro-table/utils';
import { isJSExpression } from '../utils';

const TableProps = ProTableProps.map((item) => {
  if (item.name === 'dataSource') {
    return {
      type: 'field',
      name: 'dataSource',
      title: '表格数据源',
      display: 'accordion',
      setter: (target) => {
        const columns = target.getProps().getPropValue('columns');
        if (!columns || isJSExpression(columns)) {
          return {
            componentName: 'ExpressionSetter',
          };
        }
        const mockRow = mockProTableRow(columns);
        const primaryKey = target.getProps().getPropValue('primaryKey') || 'id';
  
        const items = columns.map((column, index) => {
          return {
            title: {
              label: {
                type: 'i18n',
                'en-US': column.dataIndex,
                'zh-CN': column.title,
              },
            },
            name: column.dataIndex,
            important: index < 2,
            setter: getDataSourceItemSetter(column.formatType),
            defaultValue: mockRow[column.dataIndex],
          };
        });
        return {
          componentName: 'MixedSetter',
          props: {
            setters: [
              {
                componentName: 'ArraySetter',
                props: {
                  itemSetter: {
                    componentName: 'ObjectSetter',
                    props: {
                      config: {
                        items,
                      },
                    },
                    initialValue: () => {
                      return {
                        ...mockRow,
                        [primaryKey]: mockId(),
                      };
                    },
                  },
                },
              },
              'ExpressionSetter',
            ],
          },
        };
      },
    };
  }
  if (item.name === 'advanced') {
    return {
      type: 'group',
      title: '高级',
      name: 'other',
      extraProps: {
        display: 'accordion',
        defaultCollapsed: true,
      },
      items: [
        {
          name: 'sortColumn',
          title: '排序栏',
          setter: "BoolSetter"
        },
        {
          type: 'field',
          name: '!选择模式',
          title: '选择模式',
          display: 'inline',
          setter: {
            componentName: 'RadioGroupSetter',
            props: {
              options: [
                {
                  title: '无',
                  value: 'none',
                  tip: 'none',
                },
                {
                  title: '多选',
                  value: 'multiple',
                  tip: 'multiple',
                },
                {
                  title: '单选',
                  value: 'single',
                  tip: 'single',
                },
              ],
            },
          },
          defaultValue: 'none',
          getValue: (target) => {
            const rowSelection = target.parent.getPropValue('rowSelection');
            if (!rowSelection) {
              return 'none';
            }
            return rowSelection.mode === 'single' ? 'single' : 'multiple';
          },
          setValue: (field, value) => {
            const { node } = field;
            if (['single', 'multiple'].includes(value)) {
              node.setPropValue('rowSelection', {
                ...node.getPropValue('rowSelection'),
                mode: value,
              });
            } else {
              node.setPropValue('rowSelection', undefined);
            }
          },
        },
        {
          name: 'tableLayout',
          title: '表格布局方式',
          setter: {
            componentName: 'RadioGroupSetter',
            props: {
              options: [
                {
                  title: 'fixed',
                  value: 'fixed'
                },
                {
                  title: 'auto',
                  value: 'auto'
                },
              ],
            },
          },
        },
        {
          name: 'useVirtual',
          title: '开启虚拟滚动',
          setter: 'BoolSetter'
        },
        {
          name: 'fixedHeader',
          title: '表头固定',
          setter: 'BoolSetter'
        },
        {
          name: 'maxBodyHeight',
          title: '内容高度',
          setter: ['NumberSetter', 'StringSetter']
        },{
          name: 'stickyHeader',
          title: '表头sticky',
          setter: 'BoolSetter'
        },
        {
          type: 'field',
          name: 'indexColumn',
          title: '开启序号列',
          extraProps: {
            display: 'inline',
            defaultValue: false,
          },
          setter: {
            componentName: 'BoolSetter',
          },
        },
        {
          type: 'field',
          name: 'settingButtons',
          title: '开启设置按钮',
          extraProps: {
            display: 'inline',
            defaultValue: false,
          },
          setter: {
            componentName: 'BoolSetter',
          },
        },
        {
          type: 'field',
          name: 'primaryKey',
          title: {
            label: '数据主键',
            tip: '数据主键用于区分数据中不同的行，对行选择和行编辑功能非常重要，不同的行主键值不可重复，一般采用数据库中自增 ID 字段',
          },
          extraProps: {
            display: 'inline',
            defaultValue: 'id',
            condition: () => false,
          },
        },
        {
          name: 'cellDefault',
          title: {
            label: '单元格缺省填充',
            tip: '当单元格值为空时，显示内容',
          },
          setter: 'StringSetter',
        },
      ],
    };
  }
  return { ...item };
});

export const Meta = {
  componentName: 'SelectFromTable',
  isFormItemComponent: true,
  title: '选择表格',
  docUrl: '',
  icon: '',
  category: '内容',
  screenshot: '',
  npm: {
    package: '@alifd/fusion-ui',
    version: '1.0.24-21',
    exportName: 'SelectFromTable',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      ...TableProps
    ],
    supports: {
      style:true,
      events: ['onChange', 'onInit'],
    },
  }
};

export default Meta;
