import { IPublicTypeConfigure } from '@alilc/lowcode-types';

export const plotConfigure: NonNullable<IPublicTypeConfigure['props']> = [
  {
    name: 'data',
    type: 'group',
    display: 'accordion',
    title: {
      label: '展示设置',
    },
    items: [
      {
        name: 'title',
        title: '图表标题',
        defaultValue: '图表',
        setter: 'StringSetter',
      },
      {
        name: 'autoFit',
        title: '自适应宽高',
        defaultValue: true,
        setter: 'BoolSetter',
      },
      {
        name: 'height',
        title: '高度',
        defaultValue: 300,
        setter: {
          componentName: 'NumberSetter',
        },
      },{
        name: 'pixelRatio',
        title: '图表渲染像素比',
        defaultValue: 2,
        setter: 'NumberSetter',
      },{
        name: 'renderer',
        title: '渲染方式',
        defaultValue: 'canvas',
        setter: {
          componentName: 'RadioGroupSetter',
          props: {
            options: [
              { value: 'canvas', title: 'canvas' },
              { value: 'svg', title: 'svg' }
            ],
          },
        },
      },
    ]
  }
];
