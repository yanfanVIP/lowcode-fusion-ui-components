import { IProps } from '../types';
import { hideProp } from '../utils';
import { IPublicModelSettingField } from '@alilc/lowcode-types';

export const operationConfig: IProps = {
  name: 'operationConfig',
  display: 'accordion',
  title: '底部操作',
  setter: {
    componentName: 'ObjectSetter',
    props: {
      config: {
        items: [
          {
            name: 'visibleButtonCount',
            title: {
              label: '可见数量',
              tip: '超过会收起到”更多“菜单中',
            },
            extraProps: {
              defaultValue: 3,
            },
            setter: {
              componentName: 'NumberSetter',
              props: {
                max: 6,
                min: 1,
              },
            },
          },
          {
            name: 'fixed',
            title: '吸底',
            setter: 'BoolSetter',
          },
          {
            name: 'showSaveTime',
            title: '显示时间',
            setter: {
              componentName: 'BoolSetter',
            },
          },
          {
            name: 'align',
            title: '布局',
            defaultValue: 'center',
            setter: {
              componentName: 'RadioGroupSetter',
              props: {
                options: [
                  {
                    title: '居左',
                    value: 'left',
                  },
                  {
                    title: '居中',
                    value: 'center',
                  },
                  {
                    title: '居右',
                    value: 'right',
                  },
                ],
              },
            },
          },
        ],
      },
    },
  },
};

export const operations: IProps = {
  name: 'operations',
  display: 'block',
  title: '操作项',
  getValue: (target, value) => {
    return value || [];
  },
  setter: {
    componentName: 'MixedSetter',
    props: {
      setters: [
        {
          componentName: 'SlotSetter',
          defaultValue: {
            type: 'JSSlot',
            value: [],
          },
        },
        {
          componentName: 'ArraySetter',
          props: {
            itemSetter: {
              componentName: 'ObjectSetter',
              props: {
                config: {
                  items: [
                    {
                      name: 'id',
                      condition: hideProp,
                      setter: (target: IPublicModelSettingField) => {
                        if (!target.getValue()) {
                          target.setValue(`${target.id}`);
                        }
                        return 'StringSetter';
                      },
                    },
                    {
                      name: 'content',
                      display: 'inline',
                      title: '文本',
                      setter: 'StringSetter',
                      important: true,
                    },
                    {
                      "name": "size",
                      "title": {
                        "label": {
                          "type": "i18n",
                          "zh-CN": "按钮尺寸",
                          "en-US": "Button Size"
                        },
                        "tip": {
                          "type": "i18n",
                          "zh-CN": "属性: size | 说明: 按钮尺寸",
                          "en-US": "prop: size | description: button size"
                        }
                      },
                      "setter": {
                        "componentName": "SelectSetter",
                        "props": {
                          "options": [
                            {
                              "title": "small",
                              "value": "small"
                            },
                            {
                              "title": "medium",
                              "value": "medium"
                            },
                            {
                              "title": "large",
                              "value": "large"
                            }
                          ]
                        }
                      },
                      "description": "按钮的尺寸",
                      "defaultValue": "medium"
                    },
                    {
                      "name": "type",
                      "title": {
                        "label": {
                          "type": "i18n",
                          "zh-CN": "按钮类型",
                          "en-US": "Button Type"
                        },
                        "tip": {
                          "type": "i18n",
                          "zh-CN": "属性: type | 说明: 按钮类型",
                          "en-US": "prop: type | description: button type"
                        }
                      },
                      "setter": {
                        "componentName": "SelectSetter",
                        "props": {
                          "options": [
                            {
                              "title": "primary",
                              "value": "primary"
                            },
                            {
                              "title": "secondary",
                              "value": "secondary"
                            },
                            {
                              "title": "normal",
                              "value": "normal"
                            }
                          ]
                        }
                      },
                      "description": "按钮的类型",
                      "defaultValue": "normal"
                    },
                    {
                      "name": "ghost",
                      "title": {
                        "type": "i18n",
                        "zh-CN": "幽灵按钮",
                        "en-US": "ghost"
                      },
                      "setter": {
                        "componentName": "SelectSetter",
                        "props": {
                          "options": [
                            {
                              "title": "True",
                              "value": true
                            },
                            {
                              "title": "False",
                              "value": false
                            },
                            {
                              "title": "Light",
                              "value": "light"
                            },
                            {
                              "title": "Dark",
                              "value": "dark"
                            }
                          ]
                        }
                      },
                      "description": "是否为幽灵按钮",
                      "defaultValue": false
                    },
                    {
                      "name": "warning",
                      "title": "是否为警告按钮",
                      initialValue: false,
                      setter: 'BoolSetter',
                      "description": "是否为警告按钮",
                      "defaultValue": false
                    },
                    {
                      "name": "bgColor",
                      "title": "按钮颜色",
                      "setter": {
                        "componentName": "ColorSetter"
                      }
                    },{
                      "name": "fontColor",
                      "title": "文字颜色",
                      "setter": {
                        "componentName": "ColorSetter"
                      }
                    },
                    {
                      "name": "icon",
                      "title": {
                        "label": {
                          "type": "i18n",
                          "zh-CN": "图标",
                          "en-US": "Icon"
                        },
                        "tip": {
                          "type": "i18n",
                          "zh-CN": "属性: icon | 说明: 图标类型",
                          "en-US": "prop: icon | description: icon type"
                        }
                      },
                      "setter": "IconSetter"
                    },
                    {
                      "name": "iconSize",
                      "title": {
                        "label": {
                          "type": "i18n",
                          "zh-CN": "图标尺寸",
                          "en-US": "Icon Size"
                        },
                        "tip": {
                          "type": "i18n",
                          "zh-CN": "属性: iconSize | 说明: 图标尺寸",
                          "en-US": "prop: iconSize | description: icon size"
                        }
                      },
                      "propType": {
                        "type": "oneOf",
                        "value": [
                          "xxs",
                          "xs",
                          "small",
                          "medium",
                          "large",
                          "xl",
                          "xxl",
                          "xxxl"
                        ]
                      },
                      setter: {
                        componentName: 'SelectSetter',
                        props: {
                          options: [
                            {
                              title: 'xxs',
                              value: 'xxs',
                            },
                            {
                              title: 'xs',
                              value: 'xs',
                            },
                            {
                              title: 'small',
                              value: 'small',
                            },{
                              title: 'medium',
                              value: 'medium',
                            },{
                              title: 'large',
                              value: 'large',
                            },{
                              title: 'xl',
                              value: 'xl',
                            },{
                              title: 'xxl',
                              value: 'xxl',
                            },{
                              title: 'xxxl',
                              value: 'xxxl',
                            }
                          ],
                        },
                      },
                      "defaultValue": "small",
                      "description": "按钮中 Icon 的尺寸，用于替代 Icon 的默认大小"
                    },
                    {
                      name: 'action',
                      display: 'inline',
                      title: '操作',
                      important: true,
                      setValue: (target: IPublicModelSettingField, value: any) => {
                        const actionNameMap: any = {
                          submit: '提交',
                          reset: '重置',
                          custom: '自定义',
                        };
                        const actionName = actionNameMap[value] || '自定义';
                        target.parent.setPropValue('content', actionName);
                      },
                      setter: {
                        componentName: 'SelectSetter',
                        props: {
                          options: [
                            {
                              title: '提交',
                              value: 'submit',
                            },
                            {
                              title: '重置',
                              value: 'reset',
                            },
                            {
                              title: '自定义',
                              value: 'custom',
                            },
                          ],
                        },
                      },
                    },
                    {
                      name: 'type',
                      display: 'inline',
                      title: '样式',
                      important: true,
                      setter: {
                        componentName: 'SelectSetter',
                        props: {
                          options: [
                            {
                              title: '主要',
                              value: 'primary',
                            },
                            {
                              title: '次要',
                              value: 'secondary',
                            },
                            {
                              title: '普通',
                              value: 'normal',
                            },
                          ],
                        },
                      },
                    },
                    {
                      name: 'onClick',
                      display: 'inline',
                      title: '点击事件',
                      condition: (target: IPublicModelSettingField) => {
                        const action = target.parent.getPropValue('action');
                        return !action || action === 'custom';
                      },
                      setter: 'FunctionSetter',
                      extraProps: {
                        supportVariable: true,
                      },
                    },
                    {
                      name: 'htmlType',
                      condition: hideProp,
                    }
                  ],
                },
              },
              initialValue: () => {
                return {
                  content: '提交',
                  type:'primary',
                  action:'custom'
                };
              },
            },
          },
        },
      ],
    },
  },
};

export const operationProps = [operationConfig, operations];
