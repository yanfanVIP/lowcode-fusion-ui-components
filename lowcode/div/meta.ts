const componentName = "FusionDiv"
const title = "DIV容器"
const screenshot = "/resources/div.svg"
const group = "高级组件"
const category = "布局容器类"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'id',
        title: 'ID',
        setter: 'StringSetter'
      },
    ],
    supports: {
      events: ['onClick'],
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    }
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title
    }
  },
];
export default {
  ...Meta,
  snippets
};
