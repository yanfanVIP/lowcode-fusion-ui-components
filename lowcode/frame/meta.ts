const componentName = "FusionFrame"
const title = "Frame"
const screenshot = "/resources/iframe.svg"
const group = "高级组件"
const category = "特殊类"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'url',
            title: '远程地址',
            setter: 'StringSetter'
          }
        ],
      }
    ],
    supports: {
      events: ['onLoad'],
      style: true,
    },
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title
    },
  },
];
export default {
  ...Meta,
  snippets
};
