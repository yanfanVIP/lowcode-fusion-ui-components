const componentName = "FusionIcon"
const title = "AntDesign图标"
const screenshot = "/resources/icon.svg"
const group = "高级组件"
const category = "内容"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'type',
        title: '类型',
        setter: 'StringSetter'
      },{
        name: 'rotate',
        title: '图标旋转角度',
        setter: 'NumberSetter'
      },{
        name: 'spin',
        title: '是否有旋转动画',
        setter: 'BoolSetter'
      },{
        name: 'twoToneColor',
        title: '设置双色图标的主要颜色',
        setter: 'StringSetter'
      },
    ],
    supports: {
      events: ['onClick'],
      style: true,
    },
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title,
      props: {
        type:'QuestionCircleTwoTone'
      },
    },
  },
];
export default {
  ...Meta,
  snippets
};
