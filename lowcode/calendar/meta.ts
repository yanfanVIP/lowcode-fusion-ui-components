const componentName = "FusionCalendar"
const title = "日历"
const screenshot = "/resources/calendar.svg"
const group = "高级组件"
const category = "内容"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'theme',
        title: '主题',
        setter: {
          componentName: 'RadioGroupSetter',
          props: { options: ['light', 'dark'] },
        },
        defaultValue: 'light',
      },
      {
        name: 'shape',
        title: '样式',
        setter: {
          componentName: 'RadioGroupSetter',
          props: { options: ['card', 'fullscreen', 'panel'] },
        },
        defaultValue: 'panel',
      },{
        name: 'showOtherMonth',
        title: '默认展示其他月份',
        setter: 'BoolSetter'
      },{
        name: 'eventFunction',
        title: '日期渲染',
        setter: 'FunctionSetter'
      },{
        name: 'disabledDate',
        title: '不可选择的日期',
        setter: 'FunctionSetter'
      }
    ],
    supports: {
      events: ['onSelect'],
      className: true,
      style: true,
    },
    component: {
      isContainer: false,
      isMinimalRenderUnit: true
    }
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      props: {
        style: {
          width: '100%',
          height : '100%',
          background: "transparent",
          theme:'light',
        }
      }
    }
  },
];
export default {
  ...Meta,
  snippets
};
