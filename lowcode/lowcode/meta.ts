const componentName = "FusionLowCode"
const title = "LOWCODE"
const screenshot = "/resources/lowcode.svg"
const group = "高级组件"
const category = "特殊类"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'config',
        title: '内容',
        setter: ['JsonSetter', 'StringSetter']
      }
    ],
    supports: {
      style: true,
    },
    component: {
      isContainer: false,
      isMinimalRenderUnit: true
    }
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title
    }
  },
];
export default {
  ...Meta,
  snippets
};
