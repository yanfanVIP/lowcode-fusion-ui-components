import { IComponentDescription } from '../types';

const CodeMeta: IComponentDescription = {
  componentName: 'CodeEditor',
  title: '代码编辑器',
  category: '内容',
  group: '高级组件',
  docUrl: '',
  screenshot: '/resources/code.svg',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: 'CodeEditor',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'height',
            title: '控件高度',
            setter: 'StringSetter'
          },
          {
            name: 'language',
            title: '语言',
            setter: 'StringSetter'
          },
          {
            name: 'theme',
            title: '主题',
            setter: 'StringSetter',
            defaultValue: 'vs-dark',
          },
          {
            name: 'enableOutline',
            title: '是否开启大纲',
            setter: 'BoolSetter',
            defaultValue: false,
          }
        ],
      },{
        name: '代码内容设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '内容',
        },
        items: [
          {
            name: 'value',
            title: '当前值',
            setter: ['StringSetter', 'ExpressionSetter'],
          }
        ],
      },
    ],
    supports: {
      events: ['editorDidMount', 'editorWillMount', 'onChange'],
      style: true,
    },
  },
};

const snippets = [
  {
    title: '代码编辑器',
    name: 'CodeEditor',
    screenshot: '/resources/code.svg',
    schema: {
      componentName: 'CodeEditor',
      title: '代码编辑器',
      props: {
        value: '//input your code to here',
        height: '200px',
        language: 'json'
      },
    },
  },
];
export default {
  ...CodeMeta,
  snippets
};
