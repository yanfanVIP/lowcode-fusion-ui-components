
const CodeMeta = {
  "componentName": "FusionButton",
  "title": "按钮",
  group: '高级组件',
  "category": "内容",
  "icon": "/resources/img.alicdn.com_tfs_TB1rT0gPQL0gK0jSZFAXXcA9pXa-200-200.svg",
  "docUrl": "",
  "screenshot": "",
  "npm": {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: 'FusionButton',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  "props": [
    {
      "name": "children",
      "title": "内容",
      "propType": "string"
    },
    {
      "name": "icon",
      "propType": "string",
      "description": "自定义内联样式"
    },
    {
      "name": "type",
      "title": {
        "label": {
          "type": "i18n",
          "zh-CN": "按钮类型",
          "en-US": "Button Type"
        },
        "tip": {
          "type": "i18n",
          "zh-CN": "属性: type | 说明: 按钮类型",
          "en-US": "prop: type | description: button type"
        }
      },
      "propType": {
        "type": "oneOf",
        "value": [
          "primary",
          "secondary",
          "normal"
        ]
      },
      "description": "按钮的类型",
      "defaultValue": "normal"
    },
    {
      "name": "size",
      "title": {
        "label": {
          "type": "i18n",
          "zh-CN": "按钮尺寸",
          "en-US": "Button Size"
        },
        "tip": {
          "type": "i18n",
          "zh-CN": "属性: size | 说明: 按钮尺寸",
          "en-US": "prop: size | description: button size"
        }
      },
      "propType": {
        "type": "oneOf",
        "value": [
          "small",
          "medium",
          "large"
        ]
      },
      "description": "按钮的尺寸",
      "defaultValue": "medium"
    },
    {
      "name": "iconSize",
      "title": {
        "label": {
          "type": "i18n",
          "zh-CN": "图标尺寸",
          "en-US": "Icon Size"
        },
        "tip": {
          "type": "i18n",
          "zh-CN": "属性: iconSize | 说明: 图标尺寸",
          "en-US": "prop: iconSize | description: icon size"
        }
      },
      "propType": {
        "type": "oneOf",
        "value": [
          "xxs",
          "xs",
          "small",
          "medium",
          "large",
          "xl",
          "xxl",
          "xxxl"
        ]
      },
      "defaultValue": "small",
      "description": "按钮中 Icon 的尺寸，用于替代 Icon 的默认大小"
    },
    {
      "name": "ghost",
      "title": {
        "type": "i18n",
        "zh-CN": "幽灵按钮",
        "en-US": "ghost"
      },
      "propType": {
        "type": "oneOf",
        "value": [
          true,
          false,
          "light",
          "dark"
        ]
      },
      "description": "是否为幽灵按钮",
      "defaultValue": false
    },
    {
      "name": "loading",
      "title": "loading",
      "propType": "bool",
      "description": "设置按钮的载入状态",
      "defaultValue": false
    },
    {
      "name": "text",
      "title": {
        "type": "i18n",
        "zh-CN": "文本按钮",
        "en-US": "text"
      },
      "propType": "bool",
      "description": "是否为文本按钮",
      "defaultValue": false
    },
    {
      "name": "warning",
      "title": "warning",
      "propType": "bool",
      "description": "是否为警告按钮",
      "defaultValue": false
    },
    {
      "name": "disabled",
      "title": {
        "type": "i18n",
        "zh-CN": "禁用",
        "en-US": "disabled"
      },
      "propType": "bool",
      "description": "是否禁用",
      "defaultValue": false
    },
    {
      "name": "onClick",
      "title": "onClick",
      "propType": "func",
      "description": "点击按钮的回调\n@param {Object} e Event Object"
    },
    {
      "name": "className",
      "propType": "string"
    },
    {
      "name": "onMouseUp",
      "propType": "func"
    },
    {
      "name": "style",
      "propType": "object",
      "description": "自定义内联样式"
    }
  ],
  "configure": {
    "props": {
      "isExtends": true,
      "override": [
        {
          "name": "icon",
          "title": {
            "label": {
              "type": "i18n",
              "zh-CN": "图标",
              "en-US": "Icon"
            },
            "tip": {
              "type": "i18n",
              "zh-CN": "属性: icon | 说明: 图标类型",
              "en-US": "prop: icon | description: icon type"
            }
          },
          "setter": "IconSetter"
        },
        {
          "name": "ghost",
          "title": {
            "type": "i18n",
            "zh-CN": "幽灵按钮",
            "en-US": "ghost"
          },
          "setter": {
            "componentName": "RadioGroupSetter",
            "props": {
              "options": [
                {
                  "title": "True",
                  "value": true
                },
                {
                  "title": "False",
                  "value": false
                },
                {
                  "title": "Light",
                  "value": "light"
                },
                {
                  "title": "Dark",
                  "value": "dark"
                }
              ]
            }
          },
          "description": "是否为幽灵按钮",
          "defaultValue": false
        },
        {
          "name": "children",
          "title": {
            "label": {
              "type": "i18n",
              "zh-CN": "按钮内容",
              "en-US": "Content"
            },
            "tip": {
              "type": "i18n",
              "zh-CN": "属性: children | 说明: 按钮文本",
              "en-US": "prop: children | description: button content"
            }
          },
          "setter": {
            "componentName": "MixedSetter",
            "props": {
              "setters": [
                "StringSetter",
                "ExpressionSetter"
              ]
            }
          }
        }
      ]
    }
  },
};

const snippets = [
  {
    title: '按钮',
    name: 'FusionButton',
    screenshot: '/resources/img.alicdn.com_tfs_TB1rT0gPQL0gK0jSZFAXXcA9pXa-200-200.svg',
    schema: {
      componentName: 'FusionButton',
      title: '按钮',
      props: {
        children: "FusionButton"
      },
    },
  },
];
export default {
  ...CodeMeta,
  snippets
};
