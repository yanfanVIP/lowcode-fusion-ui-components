
import { IPublicTypeComponentMetadata, IPublicTypeSnippet } from '@alilc/lowcode-types';

const Meta: IPublicTypeComponentMetadata = {
  "componentName": "FusionQuill",
  "title": "富文本框",
  category: "内容",
  group: '高级组件',
  "docUrl": "",
  "screenshot": "/resources/quill.png",
  "devMode": "proCode",
  "npm": {
    package: '@alifd/fusion-ui',
    version: '{{version}}',
    "exportName": "FusionQuill",
    "main": "",
    "destructuring": true,
    "subName": ""
  },
  "configure": {
    "props": [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'height',
            title: '控件高度',
            defaultValue: 200,
            setter: 'NumberSetter'
          },
          {
            name: 'theme',
            title: '主题',
            defaultValue: 'snow',
            setter: {
              componentName: 'RadioGroupSetter',
              props: {
                options: [
                  { value: 'bubble', title: 'bubble' },
                  { value: 'snow', title: 'snow' }
                ],
              },
            },
          },
          {
            name: 'placeholder',
            title: '提示信息',
            setter: 'StringSetter'
          }
        ],
      }
    ],
    "supports": {
      events: ['onChange', 'onChangeSelection', 'onFocus', 'onBlur', 'onKeyPress', 'onKeyDown', 'onKeyUp'],
      "style": true
    },
    "component": {}
  }
};
const snippets: IPublicTypeSnippet[] = [
  {
    "title": "富文本框",
    "screenshot": "/resources/quill.png",
    "schema": {
      "componentName": "FusionQuill",
      "props": {}
    }
  }
];

export default {
  ...Meta,
  snippets
};
