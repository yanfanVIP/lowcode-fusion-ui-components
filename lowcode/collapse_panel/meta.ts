import { IComponentDescription } from '../types';
import { hideProp, mockId } from '../utils';

const CodeMeta: IComponentDescription = {
  componentName: 'CollapsePanel',
  title: '折叠面板(仅限折叠表单)',
  category: '表单类',
  group: '高级组件',
  docUrl: '',
  screenshot: '/resources/panel.png',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: 'CollapsePanel',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'title',
            title: '标题',
            setter: 'StringSetter'
          },
          {
            name: 'defaultClose',
            title: '默认关闭?',
            setter: 'BoolSetter',
            defaultValue: false,
          },
          {
            name: 'disabled',
            title: '是否禁用',
            setter: 'BoolSetter',
            defaultValue: false,
          }
        ],
      }
    ],
    supports: {
      style: true,
    },
    component: {
      isContainer: true,
      nestingRule: {
        childWhitelist: ['ChildForm'],
        parentWhitelist: ['CollapseForm'],
      },
    },
  },
};

const snippets = [
  {
    title: '折叠面板(仅限折叠表单)',
    name: 'CollapsePanel',
    screenshot: '/resources/panel.png',
    schema: {
      componentName: 'CollapsePanel',
      title: '折叠面板(仅限折叠表单)',
      props: {
        title: 'Tab1',
        htmlId: mockId(),
      },
      children: {
        componentName: 'ChildForm',
        props: {
          mode: 'independent',
          columns: 2,
          operationConfig: {},
          labelCol: {
            fixedSpan: 4,
          },
          labelAlign: 'top',
        },
        children: [...new Array(4).keys()].map(() => ({
          componentName: 'FormInput',
          props: {
            formItemProps: {
              primaryKey: mockId(),
              label: '表单项',
              size: 'medium',
              device: 'desktop',
              fullWidth: true,
            },
            placeholder: '请输入',
          },
        })),
      }
    },
  },
];
export default {
  ...CodeMeta,
  snippets
};
