module.exports = [
  {
    title: '评分',
    screenshot:
      '/resources/ic_light_rating.png',
    schema: {
      componentName: 'Rating',
      props: {
        prefix: 'next-',
        count: 5,
        size: 'medium',
      },
    },
  },
];
