
import { IPublicTypeComponentMetadata, IPublicTypeSnippet } from '@alilc/lowcode-types';

const TableSelectDialogMeta: IPublicTypeComponentMetadata = {
  "componentName": "TableSelectDialog",
  "title": "数据库选择器Dialog",
  icon: '/resources/O1CN01n5JLZG1aBmYZlckSx_!!6000000003292-55-tps-56-56.svg',
  category: 'AntDesignPro',
  group: '高级组件',
  "docUrl": "",
  "screenshot": "",
  "devMode": "proCode",
  "npm": {
    package: '@alifd/fusion-ui',
    version: '{{version}}',
    "exportName": "TableSelectDialog",
    "main": "",
    "destructuring": true,
    "subName": ""
  },
  "configure": {
    "props": [
      {
        name: '数据库配置',
        type: 'group',
        display: 'accordion',
        title: '数据库配置',
        items: [
          {
            name: 'config',
            title: '配置项',
            setter: 'JsonSetter'
          },{
            name: 'url',
            title: '数据请求地址',
            setter: 'StringSetter'
          },
          {
            name: 'table',
            title: '表名称',
            setter: 'StringSetter'
          },
          {
            name: 'fieldProps',
            title: '输入框配置项',
            setter: 'JsonSetter'
          },
          {
            name: 'multi',
            title: '是否可以多选?',
            setter: 'BoolSetter',
            defaultValue: false,
          },
          {
            name: 'entity',
            title: '值字段',
            setter: 'StringSetter',
            defaultValue: 'id',
          },
          {
            name: 'filters',
            title: '过滤条件',
            setter: 'JsonSetter'
          },{
            name: "default_sort",
            title: '默认排序',
            setter: 'JsonSetter'
          }
        ],
      }
    ],
    "supports": {
      "style": true
    },
    "component": {
      isModal: true,
      rootSelector: 'div.next-dialog',
    }
  }
};
const snippets: IPublicTypeSnippet[] = [
  {
    "title": "数据库选择器Dialog",
    screenshot: '/resources/O1CN01n5JLZG1aBmYZlckSx_!!6000000003292-55-tps-56-56.svg',
    "schema": {
      "componentName": "TableSelectDialog",
      "props": {}
    }
  }
];

export default {
  ...TableSelectDialogMeta,
  snippets
};
