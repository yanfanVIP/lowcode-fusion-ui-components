const componentName = "FusionGrid"
const title = "Grid填充"
const screenshot = "/resources/grid.svg"
const group = "高级组件"
const category = "内容"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'dataSource',
        title: '数据',
        setter: 'JsonSetter'
      },{
        name: 'hasSquare',
        title: '强制正方形',
        setter: 'BoolSetter'
      },{
        name: 'hasBorder',
        title: '是否展示边框',
        setter: 'BoolSetter'
      },{
        name: 'hasClick',
        title: '是否可点击',
        setter: 'BoolSetter'
      }
    ],
    supports: {
      className: true,
      events: ['onClickGridCell'],
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title,
      children: {
        componentName: "FusionTypography",
        props: {
            type: 'H1',
            value: '标题'
        }
      },
      props: {
        dataSource: [
          {
              "id": 0,
              "value": "第0个"
          },
          {
              "id": 1,
              "value": "第1个"
          },
          {
              "id": 2,
              "value": "第2个"
          },
          {
              "id": 3,
              "value": "第3个"
          },
          {
              "id": 4,
              "value": "第4个"
          },
          {
              "id": 5,
              "value": "第5个"
          },
          {
              "id": 6,
              "value": "第6个"
          },
          {
              "id": 7,
              "value": "第7个"
          },
          {
              "id": 8,
              "value": "第8个"
          },
          {
              "id": 9,
              "value": "第9个"
          },
          {
              "id": 10,
              "value": "第10个"
          },
          {
              "id": 11,
              "value": "第11个"
          },
          {
              "id": 12,
              "value": "第12个"
          },
          {
              "id": 13,
              "value": "第13个"
          },
          {
              "id": 14,
              "value": "第14个"
          },
          {
              "id": 15,
              "value": "第15个"
          },
          {
              "id": 16,
              "value": "第16个"
          },
          {
              "id": 17,
              "value": "第17个"
          },
          {
              "id": 18,
              "value": "第18个"
          },
          {
              "id": 19,
              "value": "第19个"
          },
          {
              "id": 20,
              "value": "第20个"
          },
          {
              "id": 21,
              "value": "第21个"
          },
          {
              "id": 22,
              "value": "第22个"
          },
          {
              "id": 23,
              "value": "第23个"
          },
          {
              "id": 24,
              "value": "第24个"
          },
          {
              "id": 25,
              "value": "第25个"
          },
          {
              "id": 26,
              "value": "第26个"
          },
          {
              "id": 27,
              "value": "第27个"
          },
          {
              "id": 28,
              "value": "第28个"
          },
          {
              "id": 29,
              "value": "第29个"
          },
          {
              "id": 30,
              "value": "第30个"
          },
          {
              "id": 31,
              "value": "第31个"
          },
          {
              "id": 32,
              "value": "第32个"
          },
          {
              "id": 33,
              "value": "第33个"
          },
          {
              "id": 34,
              "value": "第34个"
          },
          {
              "id": 35,
              "value": "第35个"
          },
          {
              "id": 36,
              "value": "第36个"
          },
          {
              "id": 37,
              "value": "第37个"
          },
          {
              "id": 38,
              "value": "第38个"
          },
          {
              "id": 39,
              "value": "第39个"
          },
          {
              "id": 40,
              "value": "第40个"
          },
          {
              "id": 41,
              "value": "第41个"
          },
          {
              "id": 42,
              "value": "第42个"
          },
          {
              "id": 43,
              "value": "第43个"
          },
          {
              "id": 44,
              "value": "第44个"
          },
          {
              "id": 45,
              "value": "第45个"
          },
          {
              "id": 46,
              "value": "第46个"
          },
          {
              "id": 47,
              "value": "第47个"
          },
          {
              "id": 48,
              "value": "第48个"
          },
          {
              "id": 49,
              "value": "第49个"
          },
          {
              "id": 50,
              "value": "第50个"
          },
          {
              "id": 51,
              "value": "第51个"
          },
          {
              "id": 52,
              "value": "第52个"
          },
          {
              "id": 53,
              "value": "第53个"
          },
          {
              "id": 54,
              "value": "第54个"
          },
          {
              "id": 55,
              "value": "第55个"
          },
          {
              "id": 56,
              "value": "第56个"
          },
          {
              "id": 57,
              "value": "第57个"
          },
          {
              "id": 58,
              "value": "第58个"
          },
          {
              "id": 59,
              "value": "第59个"
          },
          {
              "id": 60,
              "value": "第60个"
          },
          {
              "id": 61,
              "value": "第61个"
          },
          {
              "id": 62,
              "value": "第62个"
          },
          {
              "id": 63,
              "value": "第63个"
          }
        ]
      },
    },
  },
];
export default {
  ...Meta,
  snippets
};
