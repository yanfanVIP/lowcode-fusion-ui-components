import { IComponentDescription } from '../types';
import { operationProps } from '../common';
import { mockId } from '../utils';


const CollapseFormMeta: IComponentDescription = {
  componentName: 'CollapseForm',
  title: '折叠表单Pro',
  category: '表单类',
  group: '高级组件',
  docUrl: '',
  screenshot: '/resources/O1CN01jnfLpK1rFJ4nPj9Pt_!!6000000005601-55-tps-56-56.svg',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.6-beta.22',
    exportName: 'CollapseForm',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      ...operationProps,
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'isPreview',
            title: {
              label: {
                type: 'i18n',
                zh_CN: '是否只读？',
                en_US: 'Preview Mode',
              },
              tip: {
                type: 'i18n',
                zh_CN: '属性: isPreview | 说明: 是否开启预览态',
                en_US: 'prop: isPreview | description: preview mode',
              },
            },
            setter: 'BoolSetter',
            description: '是否开启预览态',
          },
          {
            name: 'disabled',
            title: '是否禁用',
            setter: 'BoolSetter',
            defaultValue: false,
          },
        ],
      }
    ],
    supports: { className: true, style: true, events: ['pageInit', 'onSubmit', 'onChange'] },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true,
      nestingRule: {
        childWhitelist: ['CollapsePanel'],
      },
    },
  },
};
const snippets: Snippet[] = [
  {
    title: '折叠表单Pro',
    screenshot: '/resources/O1CN01jnfLpK1rFJ4nPj9Pt_!!6000000005601-55-tps-56-56.svg',
    schema: {
      componentName: 'CollapseForm',
      children: [{
        componentName: 'CollapsePanel',
        title: '折叠面板',
        props: {
          title: 'Tab1'
        },
        children: {
          componentName: 'ChildForm',
          props: {
            mode: 'independent',
            columns: 2,
            operationConfig: {},
            labelCol: {
              fixedSpan: 4,
            },
            labelAlign: 'top',
          },
          children: [...new Array(4).keys()].map(() => ({
            componentName: 'FormInput',
            props: {
              formItemProps: {
                primaryKey: mockId(),
                label: '表单项',
                size: 'medium',
                device: 'desktop',
                fullWidth: true,
              },
              placeholder: '请输入',
            },
          })),
        }
      },{
        componentName: 'CollapsePanel',
        title: '折叠面板',
        props: {
          title: 'Tab2'
        },
        children: {
          componentName: 'ChildForm',
          props: {
            mode: 'independent',
            columns: 2,
            operationConfig: {},
            labelCol: {
              fixedSpan: 4,
            },
            labelAlign: 'top',
          },
          children: [...new Array(4).keys()].map(() => ({
            componentName: 'FormInput',
            props: {
              formItemProps: {
                primaryKey: mockId(),
                label: '表单项',
                size: 'medium',
                device: 'desktop',
                fullWidth: true,
              },
              placeholder: '请输入',
            },
          })),
        }
      }],
    }
  },
];

export default {
  ...CollapseFormMeta,
  snippets,
};
