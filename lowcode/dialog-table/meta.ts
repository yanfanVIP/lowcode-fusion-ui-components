import { IComponentDescription } from '../types/index';
import { ProTableProps } from '../pro-table/pro-table-meta';

const TableProps = ProTableProps
  .filter(item=>!['actionColumnProps', 'actionBarButtons', 'actionColumnButtons'].includes(item.name))
  .map((item) => {
  if (item.name === 'advanced') {
    return {
      type: 'group',
      title: '高级',
      name: 'other',
      extraProps: {
        display: 'accordion',
        defaultCollapsed: true,
      },
      items: [
        {
          type: 'field',
          name: '!选择模式',
          title: '选择模式',
          display: 'inline',
          setter: {
            componentName: 'RadioGroupSetter',
            props: {
              options: [
                {
                  title: '无',
                  value: 'none',
                  tip: 'none',
                },
                {
                  title: '多选',
                  value: 'multiple',
                  tip: 'multiple',
                },
                {
                  title: '单选',
                  value: 'single',
                  tip: 'single',
                },
              ],
            },
          },
          defaultValue: 'none',
          getValue: (target) => {
            const rowSelection = target.parent.getPropValue('rowSelection');
            if (!rowSelection) {
              return 'none';
            }
            return rowSelection.mode === 'single' ? 'single' : 'multiple';
          },
          setValue: (field, value) => {
            const { node } = field;
            if (['single', 'multiple'].includes(value)) {
              node.setPropValue('rowSelection', {
                ...node.getPropValue('rowSelection'),
                mode: value,
              });
            } else {
              node.setPropValue('rowSelection', undefined);
            }
          },
        },
        {
          name: 'tableLayout',
          title: '表格布局方式',
          setter: {
            componentName: 'RadioGroupSetter',
            props: {
              options: [
                {
                  title: 'fixed',
                  value: 'fixed'
                },
                {
                  title: 'auto',
                  value: 'auto'
                },
              ],
            },
          },
        },
        {
          name: 'useVirtual',
          title: '开启虚拟滚动',
          setter: 'BoolSetter'
        },
        {
          name: 'fixedHeader',
          title: '表头固定',
          setter: 'BoolSetter'
        },
        {
          name: 'maxBodyHeight',
          title: '内容高度',
          setter: ['NumberSetter', 'StringSetter']
        },{
          name: 'stickyHeader',
          title: '表头sticky',
          setter: 'BoolSetter'
        },
        {
          type: 'field',
          name: 'indexColumn',
          title: '开启序号列',
          extraProps: {
            display: 'inline',
            defaultValue: false,
          },
          setter: {
            componentName: 'BoolSetter',
          },
        },
        {
          name: 'cellDefault',
          title: {
            label: '单元格缺省填充',
            tip: '当单元格值为空时，显示内容',
          },
          setter: 'StringSetter',
        },
      ],
    };
  }
  return { ...item };
});

export const Meta: IComponentDescription = {
  componentName: 'DialogTable',
  title: '表格选择器Dialog',
  docUrl: '',
  icon: '/resources/O1CN01n5JLZG1aBmYZlckSx_!!6000000003292-55-tps-56-56.svg',
  devMode: 'procode',
  group: '高级组件',
  category: '表格类',
  tags: ['业务组件'],
  npm: {
    package: '@alifd/fusion-ui',
    version: '1.0.24-21',
    exportName: 'DialogTable',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: TableProps,
    "component": {
      isModal: true,
      rootSelector: 'div.next-dialog',
    },
    supports: {
      style:true,
      events: ['onInit'],
    },
  },
  snippets: [
    {
      title: '表格选择器Dialog',
      screenshot: '/resources/O1CN01n5JLZG1aBmYZlckSx_!!6000000003292-55-tps-56-56.svg',
      schema: {
        componentName: 'DialogTable',
        props: {
          children_field:'value',
          dataSource: [
            {
              id: 'id-2f5DdE2b-0',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司1'
            },
            {
              id: 'id-2f5DdE2b-1',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司2'
            },
            {
              id: 'id-2f5DdE2b-2',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司3'
            },
            {
              id: 'id-2f5DdE2b-3',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司4'
            },
            {
              id: 'id-2f5DdE2b-4',
              date: '2013-06-12',
              percent: 1.862,
              documentAmount: 2022,
              currency: 'CNY',
              company: '支付宝科技有限公司5'
            },
          ],
          actionColumnButtons: {
            text: true,
            visibleButtonCount: 3,
          },
          paginationProps: {
            hidden: true,
            pageSize: 20,
            current: 1,
          },
          settingButtons: false,
          columns: [
            {
              title: '公司',
              dataIndex: 'company',
              width: 160,
              formatType: 'link',
              searchable: true,
            },
            {
              title: '单据金额',
              dataIndex: 'documentAmount',
              formatType: 'money',
            },
            {
              title: '币种',
              dataIndex: 'currency',
              formatType: 'currency',
              filters: [
                {
                  label: 'CNY',
                  value: 'CNY',
                },
                {
                  label: 'USD',
                  value: 'USD',
                },
                {
                  label: 'JPY',
                  value: 'JPY',
                },
                {
                  label: 'HKD',
                  value: 'HKD',
                },
              ],
              filterMode: 'multiple',
              explanation: '提示信息',
              width: 110,
            },
            {
              title: '完成进度',
              dataIndex: 'percent',
              formatType: 'progress',
            },
            {
              title: '到账日期',
              dataIndex: 'date',
              formatType: 'date',
            },
          ],
        },
      },
    },
  ],
};

export default Meta;
