const componentName = "FusionCascader"
const title = "级联组件"
const screenshot = "/resources/ic_light_cascader-select.png"
const group = "高级组件"
const category = "内容"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'dataSource',
        title: '数据',
        setter: 'JsonSetter'
      },{
        name: 'value',
        title: '默认选中',
        setter: ["StringSetter", {
          componentName: 'ArraySetter',
          "props": {
            "itemSetter": {
              "componentName": "StringSetter"
            }
          }
        }]
      },{
        name: 'expandTriggerType',
        title: '展开触发的方式',
        setter: {
          componentName: 'RadioGroupSetter',
          props: {
            options: [
              { title: 'click', value: 'click' },
              { title: 'hover', value: 'hover' },
            ],
          },
        }
      },
      {
        name: 'useVirtual',
        title: '是否开启虚拟滚动',
        setter: 'BoolSetter'
      },{
        name: 'loading',
        title: 'Loading',
        setter: 'BoolSetter'
      },{
        name: 'multiple',
        title: '是否多选',
        setter: 'BoolSetter'
      },{
        name: 'canOnlySelectLeaf',
        title: '单选时是否只能选中叶子节点',
        setter: 'BoolSetter'
      },{
        name: 'canOnlyCheckLeaf',
        title: '多选时是否只能选中叶子节点',
        setter: 'BoolSetter'
      },{
        name: 'checkStrictly',
        title: '父子节点是否选中不关联',
        setter: 'BoolSetter'
      },{
        name: 'immutable',
        title: '是否是不可变数据',
        setter: 'BoolSetter'
      },
      {
        name: 'listStyle',
        title: '列表样式',
        display: 'accordion',
        setter: 'StyleSetter',
        defaultValue: {
          height: '100%',
          lineHeight:"20px"
        }
      }
    ],
    supports: {
      events: ['onChange'],
      style: true,
    },
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title,
      props: {
        style:{
          width:'100%',
          height:'100%',
        },
        listStyle: {
          height:'100%',
          lineHeight:"20px",
        },
        value: "2982",
        dataSource: [
          {
            value: "2973",
            label: "陕西",
            children: [
              {
                value: "2974",
                label: "西安",
                children: [
                  { value: "2975", label: "西安市" },
                  { value: "2976", label: "高陵县" }
                ]
              },
              {
                value: "2980",
                label: "铜川",
                children: [
                  { value: "2981", label: "铜川市" },
                  { value: "2982", label: "宜君县" }
                ]
              }
            ]
          },
          {
            value: "3371",
            label: "新疆",
            children: [
              {
                value: "3430",
                label: "巴音郭楞蒙古自治州",
                children: [
                  { value: "3431", label: "库尔勒市" },
                  { value: "3432", label: "和静县" }
                ]
              }
            ]
          }
        ]
      },
    },
  },
];
export default {
  ...Meta,
  snippets
};
