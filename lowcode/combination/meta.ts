
import { IPublicTypeComponentMetadata, IPublicTypeSnippet } from '@alilc/lowcode-types';

const Meta: IPublicTypeComponentMetadata = {
  "componentName": "FusionCombination",
  "title": "图片组合",
  category: "内容",
  group: '高级组件',
  "docUrl": "",
  "screenshot": "/resources/combination.svg",
  "devMode": "proCode",
  "npm": {
    package: '@alifd/fusion-ui',
    version: '{{version}}',
    "exportName": "FusionCombination",
    "main": "",
    "destructuring": true,
    "subName": ""
  },
  "configure": {
    "props": [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'data',
            title:'数据',
            setter: 'JsonSetter'
          },{
            name: 'menus',
            title: '菜单',
            setter: {
              componentName: 'ArraySetter',
              props: {
                itemSetter: {
                  componentName: 'ObjectSetter',
                  props: {
                    config: {
                      items: [
                        {
                          name: 'key',
                          title: 'Key',
                          defaultValue: 'key',
                          important: true,
                          setter: 'StringSetter',
                        },{
                          name: 'label',
                          title: '名称',
                          defaultValue: 'menu',
                          important: true,
                          setter: 'StringSetter',
                        },{
                          name: 'menuClick',
                          title: '触发函数',
                          setter: 'FunctionSetter',
                        }
                      ],
                    },
                  },
                  initialValue: () => {
                    return {
                      key: String(Math.floor(Math.random() * 10000)),
                      label: 'menu'
                    }
                  }
                }
              }
            }
          }
        ]
      }
    ],
    "supports": {
      events: ['onContextMenu', 'onclick'],
      "style": true
    },
    "component": {}
  }
};
const snippets: IPublicTypeSnippet[] = [
  {
    "title": "图片组合",
    "screenshot": "/resources/combination.svg",
    "schema": {
      "componentName": "FusionCombination",
      "props": {
        style: {
          height: '600px'
        },
        data: {
          "id": "1427",
          "name": "正方形",
          "image": "/resources/正方形.png",
          "width": "1024",
          "height": "1024",
          "menu": true,
          "children": [
            {
                "id": "1428",
                "name": "长方形1",
                "image": "/resources/长方形.png",
                "x": "200",
                "y": "300",
            },
            {
                "id": "1429",
                "name": "长方形2",
                "image": "/resources/长方形.png",
                "x": "400",
                "y": "400",
            },
            {
                "id": "1430",
                "name": "长方形3",
                "image": "/resources/长方形.png",
                "x": "500",
                "y": "500",
                "menu": true,
            }
          ]
        }
      }
    }
  }
];

export default {
  ...Meta,
  snippets
};
