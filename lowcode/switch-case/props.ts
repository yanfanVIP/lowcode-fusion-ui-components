export default [
  {
    name: 'slots',
    title: '自定义组件',
    setter: {
      componentName: 'ArraySetter',
      props: {
        itemSetter: {
          componentName: 'ObjectSetter',
          props: {
            config: {
              items: [
                {
                  name: 'primaryKey',
                  title: '项目编号',
                  condition: () => false,
                  setter: 'StringSetter',
                },
                {
                  name: 'type',
                  title: '类型',
                  important: true,
                  defaultValue: 'type1',
                  setter: 'StringSetter',
                },{
                  name: 'default',
                  title: '默认项',
                  important: true,
                  defaultValue: false,
                  setter: 'BoolSetter',
                },{
                  name: 'field',
                  title: '子元素参数名',
                  defaultValue: 'value',
                  setter: 'StringSetter',
                }
              ],
            },
          },
          initialValue: () => {
            return {
              primaryKey: String(Math.floor(Math.random() * 10000)),
              type: 'type1',
              default: false
            };
          },
        },
      }
    },
    extraProps: {
      getValue(target) {
        const map = target.node.children.map((child) => {
          const primaryKey = child.getPropValue('primaryKey') ? String(child.getPropValue('primaryKey')) : child.id;
          return {
            primaryKey,
            type: child.getPropValue('type'),
            default: child.getPropValue('default')
          }
        })
        return map;
      },
      setValue(target, value) {
        const { node } = target;
        const map = {};
        if (!Array.isArray(value)) {
          value = [];
        }
        value.forEach((item) => {
          const tabitem = Object.assign({}, item);
          map[item.primaryKey] = tabitem;
        });

        node.children.mergeChildren(
          (child) => {
            const primaryKey = String(child.getPropValue('primaryKey'));
            if (Object.hasOwnProperty.call(map, primaryKey)) {
              child.setPropValue('type', map[primaryKey].type);
              child.setPropValue('default', map[primaryKey].default);
              delete map[primaryKey];
              return false;
            }
            return true;
          },
          () => {
            const items: any[] = [];
            for (const primaryKey in map) {
              if (Object.hasOwnProperty.call(map, primaryKey)) {
                items.push({ componentName: 'FusionSwitchCase.Item', props: map[primaryKey] });
              }
            }
            return items;
          },
          (child1, child2) => {
            const a = value.findIndex(
              (item) => String(item.primaryKey) === String(child1.getPropValue('primaryKey')),
            );
            const b = value.findIndex(
              (item) => String(item.primaryKey) === String(child2.getPropValue('primaryKey')),
            );
            return a - b;
          },
        );
      },
    }
  },
  {
    name: 'value',
    title: '默认值',
    setter: {
      componentName: 'ObjectSetter',
      props: {
        config: {
          items: [
            {
              name: 'type',
              display: 'inline',
              title: '选项',
              setter: 'StringSetter',
              important: true,
            },
            {
              name: 'data',
              display: 'inline',
              title: '参数',
              setter: 'JsonSetter',
              important: true,
            },
          ],
        },
      },
      initialValue: () => {
        return {
          type: '选择项1'
        };
      },
    },
  },{
    name: 'allSlot',
    title: '展示所有自定义组件',
    setter: 'BoolSetter'
  },
]
