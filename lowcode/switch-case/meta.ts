import props from './props';

const snippets = [
  {
    title: '选择项组件',
    name: 'FusionSwitchCase',
    screenshot: '/resources/switch-case.png',
    schema: {
      componentName: 'FusionSwitchCase',
      props: {
        value: {
          type: 'type1',
          data: {
            demo: "demo1"
          }
        }
      },
      children: [
        {
          componentName: 'FusionSwitchCase.Item',
          props: {
            primaryKey: String(Math.floor(Math.random() * 10000)),
            type: 'type1'
          }
        }, {
          componentName: 'FusionSwitchCase.Item',
          props: {
            primaryKey: String(Math.floor(Math.random() * 10000)),
            type: 'type2'
          }
        }, {
          componentName: 'FusionSwitchCase.Item',
          props: {
            primaryKey: String(Math.floor(Math.random() * 10000)),
            default: true
          }
        }
      ]
    },
  },
];

const ItemMeta = {
  componentName: 'FusionSwitchCase.Item',
  title: '选择项组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '{{version}}',
    exportName: 'FusionSwitchCase',
    main: 'lib/index.js',
    destructuring: true,
    subName: 'Item',
  },
  configure: {
    supports: {
      style: true,
    },
    component: {
      isContainer: true
    },
    props: [
      {
        name: 'primaryKey',
        title: '项目编号',
        condition: () => false,
        setter: 'StringSetter',
      },{
        name: 'type',
        title: '类型',
        setter: 'StringSetter',
        defaultValue: 'type1'
      },{
        name: 'default',
        title: '默认项',
        important: true,
        defaultValue: false,
        setter: 'BoolSetter',
      },{
        name: 'field',
        title: '子元素参数名',
        defaultValue: 'value',
        setter: 'StringSetter',
      }
    ]
  },
};

const Meta = [
  {
    componentName: 'FusionSwitchCase',
    title: '分支组件',
    group: '高级组件',
    category: '分支选择',
    docUrl: '',
    screenshot: '/resources/switch-case.png',
    devMode: 'proCode',
    npm: {
      package: '@alifd/fusion-ui',
      version: '2.1.0',
      exportName: 'FusionSwitchCase',
      main: 'lib/index.js',
      destructuring: true,
      subName: '',
    },
    configure: {
      component: {
        isContainer: true,
        nestingRule: {
          childWhitelist: ['FusionSwitchCase.Item'],
        },
      },
      props,
      supports: {
        style: true,
      },
    },
    snippets,
  },
  ItemMeta,
  {
    ...ItemMeta,
    componentName: 'FusionSwitchCase.Item',
  }
];

export default Meta;
