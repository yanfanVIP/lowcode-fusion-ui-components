
import { IPublicTypeComponentMetadata, IPublicTypeSnippet } from '@alilc/lowcode-types';

const TableSelectMeta: IPublicTypeComponentMetadata = {
  "componentName": "TableSelect",
  "title": "数据库选择器",
  category: "AntDesignPro",
  group: '高级组件',
  "docUrl": "",
  "screenshot": "/resources/table-select.png",
  "devMode": "proCode",
  "npm": {
    package: '@alifd/fusion-ui',
    version: '{{version}}',
    "exportName": "TableSelect",
    "main": "",
    "destructuring": true,
    "subName": ""
  },
  "configure": {
    "props": [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'config',
            title: '配置项',
            setter: 'JsonSetter'
          },{
            name: 'url',
            title: '数据请求地址',
            setter: 'StringSetter'
          },
          {
            name: 'table',
            title: '表名称',
            setter: 'StringSetter'
          },
          {
            name: 'fieldProps',
            title: '输入框配置项',
            setter: 'JsonSetter'
          },
          {
            name: 'multi',
            title: '是否可以多选?',
            setter: 'BoolSetter',
            defaultValue: false,
          },
          {
            name: 'entity',
            title: '值字段',
            setter: 'StringSetter',
            defaultValue: 'id',
          },
          {
            name: 'filters',
            title: '过滤条件',
            setter: 'JsonSetter'
          },{
            name: "default_sort",
            title: '默认排序',
            setter: 'JsonSetter'
          }
        ],
      }
    ],
    "supports": {
      events: ['onChange'],
      "style": true
    },
    "component": {}
  }
};
const snippets: IPublicTypeSnippet[] = [
  {
    "title": "数据库选择器",
    "screenshot": "/resources/table-select.png",
    "schema": {
      "componentName": "TableSelect",
      "props": {}
    }
  }
];

export default {
  ...TableSelectMeta,
  snippets
};
