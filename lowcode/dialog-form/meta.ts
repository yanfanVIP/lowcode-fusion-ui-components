import { IComponentDescription, ISnippet } from '../types';
import { default as props, FormItemProps } from '../pro-form/common/props';
import { default as ComponentsMeta } from '../pro-form/components';

const snippets: ISnippet[] = [
  {
    title: 'Dialog表单',
    screenshot: '/resources/O1CN016gn5DQ1FeXUNKdK22_!!6000000000512-55-tps-50-36.svg',
    schema: {
      componentName: 'FusionDialogForm',
      title: 'Dialog表单',
      props: {
        placeholder: '请在右侧面板添加表单项+',
        placeholderStyle: {
          height: '38px',
          color: '#0088FF',
          background: '#d8d8d836',
          border: 0,
          gridArea: 'span 4 / span 4',
        },
        columns: 4,
        labelCol: {
          fixedSpan: 4,
        },
        labelAlign: 'top',
        emptyContent: '添加表单项',
      },
      children: [...new Array(8).keys()].map((item) => ({
        componentName: 'FormInput',
        props: {
          formItemProps: {
            primaryKey: String(Math.floor(Math.random() * 10000) + item),
            label: '表单项',
            size: 'medium',
            device: 'desktop',
            fullWidth: true,
          },
          placeholder: '请输入',
        },
      })),
    },
  },
];

const FusionDialogFormMeta: IComponentDescription[] = [
  {
    componentName: 'FusionDialogForm',
    category: '表单类',
    title: 'Dialog表单',
    group: '高级组件',
    docUrl: '',
    screenshot: '',
    devMode: 'proCode',
    npm: {
      package: '@alifd/fusion-ui',
      version: '0.1.4',
      exportName: 'FusionDialogForm',
      main: 'lib/index.js',
      destructuring: true,
      subName: '',
    },
    configure: {
      component: {
        isModal: true,
        rootSelector: 'div.next-dialog'
      },
      supports: {
        style: true,
        events: ['pageInit', 'onChange'],

      },
      props,
    },
    snippets,
  },
  {
    componentName: 'FusionDialogFormItem',
    title: '表单项',
    docUrl: '',
    screenshot: '/resources/O1CN016gn5DQ1FeXUNKdK22_!!6000000000512-55-tps-50-36.svg',
    icon: '/resources/O1CN016gn5DQ1FeXUNKdK22_!!6000000000512-55-tps-50-36.svg',
    devMode: 'proCode',
    npm: {
      package: '@alifd/fusion-ui',
      version: '0.1.4',
      exportName: 'FusionDialogForm',
      main: 'lib/index.js',
      destructuring: true,
      subName: 'Item',
    },
    configure: {
      component: {
        disableBehaviors: ['copy'],
        nestingRule: {
          parentWhitelist: ['FusionDialogForm'],
        },
      },
      props: FormItemProps,
      supports: {
        style: true,
        events: ['onPressEnter', 'onClear', 'onChange', 'onKeyDown', 'onFocus', 'onBlur'],
      },
      advanced: {
        initialChildren: [
          {
            componentName: 'FormInput',
            props: {
              hasBorder: true,
              size: 'medium',
              autoComplete: 'off',
            },
          },
        ],
        callbacks: {
          onNodeRemove: (removedNode, currentNode) => {
            if (!removedNode || !currentNode) {
              return;
            }
            const { children } = currentNode;
            if (children && children.isEmptyNode) {
              currentNode.remove();
            }
          },
        },
      },
    },
  },
];

export default [...FusionDialogFormMeta, ...ComponentsMeta];
