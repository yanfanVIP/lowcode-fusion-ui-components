const componentName = "FusionTypography"
const title = "Typography"
const screenshot = "/resources/typography.svg"
const group = "高级组件"
const category = "内容"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: 'type',
        title: '组件类型',
        defaultValue: 'Typography',
        initialValue: 'Typography',
        setter: {
          componentName: 'SelectSetter',
          props: {
            options: [
              { value: 'Typography', title: 'Typography' },
              { value: 'Text', title: 'Text' },
              { value: 'Paragraph', title: 'Paragraph' },
              { value: 'H1', title: 'H1' },
              { value: 'H2', title: 'H2' },
              { value: 'H3', title: 'H3' },
              { value: 'H4', title: 'H4' },
              { value: 'H5', title: 'H5' },
              { value: 'H6', title: 'H6' },
            ]
          }
        }
      },{
        name: 'value',
        title: '内容',
        condition: (target)=> target.getProps().getPropValue("type") != 'Typography',
        defaultValue: 'hello Typography',
        setter: 'StringSetter'
      },{
        name: 'children',
        title: '内容',
        condition: (target)=> target.getProps().getPropValue("type") == 'Typography',
        setter: 'SlotSetter',
        defaultValue: { 
          type: 'JSSlot',
          visible: false,
          value: [
            {
              componentName: componentName,
              props: {
                type: 'H1',
                value: '标题'
              }
            },{
              componentName: componentName,
              props: {
                type: 'Text',
                value: 'AntV 是蚂蚁集团全新一代数据可视化解决方案，致力于提供一套简单方便、专业可靠、不限可能的数据可视化最佳实践。得益于丰富的业务场景和用户需求挑战，AntV 经历多年积累与不断打磨，已支撑整个阿里集团内外 20000+ 业务系统，通过了日均千万级 UV 产品的严苛考验。 我们正在基础图表，图分析，图编辑，地理空间可视化，智能可视化等各个可视化的领域耕耘，欢迎同路人一起前行。'
              }
            },{
              componentName: componentName,
              props: {
                type: 'Paragraph',
                value: 'AntV 是蚂蚁集团全新一代数据可视化解决方案，致力于提供一套简单方便、专业可靠、不限可能的数据可视化最佳实践。得益于丰富的业务场景和用户需求挑战，AntV 经历多年积累与不断打磨，已支撑整个阿里集团内外 20000+ 业务系统，通过了日均千万级 UV 产品的严苛考验。 我们正在基础图表，图分析，图编辑，地理空间可视化，智能可视化等各个可视化的领域耕耘，欢迎同路人一起前行。'
              }
            }
          ]
        },
      },{
        name: 'component',
        title: '标签类型',
        setter: 'StringSetter'
      },{
        name: 'delete',
        title: '删除线',
        setter: 'BoolSetter',
      },{
        name: 'mark',
        title: '标记样式',
        setter: 'BoolSetter',
      },{
        name: 'underline',
        title: '下划线',
        setter: 'BoolSetter',
      },{
        name: 'strong',
        title: '加粗',
        setter: 'BoolSetter',
      },{
        name: 'code',
        title: '代码样式',
        setter: 'BoolSetter',
      }
    ],
    supports: {
      className: true,
      events: ['onClick'],
      style: true,
    },
    component: {
      isContainer: true,
      isMinimalRenderUnit: true
    },
  }
}

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title,
      props: {
        type: 'Typography'
      },
      children: [
        {
          componentName: componentName,
          props: {
            type: 'H1',
            value: '标题'
          }
        },{
          componentName: componentName,
          props: {
            type: 'Text',
            value: 'AntV 是蚂蚁集团全新一代数据可视化解决方案，致力于提供一套简单方便、专业可靠、不限可能的数据可视化最佳实践。得益于丰富的业务场景和用户需求挑战，AntV 经历多年积累与不断打磨，已支撑整个阿里集团内外 20000+ 业务系统，通过了日均千万级 UV 产品的严苛考验。 我们正在基础图表，图分析，图编辑，地理空间可视化，智能可视化等各个可视化的领域耕耘，欢迎同路人一起前行。'
          }
        },{
          componentName: componentName,
          props: {
            type: 'Paragraph',
            value: 'AntV 是蚂蚁集团全新一代数据可视化解决方案，致力于提供一套简单方便、专业可靠、不限可能的数据可视化最佳实践。得益于丰富的业务场景和用户需求挑战，AntV 经历多年积累与不断打磨，已支撑整个阿里集团内外 20000+ 业务系统，通过了日均千万级 UV 产品的严苛考验。 我们正在基础图表，图分析，图编辑，地理空间可视化，智能可视化等各个可视化的领域耕耘，欢迎同路人一起前行。'
          }
        }
      ]
    }
  }
];
export default {
  ...Meta,
  snippets
};
