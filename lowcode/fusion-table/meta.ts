
import { IPublicTypeComponentMetadata, IPublicTypeSnippet } from '@alilc/lowcode-types';

const Meta: IPublicTypeComponentMetadata = {
  "componentName": "FusionTable",
  "title": "数据库表",
  group: '高级组件',
  category: '表格类',
  tags: ['业务组件'],
  "docUrl": "",
  "screenshot": "/resources/O1CN01dtjMvv1heoyqst9u5_!!6000000004303-55-tps-56-56.svg",
  "devMode": "proCode",
  "npm": {
    package: '@alifd/fusion-ui',
    version: '{{version}}',
    "exportName": "FusionTable",
    "main": "",
    "destructuring": true,
    "subName": ""
  },
  "configure": {
    "props": [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'config',
            title: '配置项',
            setter: 'JsonSetter'
          },
          {
            name: 'table',
            title: '表名称',
            defaultValue: 'sys_user',
            setter: 'StringSetter'
          },{
            name: 'add_label',
            title: '操作按钮名称',
            defaultValue: '新建',
            setter: 'StringSetter'
          },{
            name: 'add_label_icon',
            title: '操作按钮图标',
            defaultValue: 'add',
            setter: 'IconSetter'
          },{
            name: 'query_id',
            title: '默认展示数据',
            defaultValue: 'id',
            setter: 'StringSetter'
          }, {
            name: 'auths',
            title: '操作点',
            setter: {
              componentName: 'ArraySetter',
              props: {
                itemSetter: {
                  componentName: 'ObjectSetter',
                  props: {
                    config: {
                      items: [
                        {
                          name: 'key',
                          title: 'Key',
                          defaultValue: 'key',
                          important: true,
                          setter: 'StringSetter',
                        },{
                          name: 'value',
                          title: '值',
                          defaultValue: 'value',
                          important: true,
                          setter: 'StringSetter',
                        }
                      ],
                    },
                  },
                  initialValue: () => {
                    return {
                      key: String(Math.floor(Math.random() * 10000)),
                      value: 'value'
                    };
                  },
                },
              },
            }
          },{
            name: 'options',
            title: '操作',
            setter: {
              componentName: 'ArraySetter',
              props: {
                itemSetter: {
                  componentName: 'ObjectSetter',
                  props: {
                    config: {
                      items: [
                        {
                          name: 'key',
                          title: 'Key',
                          defaultValue: 'key',
                          important: true,
                          setter: 'StringSetter',
                        },{
                          name: 'value',
                          title: '操作名称',
                          defaultValue: 'value',
                          important: true,
                          setter: 'StringSetter',
                        },{
                          name: 'show',
                          title: '是否展示',
                          defaultValue: true,
                          setter: ["BoolSetter",'FunctionSetter'],
                        }
                      ],
                    },
                  },
                  initialValue: () => {
                    return {
                      key: String(Math.floor(Math.random() * 10000)),
                      value: 'value'
                    };
                  },
                },
              },
            }
          },{
            name: 'option',
            title: '表类型',
            defaultValue: 'DETAIL',
            setter: {
              "componentName": "SelectSetter",
              "props": {
                "options": [
                  { value: 'ADD', title: '新增' },
                  { value: 'EDIT', title: '编辑' },
                  { value: 'DETAIL', title: '详情' }
                ]
              }
            }
          }
        ],
      }
    ],
    "supports": {
      events: ['onOption','onAdd', 'onDetail', 'onEdit', 'onDelete', 'onFinish'],
      "style": true
    },
    "component": {}
  }
};
const snippets: IPublicTypeSnippet[] = [
  {
    "title": "数据库表",
    "screenshot": "/resources/O1CN01dtjMvv1heoyqst9u5_!!6000000004303-55-tps-56-56.svg",
    "schema": {
      "componentName": "FusionTable",
      "props": {}
    }
  }
];

export default {
  ...Meta,
  snippets
};
