const componentName = 'FusionG6.Topology'
const title = '拓扑图'

const Meta = {
  componentName: componentName,
  title: title,
  category: 'G6',
  group: '图表组件',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '0.1.3-beta.3',
    exportName: 'FusionG6',
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    supports: {
      events: ['onInit', 'click', 'onSelect', 'handleMenuClick', 'dblclick', 'move', 'contextmenu'],
      style: true,
    },
    props: [
      {
        name: 'data',
        title:'数据',
        setter: 'JsonSetter'
      },{
        name: 'selected_mode',
        title: '选择模式',
        initialValue: 'none',
        defaultValue: 'none',
        setter: {
          componentName: 'RadioGroupSetter',
          props: { 
            options: [
              { title: '不可选择', value: 'none' },
              { title: '单选', value: 'single' },
              { title: '多选', value: 'multi' },
            ],
          },
        },
      },{
        name: 'selected_type',
        title: '可选择项',
        initialValue: 'all',
        defaultValue: 'all',
        setter: {
          componentName: 'RadioGroupSetter',
          props: { 
            options: [
              { title: '节点', value: 'node' },
              { title: '连线', value: 'edge' },
              { title: '全部', value: 'all' },
            ],
          },
        },
      },{
        name: 'selected',
        title:'已选择项',
        setter: {
          componentName: 'ArraySetter',
          props: {
            itemSetter: {
              componentName: 'StringSetter',
            }
          }
        }
      },
      {
        name: '节点配置',
        type: 'group',
        display: 'accordion',
        title: '节点配置',
        items: [
          {
            name: 'node_type',
            title: '节点类型',
            initialValue: 'circle',
            setter: {
              "componentName": "SelectSetter",
              "props": {
                "options": [
                  { value: 'circle', title: '圆形' },
                  { value: 'rect', title: '矩形' },
                  { value: 'ellipse', title: '椭圆' },
                  { value: 'diamond', title: '菱形' },
                  { value: 'triangle', title: '三角形' },
                  { value: 'star', title: '星形' },
                  { value: 'image', title: '图片' },
                  { value: 'modelRect', title: '卡片' },
                  { value: 'donut', title: '圆形' },
                ],
              }
            }
          },{
            name: 'node_icon',
            title: '图标',
            condition: (target)=> target.getProps().getPropValue("node_type") == 'circle' || false,
            setter: 'IconSetter'
          },{
            name: 'node_img',
            title: '节点图片',
            condition: (target)=> target.getProps().getPropValue("node_type") == 'image' || false,
            setter: 'StringSetter',
            initialValue: '/resources/server.svg',
          },{
            name: 'node_size',
            title: '图片大小',
            condition: (target)=> target.getProps().getPropValue("node_type") == 'image' || false,
            setter: 'NumberSetter',
            defaultValue: 48,
          },{
            name: 'node_labelSize',
            title: '文字大小',
            setter: 'NumberSetter',
            defaultValue: 10,
          },{
            name: 'node_offset',
            title: '文字偏移量',
            setter: 'NumberSetter',
            defaultValue: -75,
          },
        ]
      },
      {
        name: '连线配置',
        type: 'group',
        display: 'accordion',
        title: '连线配置',
        items: [
          {
            name: 'lineWidth',
            title: '连线宽度',
            setter: 'NumberSetter',
            defaultValue: 2,
          },{
            name: 'hasStartArrow',
            title: '起始箭头',
            setter: 'BoolSetter',
            defaultValue: true,
          },{
            name: 'hasArrow',
            title: '结束箭头',
            setter: 'BoolSetter',
            defaultValue: true,
          },{
            name: 'edge_labelSize',
            title: '文字大小',
            setter: 'NumberSetter',
            defaultValue: 10,
          },{
            name: 'edge_offset',
            title: '文字偏移量',
            setter: 'NumberSetter',
            defaultValue: 0,
          },
        ]
      },
      {
        name: '全局配置',
        type: 'group',
        display: 'accordion',
        title: '全局配置',
        items: [
          {
            name: 'layout',
            title: '布局方式',
            initialValue: null,
            setter: {
              "componentName": "SelectSetter",
              "props": {
                "options": [
                  { value: null, title: '默认' },
                  { value: 'random', title: '随机' },
                  { value: 'force2', title: 'Force2力导向' },
                  { value: 'force', title: 'Force 力导向' },
                  { value: 'circular', title: '环形' },
                  { value: 'radial', title: '辐射形' },
                  { value: 'concentric', title: '同心圆' },
                  { value: 'dagre', title: '层次' },
                  { value: 'fruchterman', title: 'Fruchterman' },
                  { value: 'mds', title: '高维数据降维' },
                  { value: 'grid', title: '格子' },
                  { value: 'gForce', title: 'GForce' },
                  { value: 'forceAtlas2', title: 'Force Atlas 2' },
                  { value: 'comboForce', title: 'Combo 力导向' },
                  { value: 'comboCombined', title: 'Combo 复合布局' },
                ],
              }
            }
          },{
            name: 'modes',
            title:'交互模式',
            initialValue: ['drag-canvas', 'zoom-canvas', 'drag-node', 'activate-relations'],
            setter: {
              componentName: 'ArraySetter',
              props: {
                itemSetter: {
                  componentName: 'StringSetter',
                }
              }
            }
          },,
          {
            name: 'menu',
            title: '菜单',
            setter: {
              componentName: 'ArraySetter',
              props: {
                itemSetter: {
                  componentName: 'ObjectSetter',
                  props: {
                    config: {
                      items: [
                        {
                          name: 'key',
                          title: 'Key',
                          defaultValue: 'key',
                          important: true,
                          setter: 'StringSetter',
                        },{
                          name: 'type',
                          title: '类型',
                          defaultValue: 'node',
                          important: true,
                          setter: {
                            componentName: 'RadioGroupSetter',
                            props: { 
                              options: [
                                { title: '节点', value: 'node' },
                                { title: '连线', value: 'edge' }
                              ],
                            },
                          },
                        },{
                          name: 'title',
                          title: '标题',
                          defaultValue: 'Title',
                          important: true,
                          setter: 'StringSetter',
                        }
                      ],
                    },
                  },
                  initialValue: () => {
                    return {
                      key: String(Math.floor(Math.random() * 10000)),
                      title: 'menu'
                    };
                  },
                },
              },
            }
          },
          {
            name: 'hasGrid',
            title: '是否显示网格',
            setter: 'BoolSetter',
            defaultValue: true,
          },{
            name: 'hasSnapLine',
            title: '是否显示对齐线',
            setter: 'BoolSetter',
            defaultValue: true,
          },
          {
            name: 'hasToolbar',
            title: '是否显示操作栏',
            setter: 'BoolSetter',
            defaultValue: true,
          },{
            name: 'hasMiniMap',
            title: '是否显示迷你地图',
            setter: 'BoolSetter',
            defaultValue: true,
          },{
            name: 'readOnly',
            title: '只读?',
            setter: 'BoolSetter',
            defaultValue: false,
          }
        ],
      }
    ]
  },
};


const snippets = [
  {
    title: title,
    screenshot: `/resources/${componentName}.svg`,
    schema: {
      componentName: componentName,
      props: {
        node_type: 'image',
        node_img: '/resources/server.svg',
        node_size: 48,
        node_offset: -75,
        selected_mode: 'single',
        selected_type: 'all',
        modes: ['drag-canvas', 'zoom-canvas', 'drag-node', 'activate-relations'],
        menu: [{ key: 'detail', title: '详情'}, { key: 'edit', title: '编辑'}],
        data: {
          nodes: [
            {
              id: '1',
              label: '公司1',
              x: 100, 
              y: 200
            },
            {
              id: '2',
              label: '公司2',
              x: 150, 
              y: 500
            },
            {
              id: '3',
              label: '公司3',
              x: 350, 
              y: 300
            },
            {
              id: '4',
              label: '公司4',
              x: 300, 
              y: 200
            }
          ],
          edges: [
            {
              source: '1',
              target: '2',
              data: {
                type: 'name1',
                amount: '100,000,000,00 元',
                date: '2019-08-03'
              }
            },
            {
              source: '1',
              target: '3',
              data: {
                type: 'name2',
                amount: '100,000,000,00 元',
                date: '2019-08-03'
              }
            },
            {
              source: '3',
              target: '4',
              data: {
                type: 'name3',
                amount: '100,000,000,00 元',
                date: '2019-08-03'
              }
            }
          ]
        },
        style: {
          height: '800px',
        }
      },
    },
  },
];

export default [
  { ...Meta, snippets }
];