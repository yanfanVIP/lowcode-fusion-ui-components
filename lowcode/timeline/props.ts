export default [
  {
    name: 'mode',
    title: '展示的模式',
    setter:{
      componentName: 'RadioGroupSetter',
      defaultValue: 'left',
      props: {
        options: [
          {
            title: '居左',
            value: 'left',
          },
          {
            title: '交错显示',
            value: 'alternate',
          }
        ],
      },
    },
  },{
    name: 'leftTime',
    title: '时间居左侧',
    setter:'BoolSetter',
    defaultValue: true,
  },{
    name: 'animation',
    title: '开启动画',
    setter:'BoolSetter',
    defaultValue: true,
  },{
    name: 'slot',
    title: '自定义组件',
    setter: 'BoolSetter'
  },{
    name: 'allSlot',
    title: '展示所有自定义组件',
    setter: 'BoolSetter'
  },
  {
    name: 'items',
    title: '数据',
    setter: {
      componentName: 'ArraySetter',
      props: {
        itemSetter: {
          componentName: 'ObjectSetter',
          props: {
            config: {
              items: [
                {
                  name: 'time',
                  title: '时间',
                  defaultValue: new Date(),
                  setter: 'DateSetter',
                },{
                  name: 'title',
                  title: '标题',
                  defaultValue: 'Title',
                  important: true,
                  setter: 'StringSetter',
                },{
                  name: 'type',
                  title: '类型',
                  defaultValue: 'type1',
                  important: true,
                  setter: 'StringSetter',
                },{
                  name: 'content',
                  title: '内容',
                  defaultValue: '这里是内容',
                  setter: 'StringSetter',
                },
                {
                  name: 'data',
                  title: '数据',
                  defaultValue: {},
                  setter: 'JsonSetter',
                },{
                  name: 'state',
                  title: '状态',
                  defaultValue: 'success',
                  setter: {
                    componentName: 'SelectSetter',
                    props: {
                      options: [
                        {
                          title: 'done',
                          value: 'done',
                        },
                        {
                          title: 'process',
                          value: 'process',
                        },
                        {
                          title: 'error',
                          value: 'error',
                        },
                        {
                          title: 'success',
                          value: 'success',
                        },
                      ],
                    },
                  },
                },{
                  name: 'icon',
                  title: '图标',
                  setter: 'IconSetter',
                },
              ],
            },
          },
          initialValue: () => {
            return {
              title: 'Title',
              type: 'type1',
              date: new Date(),
              data: {},
              content: '这里是内容',
              state: 'success',
              icon: 'smile',
            };
          },
        },
      },
    }
  },{
    name: 'slots',
    title: '自定义组件',
    setter: {
      componentName: 'ArraySetter',
      props: {
        itemSetter: {
          componentName: 'ObjectSetter',
          props: {
            config: {
              items: [
                {
                  name: 'primaryKey',
                  title: '项目编号',
                  condition: () => false,
                  setter: 'StringSetter',
                },
                {
                  name: 'type',
                  title: '类型',
                  important: true,
                  defaultValue: 'type1',
                  setter: 'StringSetter',
                },{
                  name: 'default',
                  title: '默认项',
                  important: true,
                  defaultValue: false,
                  setter: 'BoolSetter',
                },{
                  name: 'field',
                  title: '子元素参数名',
                  defaultValue: 'value',
                  setter: 'StringSetter',
                }
              ],
            },
          },
          initialValue: () => {
            return {
              primaryKey: String(Math.floor(Math.random() * 10000)),
              type: 'type1',
              default: false
            };
          },
        },
      }
    },
    extraProps: {
      getValue(target) {
        const map = target.node.children.map((child) => {
          const primaryKey = child.getPropValue('primaryKey') ? String(child.getPropValue('primaryKey')) : child.id;
          return {
            primaryKey,
            type: child.getPropValue('type'),
            default: child.getPropValue('default')
          }
        })
        return map;
      },
      setValue(target, value) {
        const { node } = target;
        const map = {};
        if (!Array.isArray(value)) {
          value = [];
        }
        value.forEach((item) => {
          const tabitem = Object.assign({}, item);
          map[item.primaryKey] = tabitem;
        });

        node.children.mergeChildren(
          (child) => {
            const primaryKey = String(child.getPropValue('primaryKey'));
            if (Object.hasOwnProperty.call(map, primaryKey)) {
              child.setPropValue('type', map[primaryKey].type);
              child.setPropValue('default', map[primaryKey].default);
              delete map[primaryKey];
              return false;
            }
            return true;
          },
          () => {
            const items: any[] = [];
            for (const primaryKey in map) {
              if (Object.hasOwnProperty.call(map, primaryKey)) {
                items.push({ componentName: 'FusionTimeLine.Item', props: map[primaryKey] });
              }
            }
            return items;
          },
          (child1, child2) => {
            const a = value.findIndex(
              (item) => String(item.primaryKey) === String(child1.getPropValue('primaryKey')),
            );
            const b = value.findIndex(
              (item) => String(item.primaryKey) === String(child2.getPropValue('primaryKey')),
            );
            return a - b;
          },
        );
      },
    }
  },
]
