import props from './props';

const snippets = [
  {
    title: '时间线组件',
    group: '高级组件',
    name: 'FusionTimeLine',
    screenshot: '/resources/timeline.png',
    schema: {
      componentName: 'FusionTimeLine',
      props: { 
        leftTime: true,
        items: [
          {
            time : new Date('2023-12-30'),
            type: 'type1',
            title: 'Title1',
            state: 'success',
            icon: 'smile',
            content: '这里是内容',
            data: {
              demo: "demo1"
            }
          },{
            time : new Date('2023-12-31'),
            type: 'type2',
            title: 'Title2',
            state: 'success',
            icon: 'smile',
            content: '这里是内容',
            data: {
              demo: "demo2"
            }
          },{
            time : new Date('2024-01-01'),
            type: 'type3',
            title: 'Title3',
            state: 'success',
            icon: 'smile',
            content: '这里是内容',
            data: {
              demo: "demo3"
            }
          }
        ]
      },
      children: [
        {
          componentName: 'FusionTimeLine.Item',
          props: {
            primaryKey: String(Math.floor(Math.random() * 10000)),
            type: 'type1'
          }
        }, {
          componentName: 'FusionTimeLine.Item',
          props: {
            primaryKey: String(Math.floor(Math.random() * 10000)),
            type: 'type2'
          }
        }, {
          componentName: 'FusionTimeLine.Item',
          props: {
            primaryKey: String(Math.floor(Math.random() * 10000)),
            default: true
          }
        }
      ]
    },
  },
];

const ItemMeta = {
  componentName: 'FusionTimeLine.Item',
  title: '时间线展示项',
  docUrl: '',
  screenshot: '',
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '{{version}}',
    exportName: 'FusionTimeLine',
    main: 'lib/index.js',
    destructuring: true,
    subName: 'Item',
  },
  configure: {
    supports: {
      style: true,
    },
    component: {
      isContainer: true
    },
    props: [
      {
        name: 'primaryKey',
        title: '项目编号',
        condition: () => false,
        setter: 'StringSetter',
      },{
        name: 'type',
        title: '类型',
        setter: 'StringSetter',
        defaultValue: 'type1'
      },{
        name: 'default',
        title: '默认项',
        important: true,
        defaultValue: false,
        setter: 'BoolSetter',
      },{
        name: 'field',
        title: '子元素参数名',
        defaultValue: 'value',
        setter: 'StringSetter',
      }
    ]
  },
};

const Meta = [
  {
    componentName: 'FusionTimeLine',
    title: '时间线组件',
    group: '高级组件',
    category: '内容',
    docUrl: '',
    screenshot: '/resources/timeline.png',
    devMode: 'proCode',
    npm: {
      package: '@alifd/fusion-ui',
      version: '2.1.0',
      exportName: 'FusionTimeLine',
      main: 'lib/index.js',
      destructuring: true,
      subName: '',
    },
    configure: {
      props,
      supports: {
        style: true,
      }
    },
    snippets,
  },
  ItemMeta,
  {
    ...ItemMeta,
    componentName: 'FusionTimeLine.Item',
  }
];

export default Meta;
