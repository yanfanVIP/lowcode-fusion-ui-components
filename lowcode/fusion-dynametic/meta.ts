const componentName = "FusionDynametic"
const title = "动态图表"
const screenshot = "/resources/demo.svg"
const group = "图表组件"
const category = "G6"

const Meta = {
  componentName: componentName,
  title: title,
  group: group,
  category: category,
  docUrl: '',
  screenshot: screenshot,
  devMode: 'proCode',
  npm: {
    package: '@alifd/fusion-ui',
    version: '2.1.0',
    exportName: componentName,
    main: 'lib/index.js',
    destructuring: true,
    subName: '',
  },
  configure: {
    props: [
      {
        name: '设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '设置',
        },
        items: [
          {
            name: 'height',
            title: '控件高度',
            setter: 'StringSetter'
          },
          {
            name: 'language',
            title: '语言',
            setter: 'StringSetter'
          },
          {
            name: 'theme',
            title: '主题',
            setter: 'StringSetter',
            defaultValue: 'vs-dark',
          },
          {
            name: 'enableOutline',
            title: '是否开启大纲',
            setter: 'BoolSetter',
            defaultValue: false,
          }
        ],
      },{
        name: '代码内容设置',
        type: 'group',
        display: 'accordion',
        title: {
          label: '内容',
        },
        items: [
          {
            name: 'value',
            title: '当前值',
            setter: ['StringSetter', 'ExpressionSetter'],
          }
        ],
      },
    ],
    supports: {
      events: ['editorDidMount', 'editorWillMount', 'onChange'],
      style: true,
    },
  },
};

const snippets = [
  {
    title: title,
    name: componentName,
    screenshot: screenshot,
    schema: {
      componentName: componentName,
      title: title,
      props: {
        value: '//input your code to here',
        height: '100%',
        language: 'json'
      },
    },
  },
];
export default {
  ...Meta,
  snippets
};
