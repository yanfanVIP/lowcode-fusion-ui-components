const menu = {}

export default (target, menus, event)=>{
  const div = document.createElement('div')
  div.style = `position: absolute; left: ${target.x}px; top: ${target.y}px; background: #fff; border-radius: 3px; box-shadow: 0 0 20px #ccc;`

  div.innerHTML = `
  <style>
  .fusion_popup_context_menu{ list-style: none; margin: 0; padding: 0; margin-bottom:0; }
  .fusion_popup_context_menu li{ padding: 20px; padding-top:5px; padding-bottom:5px; font-weight: 500; font-size: 15px; cursor: pointer; }
  .fusion_popup_context_menu:last-child{ border-bottom-left-radius: 3px; border-bottom-right-radius: 3px; }
  .fusion_popup_context_menu:first-child{ border-top-left-radius: 3px; border-top-right-radius: 3px; }
  .fusion_popup_context_menu li:hover{ background: #f0f0f0; }
  </style>
  <ul class="fusion_popup_context_menu">
   ${menus.map(menu=>`<li key="${menu.key}">${menu.label}</li>`).join('\n')}
  <ul>
  `

  const click = (e)=>{
    const key = e.target.getAttribute('key')
    const menu = menus.find(menu=>menu.key===key)
    event && event(menu)
  }
  Array.from(div.getElementsByTagName('li')).forEach(element => {
    element.addEventListener('click', click)
  })
  document.body.appendChild(div)
  const close = () => { 
    Array.from(div.getElementsByTagName('li')).forEach(element => {
      element.removeEventListener('click', click)
    })
    div && div.remove() 
    document.removeEventListener("click", close)
    document.removeEventListener("contextmenu", close)
  }
  document.addEventListener("click", close)
  document.addEventListener("contextmenu", close)
}