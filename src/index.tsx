import { version } from '../package';

// export { default as ProCard } from './components/pro-card';
// export type { CardProps, CardSectionProps } from './components/pro-card';

export { default as FusionDemo } from './components/demo';
export { default as FusionDynametic } from './components/fusion-dynametic';
export { default as CodeEditor } from './components/code-editor';
export { default as CollapsePanel } from './components/collapse_panel';
export { default as FusionTable } from './components/fusion-table';
export { default as TableSelect } from './components/table-select';
export { default as TableSelectDialog } from './components/table-select-dialog';
export { default as FusionButton } from './components/fusion-button';
export { default as FusionSwitchCase } from './components/switch-case';
export { default as FusionTimeLine } from './components/fusion-timeline';
export { default as FusionQuill } from './components/quill';
export { default as FusionCombination } from './components/combination';
export { default as FusionCascader } from './components/fusion-cascader';
export { default as FusionGrid } from './components/fusion-grid';
export { default as FusionTypography } from './components/typography';
export { default as FusionDiv } from './components/div';
export { default as FusionLowCode } from './components/fusion-lowcode';
export { default as FusionFrame } from './components/frame';
export { default as FusionCalendar } from './components/fusion-calendar';
export { default as FusionDialogForm } from './components/dialog-form';
export { default as FusionIcon } from './components/fusion-icon'
export { default as FusionProTable } from './components/fusion-pro-table'
export { default as FusionProModal } from './components/fusion-pro-modal'
export { default as FusionProPopconfirm } from './components/fusion-pro-popconfirm'
export { default as FusionProDrawer } from './components/fusion-pro-drawer'
export { default as FusionProContainer } from './components/fusion-pro-container'
export { default as FusionProDialog } from './components/fusion-pro-dialog';


export * from './components/page-header';
export * from './components/pro-form';
export * from './components/pro-table';
export * from './components/expand-table';
export * from './components/filter';
export * from './components/tab-container';
export * from './components/pro-drawer';
export * from './components/button';
export * from './components/button-group';
export { default as FusionCharts } from './components/charts/index';
export { default as FusionDatav } from './components/datav/index';
export { default as FusionG6 } from './components/g6/index';
export { default as StoryPlaceholder } from './components/story-placeholder';

const bizCssPrefix = 'fusion-ui';
const displayName = 'FusionUI';

export { bizCssPrefix, displayName, version };
