import React, { useState, useEffect } from 'react'
import { buildComponents, assetBundle, AssetLevel, AssetLoader } from '@alilc/lowcode-utils'
import ReactRenderer from '@alilc/lowcode-react-renderer'
import { injectComponents } from '@alilc/lowcode-plugin-inject'
import { Loading } from '@alifd/next'
import mergeWith from 'lodash/mergeWith'


window.LOWCODE_LIBRARYS = window.LOWCODE_LIBRARYS || [];

const Component = React.forwardRef((props,ref) =>{
  const [config, setConfig] = useState(null);

  const init = async({ packages, schema }) => {
    const _packages = JSON.parse(packages)
    const libraryMap = {};
    const libraryAsset = [];
    _packages.forEach(({ package: _package, library, urls, renderUrls }) => {
      libraryMap[_package] = library;
      if (renderUrls) {
        if(Array.isArray(renderUrls)){
          renderUrls.forEach(u=>{
            if(!window.LOWCODE_LIBRARYS.includes(u)){
              libraryAsset.push(u);
              window.LOWCODE_LIBRARYS.push(u)
            }
          })
        }else{
          if(!window.LOWCODE_LIBRARYS.includes(renderUrls)){
            libraryAsset.push(renderUrls);
            window.LOWCODE_LIBRARYS.push(renderUrls)
          }
        }
      } else if (urls) {
        if(Array.isArray(urls)){
          urls.forEach(u=>{
            if(!window.LOWCODE_LIBRARYS.includes(u)){
              libraryAsset.push(u);
              window.LOWCODE_LIBRARYS.push(u)
            }
          })
        }else{
          if(!window.LOWCODE_LIBRARYS.includes(urls)){
            libraryAsset.push(urls);
            window.LOWCODE_LIBRARYS.push(urls)
          }
        }
      }
    })

    if(libraryAsset && libraryAsset.length > 0){
      const assetLoader = new AssetLoader();
      await assetLoader.load(libraryAsset);
    }
    const { componentsMap: componentsMapArray, componentsTree, i18n, dataSource } = JSON.parse(schema);
    const componentsMap = {}
    componentsMapArray.forEach((component) => {
      componentsMap[component.componentName] = component;
    });
    const components = await injectComponents(buildComponents(libraryMap, componentsMap))
    const pageSchema = componentsTree[0];
    pageSchema._props_ = props

    setConfig({
      schema: pageSchema,
      components: components,
      i18n: i18n,
      dataSource: dataSource
    })
  }

  const initConfig = () => {
    if(!props.config){
      if(config){ setConfig(null) }
      return
    }
    if(typeof props.config == 'string'){
      request.get(props.config).then(res=>{
        console.log(res)
        init(res)
      })
    }else{
      init(props.config)
    }
  }

  useEffect(() => { initConfig() }, [props.config])

  if (!config) {
    return <div ref={ref} style={props.style}><Loading style={{ width: '100%', height:50}}/></div>
  }

  return (
    <ReactRenderer
      ref={ref} 
      style={props.style}
      schema={{
        ...config.schema,
        dataSource: mergeWith(config.schema.dataSource, config.dataSource),
      }}
      data={props.data}
      components={config.components}
      messages={config.i18n} />
  )
})

export default Component;

