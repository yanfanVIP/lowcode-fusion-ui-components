import React from 'react';

function Component(props) {

  const { children, dataSource, ...other } = props

  if(!dataSource || dataSource.length == 0){ 
    if(props.__designMode == 'design'){
      return <div style={{ width:'100%', height:'80px', display: 'flex', justifyContent: 'center', alignItems: 'center', fontSize: '18px',background: '#ddd',}}>请先设置数据</div> 
    }
    return <></>
  }

  const renderChildren = (data, index) => {
    if(!children || children.length == 0){
      if(props.__designMode == 'design'){
        return <div style={{ width:'100%', height:'100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: '#ddd',}}>请拖拽组件到容器内</div>
      }
      return null
    }
    return React.Children.map(children, (child) => React.cloneElement(child, { ...child.props, ...data, index }))
  }

  const columnSize = Math.ceil(Math.sqrt(dataSource.length))
  const width = 100 / columnSize
  const cell_style1 = { border: props.hasBorder ? "1px solid #ccc" : null, cursor: props.hasClick?'pointer': null, display:'flex', justifyContent:'center', alignItems:'center' }
  const cell_style2 = { height: 0, paddingBottom: "50%", paddingTop: "50%", border: props.hasBorder ? "1px solid #ccc" : null, cursor: props.hasClick?'pointer': null, display:'flex', justifyContent:'center', alignItems:'center' }

  const onClick = (e, data, index) => {
    e.preventDefault()
    if(props.hasClick){
      props.onClickGridCell && props.onClickGridCell(data, index)
    }
  }

  return (
    <div {...other} style={{...props.style, display: "grid", gridTemplateColumns: `repeat(${columnSize}, ${width}%)`, gridTemplateRows: `repeat(${columnSize}, ${width}%)`}}>
      {
        dataSource.map((data, index)=>{
          return (
            <div key={index} style={props.hasSquare ? cell_style2 : cell_style1} onClick={(e)=>onClick(e, data, index)}>
              {renderChildren(data, index)}
            </div>
          )
        })
      }
    </div>
  )
}

export default Component