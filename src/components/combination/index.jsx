import React from 'react'
import DragZoomContainer from 'drag-zoom-container'
import contextMenu from '../../utils/contextMenu'

const FusionCombination = (props) => {
  const ref = React.useRef(null)
  const {data} = props
  let target = null

  const onclick = (data, e) => {
    if(target){
      target.style.border = null
      target.style.boxShadow = null
    }
    target = e.currentTarget
    target.style.border = '1px solid red'
    target.style.boxShadow = '0 0 10px red'
    props.onclick && props.onclick(data)
  }

  if(!data){
    return <div ref={ref} style={{ height: 200, ...props.style, display: 'flex', justifyContent:'center', alignItems:'center'}}>无数据</div>
  }

  const handleContextMenu = (event, data) => {
    if(!data.menu && props.menus){ return }
    const menu = props.onContextMenu && props.onContextMenu(event, data, props.menus) || props.menus
    if(!menu){ return }
    event.preventDefault()
    contextMenu({ x:event.pageX, y:event.pageY}, menu, (menu=>{
      menu && menu.menuClick && menu.menuClick(data)
    }))
  }

  const childs = data.children || []

  const renderChild = (parent, childs) => {
    if(!childs || childs.length == 0){ return [] }
    const results = []
    childs.forEach((child, index)=>{
      child._x = child._x || parseInt(parent.x || 0) + parseInt(child.x || 0)
      child._y = child._y || parseInt(parent.y || 0) + parseInt(child.y || 0)
      child.x = child._x
      child.y = child._y
      results.push(renderItem(child))
      if(child.children){
        renderChild(child, child.children).forEach(c=>{
          results.push(c)
        })
      }
    })
    return results
  }

  const renderItem = (child) => {
    return (
      <div key={child.id} title={child.title || child.name} 
        onClick={(e)=>onclick(child, e)} 
        onContextMenu={(e)=>{ handleContextMenu(e, child) }} 
        style={{ position: 'absolute', left: `${child.x}px`, top: `${child.y}px`}}>
          <div style={{ position: 'absolute', width:"100%", height:'100%', backgroundColor : child.color, opacity: 0.5 }}></div>
          <img alt={child.title || child.name} crossOrigin="anonymous" onClick={(e)=>onclick(child, e)} src={child.image}/>
      </div>
    )
  }

  return (
    <div ref={ref} style={{ height: 200, ...props.style}}>
      <DragZoomContainer zoomOnInner={true} zoomRange={{min:0.2, max:2}}>
        { renderItem(data) }
        { renderChild(data, data.children) }
      </DragZoomContainer>
    </div>
  )
}

export default FusionCombination;

