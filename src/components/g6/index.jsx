import React from 'react'
import G6 from '@antv/g6'
import { Menu } from '@alifd/next'

const FusionG6 = React.forwardRef((props, ref) => {
  const selected = React.useRef(null)
  const canvas = React.useRef(null)
  const minimap = React.useRef(null)
  const toolbar = React.useRef(null)
  let graph = canvas?.current?.graph

  React.useImperativeHandle(ref, ()=>({
    graph: ()=>{ return graph },
    setSelected: (ids) => {
      if(!graph){ return }
      graph.findAllByState('node', 'selected').forEach(node=>{ node.clearStates('selected') })
      graph.findAllByState('edge', 'selected').forEach(node=>{ node.clearStates('selected') })
      if(ids){
        ids.forEach(id=>{ 
          const node = graph.findById(id)
          if(node){
            node.setState('selected', true) 
          }
        })
      }
    },
    getSelected: ()=>{
      if(!graph){ return [] }
      const selected = []
      graph.findAllByState('node', 'selected').forEach(node=>{
        selected.push(node._cfg.model)
      })
      graph.findAllByState('edge', 'selected').forEach(node=>{
        selected.push(node._cfg.model)
      })
      return selected
    }
  }))
  
  React.useEffect(() => {
    initGraph()
  }, [props])

  const initGraph = () => {
    const plugins = []
    if(props.hasGrid){
      plugins.push(new G6.Grid())
    }
    if(props.hasSnapLine){
      plugins.push(new G6.SnapLine())
    }
    if(props.hasToolbar){
      plugins.push(new G6.ToolBar({ container: toolbar.current }))
    }
    if(props.hasMiniMap){
      plugins.push(new G6.Minimap({ container: minimap.current, type: 'keyShape' }))
    }
    if(props.menu){
      plugins.push(new G6.Menu({
        offsetX: 6,
        offsetX: 10,
        itemTypes: ['node', 'edge'],
        getContent(e) {
          return `<ul style="margin:0;margin-bottom:0;"> 
            ${ props.menu.filter(m=>m.type == e.item._cfg.type).map(menu=>`<li style="display: flex; align-items: center; padding-left:10px; padding-right:10px; cursor:pointer; min-width: 80px; height: 30px;" key="${menu.key}">${menu.title}</li>`).join('  ') } 
          </ul>`
        },
        handleMenuClick(target, item) {
          props.handleMenuClick && props.handleMenuClick(target.getAttribute('key'), item)
        },
      }))
    }

    let layout = null
    if(props.layout){
      layout = { type: props.layout }
    }
    if(!graph){
      graph = new G6.Graph({
        container: canvas.current,
        fitView: true,
        fitCenter: true,
        autoPaint: true,
        enabledStack: !props.readOnly,
        layout,
        modes: { default: props.modes },
        zoom: { min: 0.2, max: 6 },
        minZoom: 0.2,
        maxZoom: 6,
        defaultNode: {
          img: props.node_img,
          type: props.node_type,
          labelCfg: { position: 'bottom', offset:props.node_offset, style: { fill: '#333', fontSize: props.node_labelSize } },
          style: {
            width: props.node_size, 
            height: props.node_size,
            opacity: 0.7,
            fillOpacity: 0.7,
            cursor: 'pointer'
          }
        },
        nodeStateStyles: {
          active: {
            shadowColor: '#5584ff',
            shadowBlur: 3,
            opacity: 0.8,
            fillOpacity: 0.8,
          },
          selected: {
            shadowColor: 'red',
            shadowBlur: 10,
            opacity: 1,
            fillOpacity: 1
          },
        },
        defaultEdge: {
          lineWidth: props.lineWidth,
          style: { 
            lineWidth: 1,
            startArrow: props.hasStartArrow,
            endArrow: props.hasArrow,
            cursor: 'pointer'
          },
          labelCfg: { autoRotate:true, position: 'bottom', offset:props.edge_offset, style: { fill: '#333', fontSize: props.edge_labelSize } },
        },
        edgeStateStyles: {
          active: {
            shadowColor: '#5584ff',
            shadowBlur: 2,
            lineWidth: 2
          }, 
          selected: {
            shadowColor: 'red'
          },
        },
        plugins: plugins
      })
      canvas.current.graph = graph
      bindEvents(graph)
      graph.data(props.data || { nodes: [], edges:[] })
      graph.render()
      props.onInit && props.onInit(graph)
    }else if(props.__designMode == 'design'){
      graph.set('enabledStack', !props.readOnly)
      graph.set('layout', layout)
      graph.set('modes', { default: props.modes })
      graph.set('defaultNode', {
        img: props.node_img,
        type: props.node_type,
        labelCfg: { position: 'bottom', offset:props.node_offset, style: { fill: '#333', fontSize: props.node_labelSize } },
        style: {
          width: props.node_size, 
          height: props.node_size,
          opacity: 0.7,
          fillOpacity: 0.7,
          cursor: 'pointer'
        }
      })
      graph.set('defaultEdge', {
        lineWidth: props.lineWidth,
        style: { 
          lineWidth: 1,
          startArrow: props.hasStartArrow,
          endArrow: props.hasArrow,
          cursor: 'pointer'
        },
        labelCfg: { autoRotate:true, position: 'bottom', offset:props.edge_offset, style: { fill: '#333', fontSize: props.edge_labelSize } },
      })
      graph.set('plugins', plugins)
      graph.data(props.data || { nodes: [], edges:[] })
      graph.render()
    }else{
      graph.data(props.data || { nodes: [], edges:[] })
      graph.render()
    }
    graph.findAllByState('node', 'selected').forEach(node=>{ node.clearStates('selected') })
    graph.findAllByState('edge', 'selected').forEach(node=>{ node.clearStates('selected') })
    if(props.selected){
      props.selected.forEach(id=>{ 
        const node = graph.findById(id)
        if(node){
          node.setState('selected', true) 
        }
      })
    }else{
      graph.findAllByState('node', 'selected').forEach(node=>{ node.clearStates('selected') })
      graph.findAllByState('edge', 'selected').forEach(node=>{ node.clearStates('selected') })
    }
  }

  const bindEvents = (graph) => {
    graph.on('click', async (evt)=>{ 
      const {item} = evt
      if(!item){ return }
      const { type, model } = item._cfg
      if(!model){ return }
      if(type != 'node' && type != 'edge'){ return }

      if(props.selected_mode == 'none'){ return }
      if(props.selected_type == 'node' && type != 'node'){ return }
      if(props.selected_type == 'edge' && type != 'edge'){ return }

      const hasSelected = item.hasState('selected')
      if(hasSelected){
        item.clearStates('selected')
      }else{
        if(props.selected_mode == 'single'){
          graph.findAllByState('node', 'selected').forEach(node=>{ node.clearStates('selected') })
          graph.findAllByState('edge', 'selected').forEach(node=>{ node.clearStates('selected') })
          item.setState('selected', true)
        }else{
          item.setState('selected', true)
        }
      }

      if(props.click){ 
        props.click(graph, item, type, model) 
      }

      if(props.onSelect){
        const selected = []
        graph.findAllByState('node', 'selected').forEach(node=>{
          selected.push(node._cfg.model)
        })
        graph.findAllByState('edge', 'selected').forEach(node=>{
          selected.push(node._cfg.model)
        })
        props.onSelect(selected, graph, item, type, model)
      }
    }) 
    if(props.dblclick){ 
      graph.on('dblclick',  (evt)=>{ 
        const {item} = evt
        if(!item){ return }
        const { type, model } = item._cfg
        if(!model){ return }
        if(type != 'node' && type != 'edge'){ return }
        props.dblclick(graph, item, type, model) 
      })
    }
    if(props.contextmenu){ 
      graph.on('contextmenu', (evt)=>{ 
        const {item} = evt
        if(!item){ return }
        const { type, model } = item._cfg
        if(!model){ return }
        if(type != 'node' && type != 'edge'){ return }
        props.contextmenu(graph, item, type, model) 
      })
    }

    if(props.move){ 
      const drag = {}
      graph.on('dragstart', evt=>{
        const {item} = evt
        if(!item){ return }
        const { type, model } = item._cfg
        if(!model){ return }
        if(type != 'node'){ return }
        drag.start = { x: evt.x, y: evt.y }
        drag.model = model
        drag.item = item
        drag.end = null
      })
  
      graph.on('dragend', evt=>{
        const {item} = evt
        if(!item){ return }
        const { type, model } = item._cfg
        if(!model){ return }
        if(type != 'node'){ return }
        if(!drag.start || !drag.model || !drag.item){ return }
        drag.end = { x: evt.x, y: evt.y }
        props.move(drag, graph)
      })
    }
  }

  return (
    <div ref={ref} style={{ position: 'relative', ...props.style }}>
      <div ref={canvas} style={{width:'100%', height:'100%'}}/>
      <div ref={toolbar} style={{position: 'absolute', width: "50px", right: 0, top: 0 }}></div>
      <div ref={minimap} style={{position: 'absolute', right: 0, bottom: 0 }}></div>
    </div>
  )
})

export default FusionG6;

