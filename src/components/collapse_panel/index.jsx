import React from 'react';
import { Collapse } from '@alifd/next';

const CollapsePanel = React.forwardRef((props, ref) =>{
  return (
    <Collapse.Panel {...props} ref={ref}>{props.children}</Collapse.Panel>
  )
})

export default CollapsePanel;

