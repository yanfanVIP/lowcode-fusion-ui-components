import React from "react"
import { Button } from '@alifd/next';

import { ProTable, ProTableProps } from '../../index';
import { Space } from '@/components/container';

export interface EditTableProps extends ProTableProps {
  addButton: boolean,
  isPreview: boolean;
  addPosition?: 'end' | 'start';
  value: any[],
  formItemProps: any;
  renderPreview: Function;
  onChange?: (dataSource: any[]) => void;
  onInit?: (props: EditTableProps, dataSourceRef: any) => void;
  onEditItem?: (rowIndex: number, record: Record<string, any>, dataSource: any[]) => void;
  onSave?: (rowIndex: number, record: Record<string, any>, dataSource: any[]) => void;
  onRemove?: (rowIndex: number, record: Record<string, any>, dataSource: any[]) => void;
  onCancel?: (rowIndex: number, record: Record<string, any>, dataSource: any[]) => void;
}

export const EditTable = function (props: EditTableProps) {
  const {
    primaryKey = 'id',
    addButton,
    dataSource: propDataSource,
    value,
    actionColumnButtons,
    onInit,
    onSave,
    onEditItem,
    onRemove,
    onCancel,
    onChange,
    renderPreview,
    addPosition = 'end',
    formItemProps,
    isPreview,
    ...otherProps
  } = props;

  const [dataSource, _setDataSource] = React.useState(propDataSource || value || []);
  const dataSourceRef = React.useRef(dataSource);

  React.useEffect(() => {
    if(onInit){
      onInit(props, setDataSource)
    }else{
      _setDataSource(props.value || propDataSource || [])
    }
  }, [value, propDataSource]);

  const setDataSource = (ds) => {
    _setDataSource(ds || [])
    onChange && onChange(ds)
  }

  function actionColumnButtonsHidden(showInEdit) {
    return ({ rowRecord }) => {
      return showInEdit ? !!rowRecord.editMode : !rowRecord.editMode;
    };
  }
  const defaultActionColumnButtons = isPreview || !addButton ? [] : [
    {
      children: '编辑',
      type: 'primary',
      onClick(e, payload) {
        const { rowIndex, rowKey, rowRecord } = payload;
        if(onEditItem){
          const _data = onEditItem(rowIndex, rowRecord, rowRecord)
          setDataSource(_data);
        }else{
          const _data = dataSource.map((item) => {
            if (item[primaryKey] === rowKey) {
              return Object.assign(item, rowRecord, { editMode: true });
            }
            return item;
          });
          setDataSource(_data);
        }
      },
      hidden: actionColumnButtonsHidden(true),
    },
    {
      children: '保存',
      type: 'primary',
      onClick(e, payload) {
        const { rowIndex, rowKey, rowRecord } = payload;
        const _data = dataSource.map((item) => {
          if (item[primaryKey] === rowKey) {
            return Object.assign(item, rowRecord, { editMode: false });
          }
          return item;
        });
        onSave && onSave(rowIndex, rowRecord, _data);
        setDataSource(_data);
      },
      hidden: actionColumnButtonsHidden(false),
    },
    {
      children: '取消',
      type: 'primary',
      onClick(e, payload) {
        const { rowIndex, rowKey, rowRecord } = payload;
        const _data = dataSourceRef.current.map((item) => {
          if (item[primaryKey] === rowKey) {
            const keys = Object.keys(item);
            const originKeys = keys.filter((key) => key.startsWith('origin-'));
            originKeys.forEach((originKey) => {
              item[originKey.replace('origin-', '')] = item[originKey];
            });
            return Object.assign(item, { editMode: false });
          }
          return item;
        });
        onCancel && onCancel(rowIndex, rowRecord, _data);
        setDataSource(_data);
      },
      hidden: actionColumnButtonsHidden(false),
    },
    {
      children: '删除',
      type: 'primary',
      onClick(e, payload) {
        const { rowKey, rowIndex, rowRecord } = payload;
        dataSourceRef.current = dataSourceRef.current.filter((item) => item[primaryKey] !== rowKey);
        setDataSource(dataSourceRef.current);
        onRemove && onRemove(rowIndex, rowRecord, dataSourceRef.current);
      },
    },
  ];

  const tableAfter = (
    <Space className="row-add" align="center" justify="center">
      <Button
        text
        type="primary"
        onClick={() => {
          let _data;
          if (addPosition && addPosition === 'start') {
            _data = [
              {
                [primaryKey]: `id-${Math.random().toString(36).substr(-6)}`,
                editMode: true,
              },
            ].concat(dataSource);
          } else {
            _data = dataSource.concat([
              {
                [primaryKey]: `id-${Math.random().toString(36).substr(-6)}`,
                editMode: true,
              },
            ]);
          }
          dataSourceRef.current = _data;
          setDataSource(_data);
        }}
      >
        +新增一行
      </Button>
    </Space>
  )

  const onSortDatasource = (type, value, index, ds) => {
    if(type == 'UP'){
      if(index == 0){ return }
      const _data = [...dataSource]
      const _d = dataSource[index - 1]
      _data[index - 1] = _data[index]
      _data[index] = _d
      setDataSource(_data)
    }
    if(type == 'DOWN'){
      if(index >= dataSource.length - 1){ return }
      const _data = [...dataSource]
      const _d = dataSource[index]
      _data[index] = dataSource[index + 1]
      _data[index + 1] = _d
      setDataSource(_data)
    }
  }
  
  return (
    <ProTable {...otherProps} className="fusion-ui-edit-table" 
      tableAfter={addButton && !isPreview ? tableAfter : null} dataSource={dataSource} 
      onSortDatasource={onSortDatasource}
      actionColumnButtons={{ ...actionColumnButtons, dataSource: [...defaultActionColumnButtons, ...(actionColumnButtons?.dataSource || [])] }} />
  );
};