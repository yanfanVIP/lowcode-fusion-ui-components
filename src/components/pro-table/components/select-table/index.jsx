import React from "react"
import { Button } from '@alifd/next'
import { ProTable } from '../../index'

export const SelectFromTable = function (props) {
  const {
    primaryKey = 'id',
    addButton,
    dataSource: propDataSource,
    value,
    actionColumnButtons,
    onInit,
    onChange,
    ...otherProps
  } = props;

  const [dataSource, _setDataSource] = React.useState(props.dataSource || []);
  const [selected, _setSelected] = React.useState(Array.isArray(props.value) ? props.value : props.value ? [props.value] : []);

  React.useEffect(() => {
    _setDataSource(props.dataSource || [])
  }, [props.dataSource])

  React.useEffect(() => {
    _setSelected(Array.isArray(props.value) ? props.value : props.value ? [props.value] : [])
  }, [props.value])

  const setDataSource = (ds) => {
    _setDataSource(ds || [])
  }

  const onSelectChange = (keys, isReverseSelection, selectedRecords) => {
    props.onChange && props.onChange(keys)
  }

  const onSortDatasource = (type, value, index, ds) => {
    if(type == 'UP'){
      if(index == 0){ return }
      const _data = [...dataSource]
      const _d = dataSource[index - 1]
      _data[index - 1] = _data[index]
      _data[index] = _d
      setDataSource(_data)
    }
    if(type == 'DOWN'){
      if(index >= dataSource.length - 1){ return }
      const _data = [...dataSource]
      const _d = dataSource[index]
      _data[index] = dataSource[index + 1]
      _data[index + 1] = _d
      setDataSource(_data)
    }
  }

  return (
    <ProTable className="fusion-ui-cospand-table" {...otherProps} dataSource={dataSource} onSortDatasource={onSortDatasource}
      rowSelection={{...props.rowSelection, selectedRowKeys:selected, onChange:onSelectChange }}
    />
  );
};