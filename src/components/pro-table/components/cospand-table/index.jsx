import React from "react"
import { Button } from '@alifd/next'
import { ProTable } from '../../index'

export const CospandTable = function (props) {
  const {
    primaryKey = 'id',
    addButton,
    dataSource: propDataSource,
    value,
    actionColumnButtons,
    onInit,
    onChange,
    children,
    children_field,
    ...otherProps
  } = props;

  const [dataSource, _setDataSource] = React.useState(props.dataSource || value || []);

  React.useEffect(() => {
    if(onInit){
      onInit(props, setDataSource)
    }else{
      _setDataSource(props.value || props.dataSource || [])
    }
  }, [props.value, props.dataSource])

  const setDataSource = (ds) => {
    _setDataSource(ds || [])
    onChange && onChange(ds)
  }

  const expandedRowRender = () => {
    if(!children) return null
    return (record, index) => {
      const _p = { record, index }
      _p[children_field] = record.children

      const _onChange = (children) => {
        const ds = dataSource.map(c=>{
          if(c.id === record.id){
            c.children = children
          }
          return c
        })
        setDataSource(ds)
      }

      return (
        <div class="cospantab">
          { 
            React.Children.map(children, (child, index) =>{
              if(child.props.componentName == 'Slot'){
                return React.Children.map(child.props.children, (c, i) =>{
                  return React.cloneElement(c, { key:`cospantab-${index}-${i}`, ...c.props, ..._p, onChange: _onChange})
                })
              }
              return React.cloneElement(child, { key:`cospantab-${index}`, ...child.props, ..._p, onChange: _onChange})
            })
          }
        </div>
      )
    }
  }

  const onSortDatasource = (type, value, index, ds) => {
    if(type == 'UP'){
      if(index == 0){ return }
      const _data = [...dataSource]
      const _d = dataSource[index - 1]
      _data[index - 1] = _data[index]
      _data[index] = _d
      setDataSource(_data)
    }
    if(type == 'DOWN'){
      if(index >= dataSource.length - 1){ return }
      const _data = [...dataSource]
      const _d = dataSource[index]
      _data[index] = dataSource[index + 1]
      _data[index + 1] = _d
      setDataSource(_data)
    }
  }

  return (
    <ProTable className="fusion-ui-cospand-table"
      {...otherProps}
      dataSource={dataSource} onSortDatasource={onSortDatasource}
      actionColumnButtons={{ ...actionColumnButtons, dataSource: actionColumnButtons?.dataSource || [] }}
      expandedRowRender={expandedRowRender()}
    />
  );
};