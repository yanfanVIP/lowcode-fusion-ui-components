import React from "react"
import { Dialog } from '@alifd/next'
import { ProTable } from '../../index'

export const DialogTable = React.forwardRef((props, ref) => {
  const rowSelection = props.rowSelection || { mode:'single' }

  React.useImperativeHandle(ref, ()=>({
    open : async(onSelected) => {
      let selected = []
      rowSelection.onChange = (ids, record, rows) => { 
        selected = rows 
      }
      const form = <ProTable className="fusion-ui-cospand-table" {...props} rowSelection={rowSelection}/>
      const dialog = Next.Dialog.show({
        title: '请选择',
        width:'75%', 
        v2: true, 
        hasMask: true, 
        footer: true, 
        content: form,
        onOk:()=>{
          dialog.hide()
          if(rowSelection.mode == 'multiple'){
            onSelected(selected || [])
          }else{
            onSelected(selected && selected.length > 0 ? selected[0] : null)
          }
        }
      })
    }
  }))

  if(props.__designMode != 'design'){ return null }

  const dialogProps = {
    title: '请选择',
    width:'75%', 
    v2: true, 
    hasMask: true, 
    footer: true, 
    visible: true
  }
  
  return (
    <Dialog ref={ref} {...dialogProps}>
      <ProTable className="fusion-ui-cospand-table" {...props} />
    </Dialog>
  )
})