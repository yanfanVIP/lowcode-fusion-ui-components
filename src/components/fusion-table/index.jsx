import React, { useEffect, useRef, useState } from 'react'
import { ProTable, BetaSchemaForm } from '@ant-design/pro-components'
import { Button, Icon, Dialog, Notification } from '@alifd/next'
import TableComulnUtil from '../table-select/TableComulnUtil'
import FormComulnUtil from '../table-select/FormComulnUtil'
import JSON from './jsonplus'

const formatUrl = (obj={}, format) => {
  Object.keys(obj).map(k=>{
    format = format.replace(':' + k, `${obj[k]}`)
  })
  return format
}

export default React.forwardRef((props, _ref) => {
  const [tableColumns, setTableColumns] = useState([])
  const [tableName, setTableName] = useState(null)
  const ref = useRef()
  const [filter, setFilter] = useState(null)
  const [sort, setSort] = useState([])
  const [columnConfig, setColumnConfig] = useState(null)

  let tableConfig = null
  let table = props.config?.table || props.table
  const query_id = props.query_id
  const option = props.option
  const options = props.options
  const add_label = props.add_label
  const add_label_icon = props.add_label_icon
  let reset = false
  let idField = 'id'
  let dialog = null
  

  let auths = props.auths || {}
  if(Array.isArray(props.auths)){
    props.auths.forEach(auth=>{
      auths[auth.key] = auth.value
    })
  }

  React.useImperativeHandle(_ref, ()=>({
    current: () => ref.current,
    reload: ref.current.reload
  }))

  useEffect(() => { initTableConfig() }, [props.table, props.config, props.option])


  const initTableConfig = async() => {
    if(!table){ return }
    if(tableConfig){ return tableConfig }
    tableConfig = props.config || await request.get(`/api/db.table/schema/${table}`)
    tableConfig = JSON.decode(tableConfig)
    table = tableConfig.table
    idField = tableConfig.config.primary
    setTableName(tableConfig.config.comment)

    const tableColumns = TableComulnUtil(tableConfig)
    const hasOptions = options?.map(o=>auths[o.key])
    if(auths.EDIT || auths.DELETE || auths.DETAIL || hasOptions){
      tableColumns.push({
        title: '操作',
        valueType: 'option',
        key: 'option',
        render: (text, record, _, action) => {
          const option = []
          if(auths.DETAIL){ option.push(<a key="detail" onClick={()=>onDetail(record)}>查看</a>) }
          if(auths.EDIT){ option.push(<a key="editable" onClick={()=>onEdit(record)}>编辑</a>) }
          if(auths.DELETE){ option.push(<a key="delete" onClick={()=>onDelete(record)}>删除</a>) }
          options && options.forEach(o=>{
            if(auths[o.key]){ 
              if(o.show && (o.show == true || o.show(text, record, _, action))){
                option.push(<a key="delete" onClick={()=>props.onOption && props.onOption(o.key, record, ref)}>{o.value}</a>) 
              }
            }
          })
          return option
        }
      })
    }

    setTableColumns(tableColumns)
    if(option == 'DETAIL'){
      const row = await request.get(`/api/db.table/${table}/${query_id}`)
      if(row){
        onDetail(row)
        return
      }
      Notification.error({ title: `没有找到数据${query_id}` })
    }
    return tableConfig
  }

  const req = async(req = {}, _sort) => {
    const config = await initTableConfig()
    const filter = {
      ...(config?.default?.filters || {}),
      ...req
    }
    if(option == 'DETAIL' && !reset){
      filter.id = query_id
    }
    delete filter.current
    delete filter.pageSize
    _sort = Object.keys(_sort).map(key=>key.indexOf('.') == -1 ? `${table}.${key} ${_sort[key] == 'ascend' ? ' ASC' : ' DESC'}` :`${key} ${_sort[key] == 'ascend' ? ' ASC' : ' DESC'}`)
    const sort = [..._sort, ...(config?.default?.sort || [])]
    setFilter(filter)
    setSort(sort)
    const data = await request.post(`/api/db.table/${table}`, { page: req.current, pageSize: req.pageSize, filter, sort })
    return { data: data.data, success: true, total: data.count }
  }

  const onExport = () => {
    const columns = tableColumns.filter(t=>{
      if(t.valueType == 'option'){ return false } // 操作列不导出
      if(t.valueType == 'password'){ return false } // 密码列不导出
      if(t.hideInTable){ return false }
      if(columnConfig[t.key] && columnConfig[t.key].show == false){ return false }
      return true
    }).map((t, index)=>{
      return { title: t.title, key: t.key, order: columnConfig[t.key]?.order || index  }
    })
    .filter(c=>c)
    .sort((a,b)=>a.order-b.order)
    fetch(`/api/db.table/all/${props.table}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ filter, sort, columns })
    }).then(async res => {
      const blob = await res.blob()
      const filename = res.headers.get('filename')
      var link = document.createElement('a')
      link.href = window.URL.createObjectURL(blob)
      link.download = decodeURIComponent(filename)
      link.click()
      window.URL.revokeObjectURL(link.href)
    })
  }

  const onAdd = async() => {
    if(props.onAdd){
      props.onAdd()
      return
    }
    const config = await initTableConfig()
    const formColumns = FormComulnUtil(config, true)
    const form = <BetaSchemaForm layoutType="Form" rowProps={{ gutter: [16, 16] }} colProps={{ span: 12 }} grid={true} searchConfig={config.buttons} onFinish={form=>{ onFinish('ADD', {}, form) }} columns={formColumns}/>
    dialog = Dialog.show({ title: `新增${config.comment}`, width:'800px', v2: true, hasMask: true, footer: false, content: form})
  }

  const onDetail = async(row) => {
    if(props.onDetail){
      props.onDetail(row)
      return
    }
    const config = await initTableConfig()
    const formColumns = FormComulnUtil(config, false)
    const form = <BetaSchemaForm submitter={false} layoutType="Form" initialValues={row} rowProps={{ gutter: [16, 16] }} colProps={{ span: 12 }} grid={true} searchConfig={config.buttons} onFinish={form=>{ onFinish('DETAIL', row, form) }} columns={formColumns}/>
    dialog = Dialog.show({ title: `${config.comment}详情`, width:'800px', v2: true, hasMask: true, footer: false, content: form})
  }

  const onEdit = async(row) => {
    if(props.onEdit){
      props.onEdit(row)
      return
    }
    const config = await initTableConfig()
    const formColumns = FormComulnUtil(config, true)
    const form = <BetaSchemaForm layoutType="Form" initialValues={row} rowProps={{ gutter: [16, 16] }} colProps={{ span: 12 }} grid={true} searchConfig={config.buttons} onFinish={form=>{ onFinish('EDIT', row, form) }} columns={formColumns}/>
    dialog = Dialog.show({ title: `编辑${config.comment}`, width:'800px', v2: true, hasMask: true, footer: false, content: form})
  }

  const onDelete = (row) => {
    if(props.onDelete){
      props.onDelete(row)
      return
    }
    Dialog.warning({ title: `确定要删除这条数据?`, v2: true, hasMask: true, onOk: async()=>{
      await request.post(formatUrl(row, auths.DELETE))
      ref.current.reload()
    }})
  }

  const onFinish = async(TYPE, row, values) => {
    if(props.onFinish){
      props.onFinish(TYPE, row, values)
      return
    }
    if(TYPE == 'ADD' && auths.ADD){
      await request.post(formatUrl(row, auths.ADD), values)
    }else if(TYPE == 'EDIT' && auths.EDIT){
      await request.post(formatUrl(row, auths.EDIT), values)
    }else{
      console.log(values)
    }
    ref.current.reload()
    dialog && dialog.hide()
  }

  const toolBarButton = () => {
    const toolBarButton = []
    if(auths.ADD){ toolBarButton.push(<Button key="button" onClick={onAdd} type="primary"><Icon type="add" />新建</Button>) }
    toolBarButton.push(<Button key="button" onClick={onExport} type="primary"><Icon type="download" />导出</Button>)
    return toolBarButton
  }

  return (
    <div ref={_ref}>
      <ProTable actionRef={ref} toolbar={{title: tableName}} scroll={{x:true}} cardBordered rowKey={idField} columns={tableColumns} 
      columnsState={{ value:columnConfig, persistenceKey:`PageTable_${props.table}_columns`, persistenceType: 'localStorage', onChange:(values)=>{ setColumnConfig(values) } }} 
      toolBarRender={toolBarButton} pagination={{ pageSize: 10 }} request={req} onReset={()=>{ reset = true }}/>
    </div>
  )
})