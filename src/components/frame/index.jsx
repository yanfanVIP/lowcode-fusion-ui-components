import React from 'react';

function Component(props) {
  return (
    <iframe frameBorder={0} style={{width:"100%", height:'100%', background: props.background || '#fff', ...props.style}} src={props.url} onLoad={props.onLoad}></iframe>
  )
}

export default Component;

