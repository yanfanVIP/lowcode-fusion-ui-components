import * as React from 'react'
import { Component, createRef } from 'react'
import {
  ProTable as OriginalProTable,
  ActionType,
  ProColumnType
} from '@ant-design/pro-components'
import type { ProFormInstance } from '@ant-design/pro-components'
import type { TablePaginationConfig } from 'antd'
import { Tag, ConfigProvider } from 'antd'
import zhCNIntl from 'antd/es/locale/zh_CN'
import enUSIntl from 'antd/es/locale/en_US'
import { defineGetterProperties, isPlainObj } from '../../common/index'
import { FormProps } from 'rc-field-form/lib/Form'

interface IValueEnum {
  text: string
  value: string
  status: string
}

type IExtendsColType = ProColumnType & {
  valueEnum?: IValueEnum[]
  renderTag?: boolean
}

export type IProTableProps = React.ComponentProps<typeof OriginalProTable> & {
  columns?: IExtendsColType
  intl?: string
  value: any[],
  defaultValue: any[],
  onValuesChange?: FormProps['onValuesChange'],
  onChange: (value: any[]) => void
}

const intlMap = {
  zhCNIntl,
  enUSIntl
}

class ProTable extends Component<IProTableProps, any> {
  state = {
    selectedRowKeys: (this.props.rowSelection as any)?.selectedRowKeys ?? [],
    selectedRows: [],
    collapsed: this.props.search === false ? undefined : this.props.search?.defaultCollapsed
  }

  actionRef = createRef<ActionType>()

  formRef = createRef<ProFormInstance>()

  onSelectRowsChange = (selectedRowKeys, selectedRows) => {
    this.setState({
      selectedRowKeys,
      selectedRows
    })
  }

  getSelectedRowKeys() {
    return this.state.selectedRowKeys
  }

  getSelectedRows() {
    return this.state.selectedRows
  }

  setSelectedRowKeys(selectedRowKeys) {
    this.setState({
      selectedRowKeys: Array.isArray(selectedRowKeys)
        ? selectedRowKeys
        : [selectedRowKeys]
    })
  }

  componentDidMount() {
    defineGetterProperties(this, [this.actionRef, this.formRef])
  }

  render() {
    const { value, defaultValue, columns, rowSelection, intl, onValuesChange, toolBarRender, toolBarRenderOpen } = this.props
    const { selectedRowKeys, collapsed } = this.state

    // 劫持渲染标签类型的列
    columns?.map((item) => {
      if (isPlainObj(item.valueEnum) && (item as any).renderTag === true) {
        item.render = (_, record) => {
          const colValue = record[item.dataIndex as string]

          const target = item.valueEnum[colValue]

          return target?.text ? (
            <Tag color={target?.status?.toLowerCase()}>{target?.text}</Tag>
          ) : (
            '-'
          )
        }
      }
    })

    const pagination = this.props.pagination as TablePaginationConfig

    // current 让用户自己配置的话，用户需要自己监听 onChange 事件去修改，对低代码平台不友好
    if (typeof pagination?.current === 'number') {
      delete pagination.current
    }

    if (typeof pagination?.total === 'number') {
      delete pagination.total
    }
    
    const toolBarRenderFunc = () => {
      if (toolBarRenderOpen) {
        if (toolBarRender === false) {
          return null;
        } else {
          return toolBarRender;
        }
      } else {
        return false;
      }
    };

    return (
      <ConfigProvider locale={intlMap[intl || 'zhCNIntl']}>
        <OriginalProTable
          {...this.props}
          pagination={(!pagination || pagination.hidden) ? false : pagination}
          dataSource={value || defaultValue || this.props.dataSource}
          search={{
            ...this.props.search,
            collapsed,
            onCollapse: () => {
              if (this.props.search === false) return
              this.setState({
                collapsed: !collapsed
              })
              if (this.props.search.onCollapse) {
                // 如果设置了函数则继续执行
                this.props.search.onCollapse(!collapsed)
              }
            }
          }}
          rowSelection={
            rowSelection
              ? {
                  ...rowSelection,
                  defaultSelectedRowKeys: selectedRowKeys,
                  selectedRowKeys,
                  onChange: (...args) => {
                    rowSelection?.onChange?.(...args)
                    this.onSelectRowsChange(...args)
                  }
                }
              : false
          }
          columns={columns}
          actionRef={this.actionRef}
          formRef={this.formRef}
          form={{ onValuesChange: onValuesChange }}
          toolBarRender={toolBarRenderFunc()}
        />
      </ConfigProvider>
    )
  }
}

export default ProTable
