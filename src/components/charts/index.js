import React, { useState, useEffect } from 'react'
import * as bizcharts from 'bizcharts';

const FusionCharts = (props)=>{
  const { _componentName, component, ...other } = props
  const [data, setData] = useState(props.data)

  useEffect(() => { setData(props.data) }, [props.data])
  
  const Component = bizcharts[component || _componentName.substring(_componentName.indexOf('.') + 1)]

  if(Component){
    return <Component padding="auto" {...other} data={data}></Component>
  }
  return <div>请设置组件名称</div>
}

export default FusionCharts 