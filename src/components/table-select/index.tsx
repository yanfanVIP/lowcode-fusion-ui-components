import React from 'react'
import TableSelectComponent from './TableSelectComponent'
import { InputProps } from '@alifd/next/lib/input'

interface ComponentProps {
  table: string; //表名称
  value: any;
  fieldProps: InputProps;
  multi: boolean; //是否可以多选
  entity: string; //值字段
  filters: any;
  onChange: Function;
  isPreview: boolean;
}

function TableSelect(props: ComponentProps){
  return <TableSelectComponent {...props} />
}


export default TableSelect