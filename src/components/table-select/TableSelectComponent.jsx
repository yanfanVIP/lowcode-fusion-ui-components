import React, { useState, useEffect } from "react"
import { ProForm, ProTable } from '@ant-design/pro-components';
import { Input, Icon, Dialog } from '@alifd/next'
import TableComulnUtil from './TableComulnUtil'


const TableSelectComponent = function Component(props){
  const readOnly = props.isPreview || props.fieldProps?.readOnly || false
  
  const [value, setValue] = useState(props.value)
  const [display_value, setDisplayValue] = useState(props.value)
  let tableConfig = null


  useEffect(() => { initTableConfig() }, [props.table])
  useEffect(() => { 
    setValue(props.value)
    updateDisplayValue(props.value) 
  }, [props.value])

  const updateDisplayValue = async(value) => {
    if(!value){
      setDisplayValue(value || '')
      return
    }
    const tableConfig = await initTableConfig()
    if(tableConfig.config.primary == props.entity){
      const getLable = (value) => {
        if(Array.isArray(value)){
          return request.get(`/api/db.table/entity/${props.table}?${value.map(v=>`id=${v}`).join('&')}`)
        }else{
          return request.get(`/api/db.table/entity/${props.table}?id=${value}`)
        }
      }
      const relations = await getLable(value)
      setDisplayValue(Object.values(relations).join(','))
    }else if(!props.entity){
      const getEntity = (value) => {
        if(!props.table){ return [] }
        if(Array.isArray(value)){
          return value.map(v=>v[tableConfig.config.entity])
        }else{
          return [value[tableConfig.config.entity]]
        }
      }
      const relations = getEntity(value)
      setDisplayValue(Object.values(relations).join(','))
    }else if(Array.isArray(value)){
      setDisplayValue(value.join(','))
    }else{
      setDisplayValue(value || '')
    }
  }

  const initTableConfig = async() => {
    if(!props.table){ return }
    if(tableConfig){ return tableConfig }

    if(props.config && typeof props.config == 'string'){
      const table = await request.get(props.config)
      tableConfig = table
      return table
    }else if(props.config){
      tableConfig = props.config
      return table
    }else{
      const table =  await request.get(`/api/db.table/schema/${props.table}`)
      tableConfig = props.config
      return table
    }
  }

  const openDialog = async() => {
    if(readOnly){ return }
    const tableConfig = await initTableConfig()
    const columns = TableComulnUtil(tableConfig, true)
    let defaultSelectedRowKeys = []
    if(tableConfig.config.primary == props.entity && value){
      defaultSelectedRowKeys = Array.isArray(value) ? value : [value]
    }
    let selected = []
    const onChange = (ids, rows) => { selected = rows }
    const form = (
      <ProTable toolbar={{title: tableConfig.config.comment}} scroll={{x:true}} cardBordered rowKey={tableConfig.config.primary} columns={columns}
        rowSelection={{ type:props.multi?'checkbox':'radio', preserveSelectedRowKeys:true, fixed:true, defaultSelectedRowKeys, onChange }} 
        columnsState={{ persistenceKey:`TableSelectComponent_${props.table}_columns`, persistenceType: 'localStorage' }} 
        options={{ setting: {listsHeight: 400} }} 
        pagination={{ pageSize: 5 }} 
        request={req}/>
    )
    const dialog = Next.Dialog.show({
      title: '请选择',
      width:'75%', 
      v2: true, 
      hasMask: true, 
      footer: true, 
      content: form,
      onOk:()=>{
        dialog && dialog.hide()
        if(selected && selected.length > 0){
          if(props.multi){
            const values = selected.map(item => props.entity ? item[props.entity] : item)
            setValue(values)
            updateDisplayValue(values)
            props.onChange && props.onChange(values)
          }else{
            const values = selected.map(item => props.entity ? item[props.entity] : item).find(i=>true)
            setValue(values)
            updateDisplayValue(values)
            props.onChange && props.onChange(values)
          }
        }else{
          setValue(null)
          updateDisplayValue(null)
          props.onChange && props.onChange(null)
        }
      }
    })
  }

  const req = async(req = {}, _sort) => {
    const filter = {
      ...(props.filters || {}),
      ...req
    }
    delete filter.current
    delete filter.pageSize
    const sort = Object.keys(_sort).map(key=>{
      return key.indexOf('.') == -1 ? `${props.table}.${key} ${_sort[key] == 'ascend' ? ' ASC' : ' DESC'}` :`${key} ${_sort[key] == 'ascend' ? ' ASC' : ' DESC'}`
    })
    const data = await request.post(props.url || `/api/db.table/${props.table}`, { page: req.current, pageSize: req.pageSize, filter, sort:[...sort, ...(props.default_sort || [])] })
    return { data: data.data, success: true, total: data.count }
  }

  if(!props.isPreview && readOnly){
    return <Input placeholder="Select ..." {...props.fieldProps} innerAfter={<Icon type="filter" style={{marginRight:5}}/>} value={display_value} readOnly={true}/>
  }

  if(readOnly){
    return <div class="next-form-preview">{display_value || ''}</div>
  }

  return (
    <Input placeholder="Select ..." {...props.fieldProps}
      onClick={openDialog} innerAfter={<Icon type="filter" style={{marginRight:5}}/>} 
      value={display_value} readOnly={true}/>
  )
}

export default TableSelectComponent