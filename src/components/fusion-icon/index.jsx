import * as Icon from '@ant-design/icons'

const component = (props) => {
  const Component = Icon[props.type]

  if(Component){
    return <Component {...props} />
  }
  return <></>
}

export default component