import React from "react"
import { Dialog } from '@alifd/next'
import { ProForm } from '../pro-form'

export default React.forwardRef((props, ref) => {
  const dialogProps = {
    title: '请填写',
    width:'75%', 
    v2: true, 
    hasMask: true, 
    footer: true, 
    visible: true
  }

  React.useImperativeHandle(ref, ()=>({
    open : async(onSubmit, initData) => {
      let field = null
      let pageInit = (myfield) => field = myfield
      const dialog = Next.Dialog.show({
        title: '请填写',
        width:'75%', 
        v2: true, 
        hasMask: true, 
        footer: true, 
        content: <ProForm data={initData || {}} {...props} pageInit={pageInit}/>,
        onOk:()=>{
          field.validatePromise().then(({ errors, values })=>{
            if(errors){ return }
            onSubmit && onSubmit(values)
            dialog.hide()
          })
          return false
        }
      })
    }
  }))

  if(props.__designMode != 'design'){ return null }

  return (
    <Dialog ref={ref} {...dialogProps}>
      <ProForm {...props} />
    </Dialog>
  )
})