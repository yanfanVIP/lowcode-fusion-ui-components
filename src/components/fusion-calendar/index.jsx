import * as React from 'react';
import { Calendar2 } from '@alifd/next';

function Component(props) {

  const dateCellRender = (date) => {
    const dateNum = date.date()
    if (!props.eventFunction) { return dateNum }
    let data = props.eventFunction ? props.eventFunction(date) : {}
    if(data != null && Array.isArray(data) && data.length > 0){
      return (
        <div className="custom-calendar-cell">
          <div className="custom-calendar-cell-value">{dateNum}</div>
          <div className="custom-calendar-cell-content">
            <ul className="event-list">
              {data.map((item, key) => (
                <li className={`${item.type}-event`} key={key}>
                  {item.content}
                </li>
              ))}
            </ul>
          </div>
        </div>
      )
    }

    if(data != null){
      return (
        <div className="custom-cell">
          {dateNum}
          <p>{data}</p>
        </div>
      )
    }
    return dateNum
  }

  return (
    <Calendar2 {...props} className={`${props.className} calendar-${props.theme}`} dateCellRender={dateCellRender} defaultValue={new Date()}/>
  )
}

export default Component;

