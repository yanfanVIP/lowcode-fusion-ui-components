import { Popconfirm } from 'antd'

const Component = (props) => {
  const { children } = props

  const renderChildren = () => {
    if(!children || children.length == 0){
      if(props.__designMode == 'design'){
        return <div style={{ width:'100%', height:'80px', display: 'flex', justifyContent: 'center', alignItems: 'center', background: '#ddd',}}>请拖拽组件到容器内</div>
      }
      return null
    }
    return React.Children.map(children, (child) => React.cloneElement(child, { ...child.props }))
  }

  return <div><Popconfirm {...props}>{ renderChildren() }</Popconfirm></div>
}

export default Component