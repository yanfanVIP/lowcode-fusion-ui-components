import React from 'react'
import { Typography } from '@alifd/next'

function Component(props) {
  const { type, value, children, ...other } = props

  switch (type) {
    case 'Typography':{
      if(!children || children.length == 0){
        if(props.__designMode == 'design'){
          return <div style={{height: '80px', display: 'flex', justifyContent: 'center', alignItems: 'center', fontSize: '18px',background: '#ddd',}}>请拖拽组件到容器内</div>
        }
        return <></>
      }
      return <Typography {...other}>{ React.Children.map(children, (child) => React.cloneElement(child, {...child.props })) }</Typography>
    }
    case 'Text': return <Typography.Text {...other}>{value}</Typography.Text>
    case 'Paragraph': return <Typography.Paragraph {...other}>{value}</Typography.Paragraph>
    case 'H1': return <Typography.H1 {...other}>{value}</Typography.H1>
    case 'H2': return <Typography.H2 {...other}>{value}</Typography.H2>
    case 'H3': return <Typography.H3 {...other}>{value}</Typography.H3>
    case 'H4': return <Typography.H4 {...other}>{value}</Typography.H4>
    case 'H5': return <Typography.H5 {...other}>{value}</Typography.H5>
    case 'H6': return <Typography.H6 {...other}>{value}</Typography.H6>
  }
  return <Typography.Text {...other}>{value}</Typography.Text>
}

export default Component;

