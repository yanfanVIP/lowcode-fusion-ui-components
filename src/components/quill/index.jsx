import React from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import 'react-quill/dist/quill.bubble.css'

const toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block', 'link'],
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction
  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],
  ['clean']                                         // remove formatting button
];

const Quill = React.forwardRef((props,ref) =>{
  const readOnly = props.isPreview || props.fieldProps?.readOnly || false
  const editor = React.useRef(null)
  const [value, setValue] = React.useState('')
  React.useEffect(()=>{ setValue(props.value) }, [props.value])

  React.useImperativeHandle(ref, ()=>({
    editor: editor.current,
    setValue: setValue,
    getValue: ()=>value,
  }))

  const onChange = (content, delta, source, editor) => {
    setValue(content)
    props.onChange && props.onChange(content, delta, source, editor)
  }

  return (
    <div ref={ref} style={{ ...props.style, height: props?.style?.height || props?.height || 200 }} >
      <ReactQuill ref={editor} {...props} 
        style={{ width: '100%', height: props?.style?.height ? props?.height : 'calc(100% - 42px)'}} 
        readOnly={readOnly} 
        modules={{ toolbar:toolbarOptions }} 
        value={value} onChange={onChange} />
    </div>
  )
})

export default Quill