import React, { useState, useImperativeHandle } from "react"
import { Collapse, Tag, Form, Field } from '@alifd/next';
import { Space } from '@/components/container';
import Operations from '@/common/operations';

export interface AnchorFormProps {
  isPreview: boolean;
  children?: React.ReactElement;
  pageInit: Function;
  onChange: Function;
  onSubmit: Function;
  title: string;
  operations?: any;
  operationConfig?: any;
  lastSaveTime?: any;
  data: any;
}

const CollapseForm = React.forwardRef((props: AnchorFormProps, ref: any) => {
  const {
    children,
    operations,
    operationConfig,
    lastSaveTime,
    ...otherProps
  } = props;

  if(!children){ return null }
  const empty = children.filter(c=>c).find(c=>c.type == 'div')
  if(empty){ return React.cloneElement(empty, { ref }) }

  if (!children) {
    return (
      <Space ref={ref} full align="center" verAlign="middle" style={{ minHeight: 50 }}>
        <span>内容为空</span>
      </Space>
    );
  }

  
  let myfield = Field.useField([])
  const [expandedKeys, setExpandedKeys] = useState([])
  const [fieldError, setFieldError] = useState({})
  const [isPreview, setIsPreview] = React.useState(props.isPreview);

  React.useEffect(()=>{ props.pageInit &&  props.pageInit() }, [])
  React.useEffect(()=>{
    if(props.data){
      myfield.setValues(props.data)
    }
  }, [props.data])

  React.useEffect(()=>{
    const expanded: any[] = []
    props.children.map((child, index) => {
      if(!child){ return }
      if(!child.props) { return }
      if(!child.props.__inner__.condition){ return }
      if(child.props.__inner__.hidden){ return }
      if(child.props.defaultClose){ return }
      expanded.push(`panel-${index}`)
    })
    setExpandedKeys(expanded)
  }, [props.children])

  React.useEffect(() => { 
    if(props.__designMode == 'design'){
      setIsPreview(props.isPreview) 
    }
  }, [props.isPreview]);

  useImperativeHandle(ref, ()=>({
    setValue : (key, value) => myfield.setValue(key, value),
    getValue : (key) => myfield.getValue(key),
    getFieldError : (key) => myfield.getFieldError(key),
    setFieldError : (key, error) => setFieldError({...fieldError, [key]: error}),
    getFieldValue : (key) => myfield.getFieldValue(key),
    setFieldValue : (key, value) => myfield.setFieldValue(key, value),
    getFieldProps : (key, options) => myfield.getFieldProps(key, options),
    getFieldValueProps : (key, options) => myfield.getFieldValueProps(key, options),
    getFieldErrorProps : (key, options) => myfield.getFieldErrorProps(key, options),
    setValues: (values) => myfield.setValues(values),
    getValues: () => myfield.getValues(),
    isPreview: ()=> isPreview,
    getValidateValues,
    setIsPreview:(flag)=>{
      setIsPreview(flag)
    },
    field: myfield,
  }))

  const getValidateValues = async() => {
    const {errors, values} = await myfield.validatePromise()
    const errorKeys = Object.keys(errors || {})

    const fieldError = {}
    props.children.forEach((child, index)=>{
      if(!child || !child.props || !child.props.children){ return }
      child.props.children.forEach(panel=>{
        if(!panel || !panel.props || !panel.props.children){ return }
        panel.props.children.forEach(f=>{
          if(!f || !f.props){ return }
          if(errorKeys.includes(f.props?.formItemProps?.name)){
            fieldError[`panel-${index}`] = (fieldError[`panel-${index}`] || 0) + 1
          }
        })
      })
    })
    setFieldError(fieldError)
    return {errors, values}
  }

  const onSubmit = async(e) => {
    e?.preventDefault && e.preventDefault()
    const values = await getValidateValues()
    props.onSubmit && props.onSubmit(values)
  }

  const onOpenColllapse = (key) => {
    if(expandedKeys.includes(key)){
      setExpandedKeys(expandedKeys.filter(k=>k !== key))
    }else{
      setExpandedKeys([...expandedKeys, key])
    }
  }


  const renderTitle = (child, index) => {
    return (
      <div style={{display: 'flex'}}>
        <span style={{marginRight: 5 }}>{child.props.title}</span>
        { fieldError[`panel-${index}`] ? <Tag type="primary" color="#f50" size="small">{fieldError[`panel-${index}`]}</Tag> : null }
      </div>
    )
  }

  return (
    <Form ref={ref} field={myfield} {...otherProps} isPreview={isPreview} onChange={props.onChange} onSubmit={onSubmit}>
      <Collapse expandedKeys={expandedKeys}>
      {
        props.children.map((child, index) => {
          if(!child){ return null }
          if(!child.props.__inner__.condition){ return null }
          if(child.props.__inner__.hidden){ return null }
          const title = renderTitle(child, index)
          const hasErrorStyle = {
            borderColor: '#f50',
            borderStyle: 'solid',
            borderWidth: '1px',
          }

          return (
            <Collapse.Panel ref={child.ref} key={`panel-${index}`} style={ fieldError[`panel-${index}`]? hasErrorStyle : null}
              disabled={child.props.disabled} title={title} 
              onClick={()=>{ onOpenColllapse(`panel-${index}`) }} >
              { 
                React.Children.map(child.props.children, (child: React.ReactElement<any, any>) => { 
                  return React.cloneElement(child, {...child.props, isPreview})
                })
              }
            </Collapse.Panel>
          )
        })
      }
      </Collapse>
      <Operations {...{ operations, operationConfig, lastSaveTime }} />
    </Form>
  )
})

export default CollapseForm;
