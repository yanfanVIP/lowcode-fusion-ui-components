import moment from 'moment';
import React, { useImperativeHandle } from "react"
import classnames from 'classnames';
import { FormProps } from '@alifd/next/lib/form';
import { ResponsiveGrid, Form, Field } from '@alifd/next';

import { ObjUtils } from '@/utils';
import { bizCssPrefix } from '@/variables';
import Operations from '@/common/operations';
import ProFormItem from '@/components/pro-form/components/form-item';

moment.locale('zh-cn');
const cssPrefix = `${bizCssPrefix}-pro-form`;

const execute_context = (str, data) => {
  const dataProps = Object.keys(data).map(key => `const ${key} = data.${key};`).join('');
  const funcStr = `${dataProps}; return ${str}; `;
  const templateFunc = new Function('data', funcStr);
  return templateFunc(data);
}

function getVisibleChildren(children: any[], fields) {
  return children.filter((child: any) => {
    if(child.props?.formItemProps?.invisible != null){
      if(typeof child.props?.formItemProps?.invisible === 'string'){
        const values = fields.getValues() || {}
        try {
          const f = execute_context(child.props?.formItemProps?.invisible, values)
          return !f
        } catch (error) {
          console.log('expless error', child.props?.formItemProps?.invisible)
        }
      }
      if(child.props?.formItemProps?.invisible === true){
        return false
      }
    }
    return !child?.props?.invisible;
  })
}

export interface ProFormProps extends FormProps {
  readOnly: boolean;
  columns: number;
  children: React.ReactChild;
  emptyContent: React.ReactNode | string;
  spacing: number;
  operations?: React.ReactNode | object[];
  operationConfig?: object;
  lastSaveTime?: number;
  device?: string;
  pageInit: Function;
  data: any;
}

export const calculateLastRows: Function = (children: any[], gridColumns: number) => {
  const rows: any[] = [];
  const childrenLength = children.length;
  for (let i = 0; i < childrenLength; ) {
    const subRows = [];
    let index = i;
    let sum = 0;
    let childColumnSpan = children[index].props.columnSpan || children[index].props.formItemProps?.columnSpan || 1;
    if (childColumnSpan >= gridColumns) {
      subRows.push(children[index].key);
    } else {
      while (index < childrenLength) {
        childColumnSpan = children[index].props.columnSpan || children[index].props.formItemProps?.columnSpan || 1;
        sum += childColumnSpan;
        if (sum > gridColumns) {
          index--;
          break;
        }
        subRows.push(children[index++].key);
      }
    }
    i = ++index;
    rows.push(subRows);
  }
  return rows;
};

export const formatFormItems: Function = (
  children: React.ReactChild,
  props: ProFormProps,
  isChildForm: boolean,
) => {
  const {
    columns: gridColumns,
    size,
    device,
    labelAlign,
    labelTextAlign,
    labelCol,
    wrapperCol,
    colon,
    isPreview,
    fields,
  } = props;

  let _children;

  if (!children) {
    return null;
  } else if (Array.isArray(children)) {
    _children = children.filter(
      (child: React.ReactElement) =>
        child && ['function', 'object', 'string'].indexOf(typeof child.type) > -1,
    );
  } else {
    _children = [children];
  }

  _children = getVisibleChildren(_children, fields);

  const rows: any = calculateLastRows(_children, gridColumns);

  return React.Children.map(_children, (child: React.ReactElement<any, any>) => {
    if (ObjUtils.isReactFragment(child)) {
      return formatFormItems(child.props.children, props);
    }
    if (child && ['function', 'object'].indexOf(typeof child.type) > -1) {
      const _labelAlign = device === 'phone' ? 'top' : labelAlign;
      const childrenProps: any = {
        labelCol: child.props.labelCol ? child.props.labelCol : labelCol,
        wrapperCol: child.props.wrapperCol ? child.props.wrapperCol : wrapperCol,
        labelAlign: child.props.labelAlign ? child.props.labelAlign : _labelAlign,
        labelTextAlign: child.props.labelTextAlign ? child.props.labelTextAlign : labelTextAlign,
        colon: 'colon' in child.props ? child.props.colon : colon,
        size: child.props.size ? child.props.size : size,
        isPreview: child.props.isPreview ? child.props.isPreview : isPreview,
      };
      const { formItemProps = {}, ...componentProps } = child.props || {};
      let { columnSpan = 1 } = formItemProps;
      if (!isChildForm && rows[rows.length - 1]?.includes(child.key)) {
        childrenProps.style = { marginBottom: '0px' };
      }
      if(formItemProps.childSlot){
        return (
          <ResponsiveGrid.Cell colSpan={columnSpan}>
            <div className="fusion-ui-form-item">{formItemProps.childSlot}</div>
          </ResponsiveGrid.Cell>
        )
      }

      if (['ProFormItem', 'ChildForm'].includes(child.type.displayName) || child.type?.displayName?.startsWith('Form')) {
        if (child.type.displayName === 'ChildForm') {
          columnSpan = gridColumns;
        }
        return (
          <ResponsiveGrid.Cell colSpan={columnSpan}>
            {React.cloneElement(child, {
              formItemProps: {
                ...ObjUtils.pickDefined(childrenProps),
                ...formItemProps,
              },
            })}
          </ResponsiveGrid.Cell>
        );
      }

      return (
        <ResponsiveGrid.Cell colSpan={columnSpan}>
          <ProFormItem {...childrenProps} {...formItemProps}>
            {React.cloneElement(child, componentProps)}
          </ProFormItem>
        </ResponsiveGrid.Cell>
      );
    }
    return child;
  });
};

const ProForm: React.ForwardRefRenderFunction<any, ProFormProps> = (props: ProFormProps, ref) => {
  const {
    children,
    columns,
    spacing = [0, 16, 16, 0],
    operations,
    operationConfig,
    emptyContent,
    lastSaveTime,
    ...otherProps
  } = props;
  const [isPreview, setIsPreview] = React.useState(props.isPreview);
  React.useEffect(() => { 
    if(props.__designMode == 'design'){
      setIsPreview(props.isPreview) 
    }
  }, [props.isPreview]);

  let myfield = Field.useField([])
  React.useEffect(()=>{ props.pageInit &&  props.pageInit(myfield) }, [])
  React.useEffect(()=>{
    if(props.data){
      myfield.setValues(props.data)
    }
  }, [props.data])

  useImperativeHandle(ref, ()=>({
    setValue : (key, value) => myfield.setValue(key, value),
    getValue : (key) => myfield.getValue(key),
    getFieldError : (key) => myfield.getFieldError(key),
    setFieldError : (key, error) => setFieldError({...fieldError, [key]: error}),
    getFieldValue : (key) => myfield.getFieldValue(key),
    setFieldValue : (key, value) => myfield.setFieldValue(key, value),
    getFieldProps : (key, options) => myfield.getFieldProps(key, options),
    getFieldValueProps : (key, options) => myfield.getFieldValueProps(key, options),
    getFieldErrorProps : (key, options) => myfield.getFieldErrorProps(key, options),
    setValues: (values) => myfield.setValues(values),
    getValues: () => myfield.getValues(),
    setIsPreview: setIsPreview,
    isPreview: ()=> isPreview,
    getValidateValues: myfield.validatePromise,
    field: myfield,
  }))

  const onSubmit = async(e) => {
    e?.preventDefault && e.preventDefault()
    const values = await myfield.validatePromise()
    props.onSubmit && props.onSubmit(values)
  }

  const operationProps = { operations, operationConfig, lastSaveTime };
  return (
    <Form ref={ref} field={myfield} className={cssPrefix} {...otherProps} isPreview={isPreview} onChange={props.onChange} onSubmit={onSubmit}>
      {children ? (
        <ResponsiveGrid className={classnames(cssPrefix, +columns === 1 ? 'one-column' : '')} gap={spacing} columns={columns}>
          {formatFormItems(children, { fields: myfield, columns, ...otherProps, isPreview })}
        </ResponsiveGrid>
      ) : (
        <div className="empty-content">{emptyContent}</div>
      )}
      <Operations {...operationProps} />
    </Form>
  );
};

const RefProForm: React.ForwardRefExoticComponent<ProFormProps> & { Item?: typeof ProFormItem; } = React.forwardRef(ProForm);
RefProForm.displayName = 'ProForm';
RefProForm.defaultProps = {};
RefProForm.propTypes = {};
RefProForm.Item = ProFormItem;

export default RefProForm;
