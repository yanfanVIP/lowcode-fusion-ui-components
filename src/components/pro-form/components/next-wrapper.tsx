import * as React from 'react'
import dayjs from 'dayjs'
import {
  CascaderSelect,
  Checkbox,
  DatePicker2,
  TimePicker2,
  Input,
  NumberPicker,
  Radio,
  Rating,
  Select,
  TreeSelect,
  Upload,
} from '@alifd/next';

import FormItem from './form-item';

const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;
const { RangePicker } = DatePicker2;
const { TextArea, Password } = Input;

const FormCascaderSelect = wrapper(CascaderSelect, 'FormCascaderSelect');
const FormCheckboxGroup = wrapper(CheckboxGroup, 'FormCheckboxGroup');
const FormDatePicker = wrapper(DatePicker2, 'FormDatePicker');
const FormTimePicker = wrapper(TimePicker2, 'FormTimePicker');
const FormInput = wrapper(Input, 'FormInput');
const FormNumberPicker = wrapper(NumberPicker, 'FormNumberPicker');
const FormRadioGroup = wrapper(RadioGroup, 'FormRadioGroup');
const FormRangePicker = wrapper(RangePicker, 'FormRangePicker');
const FormRating = wrapper(Rating, 'FormRating');
const FormSelect = wrapper(Select, 'FormSelect');
const FormTreeSelect = wrapper(TreeSelect, 'FormTreeSelect');
const FormUploadFile = wrapper(Upload.Card, 'FormUploadFile');
const FormUploadFileDrag = wrapper(Upload.Dragger, 'FormUploadFile');
const FormTextArea = wrapper(TextArea, 'FormTextArea');
const FormPassword = wrapper(Password, 'FormPassword');

function wrapper(NextFormComponent: any, displayName: string) {
  const WrappedComponent = (props: any) => {
    const { formItemProps = {}, ...componentProps } = props;
    switch (displayName) {
      case "FormDatePicker": {
        componentProps.preset={ "今天": new Date() }
        if(componentProps.disableAfter){
          componentProps.disabledDate = (date, mode) => {
            switch (mode) {
              case "date":
                return date.valueOf() <= dayjs().valueOf();
              case "year":
                return date.year() < dayjs().year();
              case "month":
                return (
                  date.year() * 100 + date.month() < dayjs().year() * 100 + dayjs().month()
                );
              default: return false;
            }
          }
        }
        break;
      }
      case "FormTimePicker":{
        componentProps.preset=[{ label:"现在", value: new Date()}]; 
        break;
      }
      case "FormRangePicker": {
        componentProps.preset={ "今天": [dayjs(), dayjs()], "本月": [dayjs().startOf("month"), dayjs().endOf("month")] }; 
        if(componentProps.disableAfter){
          componentProps.disabledDate = (date, mode) => {
            switch (mode) {
              case "date":
                return date.valueOf() <= dayjs().valueOf();
              case "year":
                return date.year() < dayjs().year();
              case "month":
                return (
                  date.year() * 100 + date.month() < dayjs().year() * 100 + dayjs().month()
                );
              default: return false;
            }
          }
        }
        break;
      }
      default: break;
    }
    return (
      <FormItem {...formItemProps}>
        <NextFormComponent {...componentProps} style={{width: '100%', ...componentProps.style}}/>
      </FormItem>
    )
  }
  WrappedComponent.displayName = displayName;
  return WrappedComponent;
}

export {
  FormCascaderSelect,
  FormCheckboxGroup,
  FormDatePicker,
  FormTimePicker,
  FormInput,
  FormNumberPicker,
  FormRadioGroup,
  FormRangePicker,
  FormRating,
  FormSelect,
  FormTreeSelect,
  FormUploadFile,
  FormUploadFileDrag,
  FormTextArea,
  FormPassword,
};
