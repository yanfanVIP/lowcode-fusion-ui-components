import * as React from 'react';
import { Timeline, Icon } from '@alifd/next';
import moment from 'moment';
const TimelineItem = Timeline.Item

const FusionTimeLine = React.forwardRef((props,ref) =>{
  const { items, leftTime, slot, allSlot, ...restProps } = props
  const [data, _setData] = React.useState(items);

  React.useEffect(()=>{ _setData(props.items) }, [props.items])

  React.useImperativeHandle(ref, ()=>({
    setData: (data) => _setData(data)
  }))

  const getContext = (item) => {
    const childs = props.children.filter(c=>allSlot || c.props.type == item?.type || c.props.default)
    return React.Children.map(childs, (child) => React.cloneElement(child, {...child.props, data:item.data }))
  }

  return (
    <Timeline ref={ref} {...restProps}>
      { 
        data.map(item=>{
          return (
            <TimelineItem 
              key={`${Math.floor(Math.random() * 10000)}`} 
              state={item.state}
              title={item.title}
              dot={<Icon type={item.icon} size="medium"/>}
              timeLeft={leftTime ? moment(item.time).format('YYYY-MM-DD HH:mm:ss') : null}
              time={leftTime ? null : moment(item.time).format('YYYY-MM-DD HH:mm:ss')}
              content={slot && item ? getContext(item) : `${item.content}` }
            />
          )
        }) 
      }
    </Timeline>
  )
})


const FusionTimeLineItem = React.forwardRef((props,ref) =>{

  const { children, field, data } = props;
  const value = {}
  value[field || "value"] = data

  return (
    <div key={`${Math.floor(Math.random() * 10000)}`} ref={ref}>
      { React.Children.map(children, (child) => React.cloneElement(child, {...child.props, ...value })) }
    </div>
  )
})

FusionTimeLine.displayName = 'FusionTimeLine'
FusionTimeLineItem.displayName = 'FusionTimeLineItem'
FusionTimeLine.Item = FusionTimeLineItem

export default FusionTimeLine;
