import React from 'react';
import SingleMonacoEditorComponent from '@alilc/lowcode-plugin-base-monaco-editor'

type IProps = React.ComponentProps<typeof SingleMonacoEditorComponent>;
const config = {
  paths: {
    vs: "/monaco-editor@0.30.1/min/vs"
  }
}

function CodeEditor(props: IProps) {

  const { formItemProps, ...otherPops } = props
  
  return (
    <SingleMonacoEditorComponent requireConfig={config} {...otherPops} options={{...props.options, readOnly: otherPops.isPreview}} supportFullScreen={true}/>
  )
}

export default CodeEditor;

