import React from "react"
import { ProTable } from '@ant-design/pro-components';
import { Dialog, Loading } from '@alifd/next';
import TableComulnUtil from '../table-select/TableComulnUtil'
import { isEditorEnv } from '@/utils';

const TableSelectDialogComponent = React.forwardRef((props, ref) => {
  const [tableConfig, setTableConfig] = React.useState(null)
  const [columns, setColumns] = React.useState([])
  const realVisible = isEditorEnv(props) ? true : false;

  React.useEffect(() => { initTableConfig() }, [props.table])

  React.useImperativeHandle(ref, ()=>({
    open : async(onSelected) => {
      const { table, columns } = await initTableConfig()
      let selected = []
      const onChange = (ids, rows) => { selected = rows }
      const form = (
        <ProTable toolbar={{title: table.config.comment}} scroll={{x:true}} cardBordered rowKey={table.config.primary} columns={columns}
          rowSelection={{ type:props.multi?'checkbox':'radio', preserveSelectedRowKeys:true, fixed:true, onChange }} 
          columnsState={{ persistenceKey:`TableSelectComponent_${props.table}_columns`, persistenceType: 'localStorage' }} 
          options={{ setting: {listsHeight: 400} }} 
          pagination={{ pageSize: 5 }} 
          request={req}/>
      )
      const dialog = Next.Dialog.show({
        title: '请选择',
        width:'75%', 
        v2: true, 
        hasMask: true, 
        footer: true, 
        content: form,
        onOk:()=>{
          dialog.hide()
          if(props.multi){
            onSelected(selected || [])
          }else{
            onSelected(selected && selected.length > 0 ? selected[0] : null)
          }
        }
      })
    }
  }))

  const initTableConfig = async() => {
    if(!props.table){ return }
    if(tableConfig && columns){ 
      return { table: tableConfig, columns: columns } 
    }
    if(props.config && typeof props.config == 'string'){
      const table =  await request.get(props.config)
      const columns = TableComulnUtil(table, true)
      setColumns(columns)
      setTableConfig(table)
      return { table, columns }
    }else if(props.config){
      const table =  props.config
      const columns = TableComulnUtil(table, true)
      setColumns(columns)
      setTableConfig(table)
      return { table, columns }
    }else{
      const table =  await request.get(`/api/db.table/schema/${props.table}`)
      const columns = TableComulnUtil(table, true)
      setColumns(columns)
      setTableConfig(table)
      return { table, columns }
    }
  }
    
  const req = async(req = {}, _sort) => {
    const filter = {
      ...(props.filters || {}),
      ...req
    }
    delete filter.current
    delete filter.pageSize
    const sort = Object.keys(_sort).map(key=>{
      return key.indexOf('.') == -1 ? `${props.table}.${key} ${_sort[key] == 'ascend' ? ' ASC' : ' DESC'}` :`${key} ${_sort[key] == 'ascend' ? ' ASC' : ' DESC'}`
    })
    const data = await request.post(props.url || `/api/db.table/${props.table}`, { page: req.current, pageSize: req.pageSize, filter, sort:[...sort, ...(props.default_sort || [])] })
    return { data: data.data, success: true, total: data.count }
  }

  if(props.__designMode != 'design'){ return null }

  if(!tableConfig){
    return (
      <Dialog ref={ref} title={'请选择'} width={'75%'} height={600} v2 hasMask visible={realVisible}>
        { props.table ? <Loading /> : '请先配置表名称' }
      </Dialog>
    )
  }

  const dialogProps = {
    title: '请选择',
    width:'75%', 
    v2: true, 
    hasMask: true, 
    footer: true, 
    visible: realVisible
  }

  return (
    <Dialog ref={ref} {...dialogProps}>
      <ProTable toolbar={{title: tableConfig.config.comment}} scroll={{x:true}} cardBordered rowKey={tableConfig.config.primary} columns={columns}
        rowSelection={{ type:props.multi?'checkbox':'radio', preserveSelectedRowKeys:true, fixed:true }} 
        columnsState={{ persistenceKey:`TableSelectComponent_${props.table}_columns`, persistenceType: 'localStorage' }} 
        options={{ setting: {listsHeight: 400} }} 
        pagination={{ pageSize: 5 }} 
        request={req}/>
    </Dialog>
  )
})

export default TableSelectDialogComponent