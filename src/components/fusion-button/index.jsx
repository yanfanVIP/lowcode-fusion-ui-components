import * as React from 'react';
import { Button as NextButton, Icon } from '@alifd/next';

const FusionButton = React.forwardRef((props, ref) =>{
  return (
    <NextButton ref={ref} {...props} >
      { props.icon ? <Icon type={props.icon} /> : null }
      {props.children}
    </NextButton>
  );
})

FusionButton.displayName = 'FusionSwitchCase'
export default FusionButton;
