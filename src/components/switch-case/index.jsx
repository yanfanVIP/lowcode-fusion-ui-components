import * as React from 'react';

const SwitchCase = React.forwardRef((props,ref) =>{
  const { children, allSlot, value, ...restProps } = props
  const childs = children.filter(c=>allSlot || c.props.type == value?.type || c.props.default)

  return (
    <div ref={ref} {...restProps}>
      { React.Children.map(childs, (child) => React.cloneElement(child, {...child.props, data: value?.data })) }
    </div>
  )
})

const SwitchCaseItem = React.forwardRef((props,ref) =>{
  const { children, field, data } = props;
  const value = {}
  value[field || "value"] = data

  return (
    <div key={`${Math.floor(Math.random() * 10000)}`} ref={ref}>
      { React.Children.map(children, (child) => React.cloneElement(child, {...child.props, ...value })) }
    </div>
  )
})

SwitchCase.displayName = 'FusionSwitchCase'
SwitchCaseItem.displayName = 'FusionSwitchCaseItem'
SwitchCase.Item = SwitchCaseItem

export default SwitchCase;
