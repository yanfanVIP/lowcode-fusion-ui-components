function Component(props) {
  return (
    <div {...props}>{props.children}</div>
  )
}

export default Component;

