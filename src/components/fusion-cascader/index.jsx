import React from 'react';
import { Cascader, Loading } from '@alifd/next'

function Component(props) {
  const [value, setValue] = React.useState(props.value)
  const [loading, setLoading] = React.useState(props.loading)
  React.useEffect(()=>{ setValue(props.value) }, [props.value])
  React.useEffect(()=>{ setLoading(props.loading) }, [props.loading])

  const handleChange = (value, data, extra) => {
    setValue(value)
    props.onChange && props.onChange(value, data, extra)
  }

  if(loading){
    return (
      <div {...props} style={{...props.style, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
        <Loading/>
      </div>
    )
  }

  if(!props.dataSource || props.dataSource.length == 0){
    return (
      <div {...props} style={{...props.style, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
        <img src="/resources/empty.png" style={{width:'40%'}}></img>
        <p style={{marginTop: 16, fontSize:14}}>暂时没有可选项</p>
      </div>
    )
  }

  return (
    <Cascader className='fusion-cascader' {...props} value={value} dataSource={props.dataSource} onChange={handleChange}/>
  )
}

export default Component;

