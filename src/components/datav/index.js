import * as datav from "../data-view-react/index/index";

const FusionDatav = (props)=>{
  const { component, children, ...other } = props
  const Component = datav[component]
  let childs = children

  if(Array.isArray(childs) && childs.length == 1){
    const c = childs[0]
    if(typeof c == 'object' && c.props.componentName == 'Slot'){
      childs = c.props.children
    }
  }
  if(Component){
    if(!childs){ 
      return <Component className="fusion-datav" {...other} /> 
    }else{
      return (
        <Component className="fusion-datav" {...other}>
          { React.Children.map(childs, (child) => React.cloneElement(child, { ...child.props })) }
        </Component>
      )
    }
  }
  return <div>请设置组件名称</div>
}

export default FusionDatav 