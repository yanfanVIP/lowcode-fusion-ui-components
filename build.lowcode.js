const { library } = require('./build.json');
const { version, name } = require('./package.json')
const path = require('path')

module.exports = {
  sourceMap: false,
  alias: {
    '@': './src',
    '@alifd/fusion-ui': './src',
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    proxy: {
      "/api": 'http://localhost:3000'
    }
  },
  plugins: [
    [
      '@alifd/build-plugin-lowcode',
      {
        noParse: true,
        engineScope: '@alilc',
        builtinAssets: [{
          packages:[{
              package: 'JSON',
              version: '1.0.0',
              urls:[
                '/resources/common/jsonplus.js',
                '/resources/common/request.js'
              ]
            }
          ],
          components:[],
          sort:{},
          groupList: [],
          ignoreComponents: {}
        }]
      },
    ],
  ],
};
